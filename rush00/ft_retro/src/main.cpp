#include <iostream>

#include "Core.hpp"

int main(void)
{
	Core 	*gameConsole = new Core();

	gameConsole->start();
	delete gameConsole;
	return 0;
}
