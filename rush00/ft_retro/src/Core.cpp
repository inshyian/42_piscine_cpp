#include <ctime>
#include <iostream>

#include "Core.hpp"

Core::Core(void)
{
	_score = 0;
	_cycles = 0;
	_cyclesForEnemyMove = 8;
	_level = 1;
	_player = new Player(COLUMNS / 2, LINES - 2, 1);
	_stars = new Collection();
	_amos = new Collection();
	_enemies = new Collection();
	initNcurses();
}

Core::~Core()
{
	delete _stars;
	delete _amos;
	delete _enemies;
	delete _player;
	destroyNcurses();
}

void		Core::initNcurses() {

	initscr();
	noecho();
	raw();
	curs_set(false);
	nodelay(stdscr, true);
	keypad(stdscr, TRUE);

	_win = newwin(LINES, COLUMNS, 0, 0);
	_info = newwin(40, 20, 0, 180);

	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_CYAN, COLOR_BLACK);
	init_pair(4, COLOR_YELLOW, COLOR_BLACK);
}

void		Core::destroyNcurses() {

	delwin(_win);
	delwin(_info);
	endwin();
}

bool		Core::checkPenetration(Object *enemy)
{
	for (int i = 0; i < _amos->getCount(); i++)
	{
		if (enemy->getX() == _amos->getOneUnit(i)->getX() &&
			enemy->getY() == _amos->getOneUnit(i)->getY())
		{
			_score++;
			if (_score % 10 == 0)
			{
				if (_cyclesForEnemyMove > 2)
				{
					_cyclesForEnemyMove = _cyclesForEnemyMove - 2;
					_level = _level + 1;
				}
				else if (_level < 5 && _cyclesForEnemyMove == 2)
					_level = _level + 1;
				system("afplay resources/power_up.mp3&");
				_player->setLives(_player->getLives() + 1);
			}
			return true;
		}
	}
	return false;
}

bool		Core::checkCollision(Object *enemy)
{
	if (_player->getX() == enemy->getX() &&
		_player->getY() == enemy->getY())
	{
		_score++;
		return true;
	}
	return false;
}

void		Core::moveEnemies()
{
	std::string		character;

	if (_cycles % _cyclesForEnemyMove == 0)
	{
		for (int i = 0; i < _enemies->getCount(); i++)
		{
			if (_enemies->getOneUnit(i) != NULL && _enemies->getOneUnit(i)->getY() > LINES - 2)
				_enemies->getOneUnit(i)->setY(1);
			else if (_enemies->getOneUnit(i) != NULL && checkPenetration(_enemies->getOneUnit(i)) == true)
				_enemies->setNullUnit(i);
			else if (_enemies->getOneUnit(i) != NULL && checkCollision(_enemies->getOneUnit(i)) == true)
			{
				_player->takeDamage(1);
				_enemies->setNullUnit(i);
			}
			else if (_enemies->getOneUnit(i) != NULL)
			{
				character = _enemies->getOneUnit(i)->getCharacter();
				mvwaddstr(_win, _enemies->getOneUnit(i)->getY(), _enemies->getOneUnit(i)->getX(), character.c_str());
				_enemies->getOneUnit(i)->moveDown();
			}
		}
	}
	else
	{
		for (int i = 0; i < _enemies->getCount(); i++)
		{
			if (_enemies->getOneUnit(i) != NULL && _enemies->getOneUnit(i)->getY() > LINES - 2)
				_enemies->getOneUnit(i)->setY(1);
			else if (_enemies->getOneUnit(i) != NULL && checkPenetration(_enemies->getOneUnit(i)) == true)
				_enemies->setNullUnit(i);
			else if (_enemies->getOneUnit(i) != NULL && checkCollision(_enemies->getOneUnit(i)) == true)
			{
				_player->takeDamage(1);
				_enemies->setNullUnit(i);
			}
			else if (_enemies->getOneUnit(i) != NULL)
			{
				character = _enemies->getOneUnit(i)->getCharacter();
				mvwaddstr(_win, _enemies->getOneUnit(i)->getY(), _enemies->getOneUnit(i)->getX(), character.c_str());
			}
		}
	}
}

void		Core::printInfo()
{
	wattron(_info, COLOR_PAIR(2));
	mvwprintw(_info, 1, 6, "*GAME");
	mvwprintw(_info, 2, 4, "(a)steroids");
	mvwprintw(_info, 4, 2, "Lives: %d", _player->getLives());
	mvwprintw(_info, 6, 2, "Score: %d", _score);
	mvwprintw(_info, 8, 2, "Time: %ds", (time(NULL) - _startGame));
	mvwprintw(_info, 10, 2, "Level: %d", (_level));
	mvwprintw(_info, 14, 2, "'p' - pause");
	mvwprintw(_info, 15, 2, "'esc' - exit");
	mvwprintw(_info, 16, 2, "'a' - move left");
	mvwprintw(_info, 17, 2, "'d' - move right");
}

void		Core::createDotEnemy(int count) {

	for (int i = 0; i < count; i++)
	{
		int rX = 3 + (rand() % 150);
		int rY = 2 + (rand() % 5);
		_enemies->push(new Enemy(rX, rY, 'o'));

	}
}

void		Core::createCrossEnemy(int count) {

	for (int i = 0; i < count; i++)
	{
		int rX = 3 + (rand() % 140);
		int rY = 2 + (rand() % 8);
		_enemies->push(new Enemy(rX, rY, '|'));
		_enemies->push(new Enemy(rX, rY + 1, '+'));
		_enemies->push(new Enemy(rX, rY + 2, '|'));
		_enemies->push(new Enemy(rX - 1, rY + 1, '-'));
		_enemies->push(new Enemy(rX + 1, rY + 1, '-'));
	}
}

void		Core::createSqEnemy(int count) {

	for (int i = 0; i < count; i++)
	{
		int rX = 3 + (rand() % 150);
		int rY = 2 + (rand() % 8);
		_enemies->push(new Enemy(rX, rY, '['));
		_enemies->push(new Enemy(rX + 1, rY, ']'));
	}
}

void		Core::createHorEnemy(int count) {

	for (int i = 0; i < count; i++)
	{
		int rX = 3 + (rand() % 150);
		int rY = 2 + (rand() % 8);
		_enemies->push(new Enemy(rX, rY, '-'));
		_enemies->push(new Enemy(rX - 1, rY, '-'));
		_enemies->push(new Enemy(rX + 1, rY, '-'));
	}
}

void		Core::createVertEnemy(int count) {

	for (int i = 0; i < count; i++)
	{
		int rX = 3 + (rand() % 150);
		int rY = 2 + (rand() % 8);
		_enemies->push(new Enemy(rX, rY, '-'));
		_enemies->push(new Enemy(rX, rY + 1, '+'));
		_enemies->push(new Enemy(rX, rY + 2, '+'));
		_enemies->push(new Enemy(rX, rY + 3, '+'));
		_enemies->push(new Enemy(rX, rY + 4, '-'));
	}
}

void		Core::createRandEnemy(int count) {

	typedef void	(Core::*enemyCreators)(int);

	enemyCreators	creators[] = {
		&Core::createDotEnemy,
		&Core::createCrossEnemy,
		&Core::createSqEnemy,
		&Core::createHorEnemy,
		&Core::createVertEnemy
	};
	for (int i = 0; i < count; i++)
		(this->*(creators[(rand()) % 5]))(1);
}

void		Core::createSteroids(int count) {

	for (int i = 0; i < count; i++)
		_stars->push(new Star(rand() % COLUMNS, rand() % LINES, '.'));
}

void		Core::moveBullets() {

	std::string		character;

	for (int i = 0; i < _amos->getCount(); i++)
	{
		if (_amos->getOneUnit(i)->getY() > 0)
		{
			character = _amos->getOneUnit(i)->getCharacter();
			wattron(_win, COLOR_PAIR(4));
			mvwaddstr(_win, _amos->getOneUnit(i)->getY(), _amos->getOneUnit(i)->getX(), character.c_str());
			wattroff(_win, COLOR_PAIR(3));
		}
		_amos->getOneUnit(i)->moveUp();
	}
}

void		Core::moveSteroids() {

	std::string		character;

	for (int i = 0; i < _stars->getCount(); i++)
	{
		if (_stars->getOneUnit(i)->getY() > LINES - 2)
			_stars->getOneUnit(i)->setY(0);
		else if (_stars->getOneUnit(i)->getX() > 0 && _stars->getOneUnit(i)->getX() < COLUMNS - 2)
		{
			character = _stars->getOneUnit(i)->getCharacter();
			mvwaddstr(_win, _stars->getOneUnit(i)->getY(), _stars->getOneUnit(i)->getX(), character.c_str());
		}
		_stars->getOneUnit(i)->moveDown();
	}
}

void		Core::movePlayer() {

	wattron(_win, COLOR_PAIR(3));
	mvwaddstr(_win, _player->getY(), _player->getX(), "^");
	wattroff(_win, COLOR_PAIR(3));
}

void		Core::playerAction(int key) {

	if (key == 32)
	{
		Amo *newAmo = _player->shoot('|');
		if (_level >= 2)
			_amos->push(new Amo(_player->getX() + 1, _player->getY(), '|'));
		if (_level >= 3)
			_amos->push(new Amo(_player->getX() - 1, _player->getY(), '|'));
		if (_level >= 4)
			_amos->push(new Amo(_player->getX() + 2, _player->getY(), '|'));
		if (_level >= 5)
			_amos->push(new Amo(_player->getX() - 2, _player->getY(), '|'));
		_amos->push(newAmo);
	}
	else if ((key == 97 || key == KEY_LEFT) && _player->getX() > 3)
	{
		_player->moveLeft();
		if (_level >= 5 && _player->getX() > 3)
			_player->moveLeft();
	}
	else if ((key == 100 || key == KEY_RIGHT) && _player->getX() < COLUMNS - 4)
	{
		_player->moveRight();
		if (_level >= 5 && _player->getX() < COLUMNS - 4)
		 	_player->moveRight();
	}
}

void		Core::clearNcursesWin() {

	wclear(_win);
	wattron(_win, COLOR_PAIR(1));
	wattron(_info, COLOR_PAIR(1));
	box(_win, 0, 0);
	box(_info, 0, 0);
	wattroff(_win, COLOR_PAIR(1));
	wattroff(_info, COLOR_PAIR(1));
}

void		Core::refreshNcursesWin() {

	wrefresh(_win);
	wrefresh(_info);
}

void		Core::gamePause() {

	while (getch() == EOF)
		;
}

void		Core::start()
{
	gamePause();
	system("afplay resources/play.mp3&");
	_startGame = time(NULL);
	srand(clock());
	createSteroids(50);
	createDotEnemy(5);
	createCrossEnemy(3);
	createHorEnemy(3);
	createVertEnemy(3);
	createSqEnemy(3);
	int key = 0;
	_lastTime = clock();
	while (true)
	{
		if ((float)(clock() - _lastTime) / CLOCKS_PER_SEC < 0.1)
			continue ;
		if (_player->getLives() <= 0)
		{
			system("afplay resources/game_over.mp3&");
			gamePause();
			break ;
		}
		else if (_score == _enemies->getCount())
		{
			system("afplay resources/game_complete.mp3&");
			gamePause();
			break ;
		}
		printInfo();
		_lastTime = clock();
		clearNcursesWin();
		moveSteroids();
		moveBullets();
		moveEnemies();
		playerAction(key);
		movePlayer();
		refreshNcursesWin();
		key = getch();
		flushinp();
		if (_cycles % 100 == 0)
			createRandEnemy(1);
		if (key == 27)
			break ;
		if (key == 112)
			gamePause();
		_cycles++;
	}
}
