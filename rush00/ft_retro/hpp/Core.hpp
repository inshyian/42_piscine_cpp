#ifndef CORE_HPP
# define CORE_HPP

# include <string.h>
# include <ncurses.h>
# include <fstream>
# include <iostream>
# include "Player.hpp"
# include "Collection.hpp"
# include "Star.hpp"
# include "Enemy.hpp"
# include "Amo.hpp"

class Collection;

class Core
{
	private:
		static const int	COLUMNS = 180;
		static const int	LINES = 40;

		time_t				_startGame;
		double				_lastTime;
		int					_cycles;
		int					_cyclesForEnemyMove;
		int					_level;

		Player				*_player;
		int					_score;
		Collection			*_stars;
		Collection			*_amos;
		Collection			*_enemies;

		WINDOW				*_win;
		WINDOW				*_info;

		void				printInfo();
		bool				checkPenetration(Object *enemy);
		bool				checkCollision(Object *enemy);
		void				moveEnemies();
		void				moveBullets();
		void				moveSteroids();
		void				movePlayer();
		void				playerAction(int key);
		void				createSteroids(int count);
		void				createDotEnemy(int count);
		void				createCrossEnemy(int count);
		void				createSqEnemy(int count);
		void				createHorEnemy(int count);
		void				createVertEnemy(int count);
		void				createRandEnemy(int count);

		void				initNcurses();
		void				clearNcursesWin();
		void				refreshNcursesWin();
		void				destroyNcurses();

		void				gamePause();

		Core				&operator =(Core const &other);
		Core(Core const &other);

	public:
		Core(void);
		~Core();

		void				start();
};

#endif
