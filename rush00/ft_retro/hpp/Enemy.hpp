#ifndef ENEMY_HPP
# define ENEMY_HPP

# include "Object.hpp"

class Enemy : public Object
{
public:
	Enemy(void);
	Enemy(Enemy const &other);
	Enemy(int x, int y, char character);
	~Enemy();

	Enemy &operator =(Enemy const &other);

	Object* clone() const;
};

#endif
