#ifndef AMO_HPP
# define AMO_HPP

# include "Object.hpp"

class Amo : public Object
{
public:
	Amo(void);
	Amo(Amo const &other);
	Amo(int x, int y, char character);
	~Amo();

	Amo &operator =(Amo const &other);

	Object* clone() const;
};



#endif
