#ifndef STAR_HPP
# define STAR_HPP

#include "Object.hpp"

class Star : public Object
{
public:
	Star(void);
	Star(Star const &other);
	Star(int x, int y, char character);
	~Star();

	Star &operator =(Star const &other);

	Object* 		clone() const;
};




#endif
