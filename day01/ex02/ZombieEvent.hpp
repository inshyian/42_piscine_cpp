/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 13:40:02 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/01 18:01:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_EVENT_HPP
# define ZOMBIE_EVENT_HPP
# define NAME_LEN 4
# include <iostream>
# include "Zombie.hpp"

class ZombieEvent {

private:
	std::string		nameGenerator(int len);
	std::string		type;
	std::string		voice;
	void			setVoice(void);

public:
	ZombieEvent();
	~ZombieEvent();
	void			setZombieType(std::string);
	Zombie			*newZombie(std::string);

};

#endif
