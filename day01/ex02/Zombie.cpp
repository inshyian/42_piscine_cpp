/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 13:19:34 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/01 18:05:06 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

Zombie::Zombie(std::string name) : name(name) {

				return;
}

Zombie::~Zombie(void) {

				return;
}

std::string		&Zombie::getNameRef(void) {

				return (name);
}

std::string		&Zombie::getTypeRef(void) {

				return (type);
}

std::string		&Zombie::getVoiceRef(void) {

				return (voice);
}

void			Zombie::announce(void) {

				std::cout << "<" << name << "(" << type << ")" << "> " << voice << std::endl;
				return;
}
