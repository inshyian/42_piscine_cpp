/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 13:40:06 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/01 17:51:15 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP
# define NAME_LEN 4
# include <iostream>

class Zombie {

private:

	std::string		name;
	std::string		type;
	std::string		voice;

public:
	Zombie(std::string);
	~Zombie();
	std::string		&getNameRef(void);
	std::string		&getTypeRef(void);
	std::string		&getVoiceRef(void);
	void			announce(void);

};

#endif
