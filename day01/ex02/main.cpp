/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 13:40:00 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 16:30:50 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"

std::string		getRandomName(size_t len) {

	std::string str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	std::string newstr;
	int			pos;

	srand(time(0));
	while (newstr.size() != len) {
		pos = ((rand() % (str.size() - 1)));
		newstr += str.substr(pos,1);
	}
	return newstr;
}

void			randomChump(void) {

	Zombie		instance(getRandomName(5));

	instance.getTypeRef() = "greedy";
	instance.getVoiceRef() = "Brains...........";
	instance.announce();
}

int				main(void) {

	ZombieEvent	ze;
	Zombie		*instancep;

	ze.setZombieType("angry");
	instancep = ze.newZombie("good_name");
	delete instancep;
	randomChump();
	return (0);
}
