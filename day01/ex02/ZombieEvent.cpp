/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 13:40:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/01 18:40:55 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent(void) {

				return;
}

ZombieEvent::~ZombieEvent(void) {

				return;
}

void			ZombieEvent::setZombieType(std::string type) {

				ZombieEvent::type = type;
				ZombieEvent::setVoice();
				return;
}

void			ZombieEvent::setVoice(void) {

				voice = "I need moar brains!";
				return;
}

Zombie			*ZombieEvent::newZombie(std::string name) {

				Zombie		*instance = new Zombie(name);

				instance->getTypeRef() = type;
				instance->getVoiceRef() = voice;
				instance->announce();
				return (instance);
}
