/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 14:36:35 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 21:43:09 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
# define HUMANB_HPP
# include <iostream>
# include "Weapon.hpp"

class HumanB {

private:
	std::string		name;
	Weapon			*weapon;

public:
	HumanB(std::string);
	~HumanB();
	void			attack(void);
	void			setWeapon(Weapon &w);
};

#endif
