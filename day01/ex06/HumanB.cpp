/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 14:47:00 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 19:52:01 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"

HumanB::HumanB(std::string name) : name(name) {

	return ;
}

HumanB::~HumanB(void) {

	return ;
}

void	HumanB::attack(void) {

	std::cout << name << " attacks with his " << weapon->getType() << std::endl;
	return;
}

void	HumanB::setWeapon(Weapon &w) {

	weapon = &w;
	return ;
}
