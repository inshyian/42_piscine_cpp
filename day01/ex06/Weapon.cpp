/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 14:22:26 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 21:03:17 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.hpp"

Weapon::Weapon(std::string type) : type(type) {

	return ;
}

Weapon::~Weapon(void) {

	return ;
}

const std::string		&Weapon::getType(void) {

	return (type);
}

void					Weapon::setType(std::string new_type) {

	type = new_type;
	return ;
}
