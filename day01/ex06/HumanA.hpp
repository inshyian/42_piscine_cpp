/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 14:30:08 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 21:42:53 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANA_HPP
# define HUMANA_HPP
# include <iostream>
# include "Weapon.hpp"

class HumanA {

private:
	std::string		name;
	Weapon			&weapon;

public:
	HumanA(std::string, Weapon &);
	~HumanA();
	void			attack(void);
};

#endif
