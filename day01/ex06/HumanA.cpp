/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 14:43:17 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 19:52:03 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"

HumanA::HumanA(std::string name, Weapon &w) : name(name), weapon(w) {

	return ;
}

HumanA::~HumanA(void) {

	return ;
}

void	HumanA::attack(void) {

	std::cout << name << " attacks with his " << weapon.getType() << std::endl;
	return ;
}
