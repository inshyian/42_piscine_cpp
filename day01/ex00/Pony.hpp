/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/26 16:48:15 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/01 13:57:24 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_HPP
# define PONY_HPP
# include <iostream>

class	Pony {

private:
	std::string		name;
	std::string		noise;

public:
	Pony(std::string);
	~Pony(void);
	void			PlayNoise(void) const;
};

#endif
