/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/26 16:48:12 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/26 20:55:07 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

Pony::Pony(std::string name) {

	this->name = name;
	this->noise = "pi-u";
	std::cout << "Pony " << name << ": comes to life.." << std::endl;
	return;
}

Pony::~Pony(void) {

	std::cout << "Pony " << name << ": * the cry of a dying pony *" << std::endl;
	return;
}

void	Pony::PlayNoise(void) const {

	std::cout << "Pony " << this->name << " says: " << noise << std::endl;
	return;
}
