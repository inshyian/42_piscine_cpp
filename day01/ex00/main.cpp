/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/26 16:47:08 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/01 13:57:08 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

void	ponyOnTheHeap(void) {

	Pony *pony = new Pony("onTheHeap");

	pony->PlayNoise();
	delete pony;
};

void	ponyOnTheStack(void) {

	Pony pony = Pony("onTheStack");

	pony.PlayNoise();
}

int		main(void) {

	ponyOnTheHeap();
	ponyOnTheStack();
	return (0);
}
