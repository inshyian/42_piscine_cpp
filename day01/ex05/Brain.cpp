/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 22:06:53 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 18:13:37 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"

Brain::Brain(void) {

	countNeuron = 1000000000;
	return;
}

Brain::~Brain(void) {

	return;
}

std::string		Brain::identify(void) const {

	std::stringstream	ss;

	ss << "0x" << std::uppercase << std::hex << (size_t)this;
	return (ss.str());
}
