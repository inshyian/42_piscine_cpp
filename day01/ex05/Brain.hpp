/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 22:06:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 18:13:39 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BRAIN_HPP
# define BRAIN_HPP
# include <sstream>
# include <iostream>

class Brain {

private:
	size_t			countNeuron;

public:
	Brain(void);
	~Brain(void);
	std::string identify(void) const;
};

#endif
