/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 22:06:45 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 17:57:38 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMAN_HPP
# define HUMAN_HPP
# include "Brain.hpp"

class Human {

private:
	const Brain		neuralNetwork;
	static int		id;

public:
	Human(void);
	~Human(void);
	std::string		identify(void);
	const Brain		&getBrain(void);
};

#endif
