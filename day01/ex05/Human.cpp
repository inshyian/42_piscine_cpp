/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 22:06:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 18:13:41 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

Human::Human(void) {

	id = id + 1;
	return;
}

Human::~Human(void) {

	return;
}

 std::string		Human::identify(void) {

	return (this->neuralNetwork.identify());
}

const Brain		&Human::getBrain(void) {

	return (this->neuralNetwork);
}

int	Human::id = 0;
