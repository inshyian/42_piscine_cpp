/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/30 22:20:33 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/01 12:31:44 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cat9Tails.hpp"

static void			openFile(char *filename, Cat9Tails &printer) {

	std::ifstream	ifs;

	ifs.open(filename);
	struct stat		st;
	if (ifs.good() && stat(filename, &st) == 0 && (st.st_mode & S_IFDIR) == 0)
		printer.printFile(ifs);
	else if (ifs.good() && stat(filename, &st) == 0 && st.st_mode & S_IFDIR)
		std::cout << "Cat9Tails: " << filename << ": " << "Is a directory" << std::endl;
	else
		std::cout << "Cat9Tails: " << filename << ": " << "No such file or directory" << std::endl;
}

static void			argvCycle(int argc, char **argv, Cat9Tails &printer) {

	int				i;

	i = 1;
	while (i < argc)
	{
		openFile(argv[i], printer);
		i++;
	}
}

int					main(int argc, char **argv) {

	Cat9Tails		printer;

	if (argc == 1)
		printer.printCin();
	else
		argvCycle(argc, argv, printer);
	return (0);
}
