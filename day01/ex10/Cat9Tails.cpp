/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat9Tails.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/30 22:29:20 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/01 12:35:08 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cat9Tails.hpp"

Cat9Tails::Cat9Tails(void) {

	return ;
}

Cat9Tails::~Cat9Tails(void) {

	return ;
}

void Cat9Tails::printFile(std::ifstream &ifs) {

	std::cout << ifs.rdbuf();
	return ;
}

void Cat9Tails::printCin(void) {

	std::string		buf;

	while (!std::cin.eof())
	{
		std::cin >> buf;
		std::cout << buf << std::endl;
	}
	return ;
}
