/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cat9Tails.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/30 22:29:22 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/01 12:34:55 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CAT9TAILS_HPP
# define CAT9TAILS_HPP
# include <iostream>
# include <fstream>
# include <sstream>
# include <sys/stat.h>

class Cat9Tails {

public:
	Cat9Tails(void);
	~Cat9Tails(void);
	void printFile(std::ifstream &);
	void printCin(void);
};

#endif
