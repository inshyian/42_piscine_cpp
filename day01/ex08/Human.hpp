/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 20:43:34 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 21:43:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMAN_HPP
# define HUMAN_HPP
# include <iostream>

class Human {

private:
	void	meleeAttack(std::string const &target);
	void	rangedAttack(std::string const &target);
	void	intimidatingShout(std::string const &target);

public:
	void	action(std::string const &action_name, std::string const &target);
};

#endif
