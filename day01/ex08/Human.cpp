/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 20:43:36 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 21:24:10 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"
#include <map>

void	Human::meleeAttack(std::string const &target) {

		std::cout << __func__ << " called" << std::endl;
		std::cout << "Human kicks " << target << std::endl;
}

void	Human::rangedAttack(std::string const &target) {

		std::cout << __func__ << " called" << std::endl;
		std::cout << "Human eliminates " << target << std::endl;
}

void	Human::intimidatingShout(std::string const &target) {

		std::cout << __func__ << " called" << std::endl;
		std::cout << "Human intimidates " << target << std::endl;
}

void	Human::action(std::string const &action_name, std::string const &target) {

		typedef void (Human::*Actions)(std::string const &target);
		std::string		actionsStr[] = {"meleeAttack", "rangedAttack", "intimidatingShout"};
		Actions			actions[] = {&Human::meleeAttack, &Human::rangedAttack, &Human::intimidatingShout};
		int				i;

		i = 2;
		while (i >= 0)
		{
			if (actionsStr[i] == action_name)
			{
				(this->*(actions[i]))(target);
				break ;
			}
			i--;
		}
		if (i < 0)
			std::cout << "Invalid action called" << std::endl;
}
