/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 20:43:33 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 21:22:11 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

int		main(void) {

	Human			human;
	std::string		actionName;
	std::string		targetName;

	actionName = "meleeAttack";
	targetName = "Someone";
	human.action(actionName, targetName);
	actionName = "rangedAttack";
	human.action(actionName, targetName);
	actionName = "intimidatingShout";
	human.action(actionName, targetName);
	actionName = "invalidAction";
	human.action(actionName, targetName);
	human.action(actionName, targetName);
}
