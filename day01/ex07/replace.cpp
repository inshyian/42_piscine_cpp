/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   replace.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 15:01:45 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/02 13:57:52 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/stat.h>

static void				replace(std::ifstream &fs, std::string regex, std::string replacement, std::string filename) {

	std::ofstream		fs_replace;
	std::stringstream	s_str;
	std::string			new_str;
	size_t				cur;
	size_t				prev;

	filename = filename + ".replace";
	fs_replace.open(filename);
	s_str << fs.rdbuf();
	new_str = s_str.str();
	prev = 0;
	cur = 0;
	while (cur < new_str.length() && cur != std::string::npos)
	{
		cur = new_str.find(regex, cur);
		if (cur != std::string::npos)
		{
			fs_replace << new_str.substr(prev, cur - prev);
			fs_replace << replacement;
			cur = cur + regex.length();
			prev = cur;
		}
		else
			fs_replace << new_str.substr(prev);
	}
	fs_replace.close();
}

static void				displayFileError(std::string filename) {

	std::cout << "\tError while reading file <" << filename << ">" << std::endl;
}

static void				displayUsage(void) {

	std::cout << "Usage:\t./replace <filename> <regex> <replacewith>" << std::endl;
	std::cout << "\tProgram will save result in file <filename>.replace" << std::endl;
}

int						main(int ac, char **av) {

	std::ifstream	fs;

	if (ac == 4)
	{
		fs.open(av[1]);
		struct stat 	st;
		if (fs.good() && stat(av[1], &st) == 0 && (st.st_mode & S_IFDIR) == 0)
		{
			replace(fs, av[2], av[3], av[1]);
		}
		else
			displayFileError(av[1]);
	}
	else
		displayUsage();
	return (0);
}
