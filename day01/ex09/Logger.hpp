/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logger.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/30 21:24:24 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 22:06:27 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LOGGER_H
# define LOGGER_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>

class Logger {

private:
	std::string		fileName;
	void			logToConsole(std::string string);
	void			logToFile(std::string string);
	std::string		makeLogEntry(std::string string);

public:
	Logger(std::string fileName);
	~Logger(void);
	void			log(std::string const &dest, std::string const &message);
};

#endif
