/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logger.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/30 21:24:31 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 22:19:47 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.hpp"

Logger::Logger(std::string fileName) : fileName(fileName) {

	return ;
}

Logger::~Logger(void) {

	return ;
}

void	Logger::log(std::string const &dest, std::string const &message) {

	typedef void (Logger::*Destinations)(std::string string);
	std::string			destinationsStr[] = {"logToConsole", "logToFile"};
	Destinations		destinations[] = {&Logger::logToConsole, &Logger::logToFile};
	int					i;

	i = 1;
	while (i >= 0)
	{
		if (destinationsStr[i] == dest)
		{
			(this->*(destinations[i]))(this->makeLogEntry(message));
			break ;
		}
		i--;
	}
	if (i < 0)
		std::cout << "Invalid dest called" << std::endl;
	return ;
}

void	Logger::logToConsole(std::string string)
{
	std::cout << string;
	return ;
}

void	Logger::logToFile(std::string string)
{
	std::ofstream		ofstream;

	ofstream.open(fileName, std::ios_base::app);
	ofstream << string;
	ofstream.close();
	return ;
}

std::string	Logger::makeLogEntry(std::string string)
{
	std::stringstream	stream;
	time_t				rawtime;
	struct tm			*timeinfo;
	char				buffer[80];

	time (&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer,80,"%Y%m%d_%H%M%S ",timeinfo);
	stream << buffer << std::endl << string << std::endl;
	return (stream.str());
}
