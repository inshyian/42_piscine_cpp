/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/30 21:24:29 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 22:19:57 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.hpp"

int main(void) {

	std::string		dest;
	std::string		message;
	Logger			logger("log.txt");

	dest = "logToConsole";
	message = "Some log message";
	logger.log(dest, message);
	dest = "logToFile";
	logger.log(dest, message);
	dest = "asd";
	logger.log(dest, message);
}
