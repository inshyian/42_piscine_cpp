/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 22:01:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/01 22:06:07 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

void	printRef(std::string &str) {

	std::cout << "Print via reference:" << std::endl;
	std::cout << str << std::endl;
}

void	printPtr(std::string *str) {

	std::cout << "Print via pointer:" << std::endl;
	std::cout << *str << std::endl;
}

int		main(void) {

	std::string 	str	= "HI THIS IS BRAIN";

	printRef(str);
	printPtr(&str);
	return (0);
}
