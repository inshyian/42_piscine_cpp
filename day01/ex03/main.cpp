/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 13:40:00 by ishyian           #+#    #+#             */
/*   Updated: 2019/07/02 19:21:40 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieHorde.hpp"
#include <sys/time.h>
#include <unistd.h>

int			main(void) {

	struct timeval  tv;

	gettimeofday(&tv, NULL);
	srand((tv.tv_sec) * 1000 + tv.tv_usec / 1000);
	ZombieHorde	zh(13);
	return (0);
}
