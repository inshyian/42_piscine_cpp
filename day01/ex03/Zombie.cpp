/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 13:19:34 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/03 17:34:57 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"

Zombie::Zombie(void) {

				name = getRandomName(5);
				type = "Copy";
				voice = "We are Borg. Resistance is futile. You will be assimilated";
				return;
}

Zombie::~Zombie(void) {

				return;
}

std::string		Zombie::getRandomName(size_t len) {

	std::string str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	std::string newstr;
	int 		pos;

	while (newstr.size() != len) {
		pos = ((std::rand() % (str.size() - 1)));
		newstr += str.substr(pos,1);
	}
	return newstr;
}

std::string		&Zombie::getNameRef(void) {

				return (name);
}

std::string		&Zombie::getTypeRef(void) {

				return (type);
}

std::string		&Zombie::getVoiceRef(void) {

				return (voice);
}

void			Zombie::announce(void) {

				std::cout << "<" << name << "(" << type << ")" << "> " << voice << std::endl;
				return;
}
