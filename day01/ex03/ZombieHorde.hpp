/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 18:48:43 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/01 21:42:47 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HORDE_HPP
# define ZOMBIE_HORDE_HPP
# include "Zombie.hpp"

class	ZombieHorde {

private:
	int			countZombie;
	Zombie 		*instances;

public:
	ZombieHorde(int countZombie);
	~ZombieHorde(void);
	void		announce(void);
};

#endif
