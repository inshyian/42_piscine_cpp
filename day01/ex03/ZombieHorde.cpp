/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/01 18:48:11 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/01 21:41:40 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int countZombie) {

	this->countZombie = countZombie;
	instances = new Zombie[countZombie];
	for (int i = 0; i < countZombie; i++)
	{
		instances[i].announce();
	}
	return;
}

ZombieHorde::~ZombieHorde() {

	return;
}
