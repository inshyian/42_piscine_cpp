/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.hpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 18:10:26 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 23:05:04 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRESEDENTIALPARDONFORM_HPP
# define PRESEDENTIALPARDONFORM_HPP

# include "Form.hpp"

class Bureaucrat;

class PresidentialPardonForm : public Form {

private:
	std::string		_target;
	PresidentialPardonForm();
	Form			&operator=(PresidentialPardonForm const &rhs);

public:
	PresidentialPardonForm(std::string target);
	PresidentialPardonForm(PresidentialPardonForm const &);
	virtual ~PresidentialPardonForm();

	void		execute(Bureaucrat const &executor) const;

	void		tellAboutZaphod() const;
};

#endif
