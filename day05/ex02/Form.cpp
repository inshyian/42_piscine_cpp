/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 15:19:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 19:50:44 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.hpp"

Form::Form(std::string name, int gradeToSign, int gradeToExecute) :
_name(name),
_gradeToSign(gradeToSign),
_gradeToExecute(gradeToExecute)
{};

Form::Form(Form const &src) :
_name(src._name),
_gradeToSign(src._gradeToSign),
_gradeToExecute(src._gradeToExecute)
{};

Form::~Form() {};

void		Form::beSigned(Bureaucrat const &bureaucrat) {

	if (bureaucrat.getGrade() <= _gradeToSign)
		_signed = true;
	else
		throw (GradeToSignTooLowException());
}

std::string const 	Form::getName() const {

	return (_name);
};

bool		Form::isSigned() const {

	return (_signed);
};

int			Form::getGradeToSign() const {

	return (_gradeToSign);
};

bool 		Form::canBeExecutedBy(const Bureaucrat &bureaucrat) const {

	if (this->_gradeToExecute >= bureaucrat.getGrade())
		return (true);
	else
		return (false);
};

int 		Form::getGradeToExecute() const {

	return (_gradeToExecute);
};

std::ostream			&operator<<(std::ostream &o, Form const &rhs) {

	o << "It's form " << rhs.getName() << ", needs grade " << rhs.getGradeToSign() << " to sign, and " << rhs.getGradeToExecute() << " to execute" << std::endl;
	return (o);
};

Form::GradeToSignTooLowException::GradeToSignTooLowException() {};

Form::GradeToSignTooLowException::GradeToSignTooLowException(GradeToSignTooLowException const &) {

};

Form::GradeToSignTooLowException::~GradeToSignTooLowException() throw() {};

Form::GradeToSignTooLowException	&Form::GradeToSignTooLowException::operator=(GradeToSignTooLowException const &) {

	return (*this);
}

const char* Form::GradeToSignTooLowException::what() const throw() {

	return "GradeToSignTooLowException";
}

Form::GradeToExecuteTooLowException::GradeToExecuteTooLowException() {};

Form::GradeToExecuteTooLowException::GradeToExecuteTooLowException(GradeToExecuteTooLowException const &) {};

Form::GradeToExecuteTooLowException::~GradeToExecuteTooLowException() throw() {};

Form::GradeToExecuteTooLowException	&Form::GradeToExecuteTooLowException::operator=(GradeToExecuteTooLowException const &) {

	return (*this);
}

const char* Form::GradeToExecuteTooLowException::what() const throw() {

	return "GradeToExecuteTooLowException";
}

Form::AttemptedToExecuteUnsignedForm::AttemptedToExecuteUnsignedForm() {};

Form::AttemptedToExecuteUnsignedForm::AttemptedToExecuteUnsignedForm(AttemptedToExecuteUnsignedForm const &) {};

Form::AttemptedToExecuteUnsignedForm::~AttemptedToExecuteUnsignedForm() throw() {};

Form::AttemptedToExecuteUnsignedForm	&Form::AttemptedToExecuteUnsignedForm::operator=(AttemptedToExecuteUnsignedForm const &) {

	return (*this);
}

const char* Form::AttemptedToExecuteUnsignedForm::what() const throw() {

	return "AttemptedToExecuteUnsignedForm";
}
