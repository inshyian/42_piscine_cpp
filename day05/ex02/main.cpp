/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 11:54:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/09 00:39:42 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"

void	ex02() {

	std::cout << "=+= ex02 tests ..." << std::endl;
	// sign 25, exec 5
	PresidentialPardonForm PresidentialPardonForm("Tree planter");
	// sign 72, exec 45
	RobotomyRequestForm RobotomyRequestForm("Robotazation");
	// sign 145, exec 137
	ShrubberyCreationForm ShrubberyCreationForm("Peter");

	Bureaucrat student("student", 150);
	student.signForm(ShrubberyCreationForm);
	student.executeForm(ShrubberyCreationForm);

	Bureaucrat trainee("trainee", 140);
	trainee.signForm(ShrubberyCreationForm);
	trainee.executeForm(ShrubberyCreationForm);

	Bureaucrat someone_137("someone_137", 137);
	someone_137.signForm(ShrubberyCreationForm);
	someone_137.executeForm(ShrubberyCreationForm);

	someone_137.signForm(RobotomyRequestForm);
	someone_137.signForm(ShrubberyCreationForm);

	Bureaucrat godMode("godMode_1", 1);
	godMode.signForm(PresidentialPardonForm);
	godMode.executeForm(PresidentialPardonForm);
	godMode.signForm(RobotomyRequestForm);
	godMode.executeForm(RobotomyRequestForm);
	godMode.signForm(ShrubberyCreationForm);
	godMode.executeForm(ShrubberyCreationForm);
}

int		main() {

	srand(time(NULL));
	ex02();
	return (0);
}
