/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 18:10:27 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 23:05:08 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) :
Form("ShrubberyCreationForm", 145, 137) {

	_target = target;
};

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const &src) :
Form("ShrubberyCreationForm", src.getGradeToSign(), src.getGradeToExecute()) {

	_target = src._target;
};

ShrubberyCreationForm::~ShrubberyCreationForm() {};

void		 ShrubberyCreationForm::execute(Bureaucrat const &executor) const {

	if (this->isSigned())
	{
		if (this->canBeExecutedBy(executor))
		{
			plantATree();
		}
		else
			throw (GradeToExecuteTooLowException());
	}
	else
	{
		throw (AttemptedToExecuteUnsignedForm());
	}
}


void		ShrubberyCreationForm::plantATree() const {

	std::string		filename;
	std::ofstream	ofs;

	filename = _target + "_shrubbery";
	ofs.open(filename);
	ofs << "  ,d" << std::endl;
	ofs << "  88" << std::endl;
	ofs << "MM88MMM 8b,dPPYba,  ,adPPYba,  ,adPPYba," << std::endl;
	ofs << "  88    88P'   \"Y8 a8P_____88 a8P_____88" << std::endl;
	ofs << "  88    88         8PP\"\"\"\"\"\"\" 8PP\"\"\"\"\"\"\"" << std::endl;
	ofs << "  88,   88         \"8b,   ,aa \"8b,   ,aa" << std::endl;
	ofs << "  \"Y888 88          `\"Ybbd8\"'  `\"Ybbd8\"'" << std::endl;
	ofs.close();
}
