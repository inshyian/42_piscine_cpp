/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 15:19:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 17:43:59 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORM_HPP
# define FORM_HPP

# include <iostream>
# include "Bureaucrat.hpp"

class Bureaucrat;

class Form {
private:
	std::string const	_name;
	bool				_signed;
	int	const			_gradeToSign;
	int	const			_gradeToExecute;

public:
	Form();
	Form(std::string name, int gradeToSign, int gradeToExecute);
	Form(Form const &src);
	~Form();

	Form				&operator=(Form const &rhs);

	void				beSigned(Bureaucrat &);

	std::string const	getName() const;
	bool				isSigned();
	int 				getGradeToSign() const;
	int 				getGradeToExecute() const;

	class GradeToSignTooLowException : public std::exception {

	public:
		GradeToSignTooLowException();
		GradeToSignTooLowException(GradeToSignTooLowException const &src);
		~GradeToSignTooLowException() throw();
		GradeToSignTooLowException	&operator=(GradeToSignTooLowException const &rhs);

		virtual const char		*what() const throw();
	};

	class GradeToExecuteTooLowException : public std::exception {

	public:
		GradeToExecuteTooLowException();
		GradeToExecuteTooLowException(GradeToExecuteTooLowException const &src);
		~GradeToExecuteTooLowException() throw();
		GradeToExecuteTooLowException	&operator=(GradeToExecuteTooLowException const &rhs);

		virtual const char		*what() const throw();
	};
};

std::ostream			&operator<<(std::ostream &o, Form const &rhs);

#endif
