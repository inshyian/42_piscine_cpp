/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 11:54:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/09 00:21:35 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"

void	ex01() {

	std::cout << "=+= ex01 tests ..." << std::endl;

	Bureaucrat kirk("Kirk", 1);
	Bureaucrat spock("Spock", 2);

	Form kirksForm("Form for Kirk", 1, 2);

	kirk.signForm(kirksForm);
	spock.signForm(kirksForm);

	Form spocksForm("Form for Spock", 2, 2);

	kirk.signForm(spocksForm);
	spock.signForm(spocksForm);
}

int		main() {

	ex01();
	return (0);
}
