/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 23:14:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/09 21:42:16 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OFFICEBLOCK_HPP
# define OFFICEBLOCK_HPP

# include "Bureaucrat.hpp"
# include "Form.hpp"
# include "PresidentialPardonForm.hpp"
# include "ShrubberyCreationForm.hpp"
# include "RobotomyRequestForm.hpp"
# include "Intern.hpp"

class OfficeBlock {

private:
	Intern		*_intern;
	Bureaucrat	*_bureaucratSigner;
	Bureaucrat	*_bureaucratExecutor;
	OfficeBlock(OfficeBlock const &);
	OfficeBlock		operator=(OfficeBlock const &rhs);

public:
	OfficeBlock();
	OfficeBlock(Intern *, Bureaucrat *, Bureaucrat *);
	~OfficeBlock();

	void		setIntern(Intern *);
	void		setBureaucratSigner(Bureaucrat *);
	void		setBureaucratExecutor(Bureaucrat *);
	void		doBureaucracy(std::string formName, std::string targetName);

	class TheOfficeIsNotStaffedException : public std::exception {

	public:
		TheOfficeIsNotStaffedException();
		TheOfficeIsNotStaffedException(TheOfficeIsNotStaffedException const &src);
		~TheOfficeIsNotStaffedException() throw();
		TheOfficeIsNotStaffedException	&operator=(TheOfficeIsNotStaffedException const &rhs);

		virtual const char		*what() const throw();
	};
};

#endif
