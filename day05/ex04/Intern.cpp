/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 20:07:06 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 23:45:35 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"

Intern::Intern() {};

Intern::Intern(Intern const &) {};

Intern				&Intern::operator=(Intern const &) {

	return (*this);
}

Intern::~Intern() {};

typedef Form	*(Intern::*Forms)(std::string const);

typedef struct	s_forms {

	std::string		formName;
	Forms			formCreator;
}					t_forms;

t_forms		array[] = {

	{"presedential pardon", &Intern::makePresidentialPardon},
	{"robotomy request", &Intern::makeRobotomyRequest},
	{"shrubbery creation", &Intern::makeShrubberyCreation},
	{"NULL", NULL}
};

Form		*Intern::makeForm(std::string const formName, std::string const formTarget) {

	int		i;

	i = 0;
	while (array[i].formCreator)
	{
		if (array[i].formName == formName)
		{
			std::cout << "Intern creates " << formName << " form" << std::endl;
			return ((this->*array[i].formCreator)(formTarget));
		}
		i++;
	}
	throw UnknownFormRequestedException();
	return (NULL);
}

Form		*Intern::makePresidentialPardon(std::string const formTarget) {

	return (new PresidentialPardonForm(formTarget));
}

Form		*Intern::makeRobotomyRequest(std::string const formTarget) {

	return (new RobotomyRequestForm(formTarget));
};

Form		*Intern::makeShrubberyCreation(std::string const formTarget) {

	return (new ShrubberyCreationForm(formTarget));
};

Intern::UnknownFormRequestedException::UnknownFormRequestedException() {};

Intern::UnknownFormRequestedException::UnknownFormRequestedException(UnknownFormRequestedException const &) {};

Intern::UnknownFormRequestedException::~UnknownFormRequestedException() throw() {};

Intern::UnknownFormRequestedException	&Intern::UnknownFormRequestedException::operator=(UnknownFormRequestedException const &) {

	return (*this);
}

const char* Intern::UnknownFormRequestedException::what() const throw() {

	return "UnknownFormRequestedException";
}
