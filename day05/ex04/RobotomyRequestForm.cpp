/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 18:10:28 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 23:04:51 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm(std::string target) :
Form("RobotomyRequestForm", 72, 45) {

	_target = target;
};

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const &src) :
Form("RobotomyRequestForm", src.getGradeToSign(), src.getGradeToExecute()) {

	_target = src._target;
};

RobotomyRequestForm::~RobotomyRequestForm() {};

void		 RobotomyRequestForm::execute(Bureaucrat const &executor) const {

	if (this->isSigned())
	{
		if (this->canBeExecutedBy(executor))
		{
			makeNoise();
		}
		else
			throw (GradeToExecuteTooLowException());
	}
	else
	{
		throw (AttemptedToExecuteUnsignedForm());
	}
}

void		RobotomyRequestForm::makeNoise() const {

	system("afplay ./resources/drill.mp3&");
	if (rand() % 2 == 0)
		std::cout << _target << " has been robotomized successfully." << std::endl;
	else
		std::cout << _target << " has been robotomized unsuccessfully." << std::endl;
}
