/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 18:26:08 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 19:30:59 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

#include <iostream>
#include <stdexcept>
#include "Form.hpp"

class Form;

class Bureaucrat {

private:
	Bureaucrat();
	Bureaucrat		&operator=(Bureaucrat const &rhs);
	std::string const		_name;
	int						_grade;

public:
	Bureaucrat(std::string name, int grade);
	Bureaucrat(Bureaucrat const &src);
	~Bureaucrat();

	std::string		getName() const;
	int				getGrade() const;

	void			incrementGrade();
	void			decrementGrade();

	void			signForm(Form &);

	void			executeForm(Form const &form);

	class GradeTooLowException : public std::exception {

	public:
		GradeTooLowException();
		GradeTooLowException(GradeTooLowException const &src);
		~GradeTooLowException() throw();
		GradeTooLowException	&operator=(GradeTooLowException const &rhs);

		virtual const char		*what() const throw();
	};

	class GradeTooHighException : public std::exception {

	public:
		GradeTooHighException();
		GradeTooHighException(GradeTooHighException const &src);
		~GradeTooHighException() throw();
		GradeTooHighException	&operator=(GradeTooHighException const &rhs);

		virtual const char		*what() const throw();
	};
};

std::ostream			&operator<<(std::ostream &o, Bureaucrat const &rhs);

#endif
