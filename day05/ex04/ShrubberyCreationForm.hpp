/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.hpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 18:10:27 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 23:05:07 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP

# include <fstream>
# include "Form.hpp"

class Bureaucrat;

class ShrubberyCreationForm : public Form {

private:
	std::string		_target;
	ShrubberyCreationForm();
	Form			&operator=(ShrubberyCreationForm const &rhs);

public:
	ShrubberyCreationForm(std::string target);
	ShrubberyCreationForm(ShrubberyCreationForm const &);
	virtual ~ShrubberyCreationForm();

	void		execute(Bureaucrat const &executor) const;

	void		plantATree() const;
};

#endif
