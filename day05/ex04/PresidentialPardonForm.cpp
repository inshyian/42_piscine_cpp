/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 18:10:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 23:05:47 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm(std::string target) :
Form("PresidentialPardonForm", 25, 5) {

	_target = target;
};

PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const &src) :
Form("PresidentialPardonForm", src.getGradeToSign(), src.getGradeToExecute()) {

	_target = src._target;
};

PresidentialPardonForm::~PresidentialPardonForm() {};

void		 PresidentialPardonForm::execute(Bureaucrat const &executor) const {

	if (this->isSigned())
	{
		if (this->canBeExecutedBy(executor))
		{
			tellAboutZaphod();
		}
		else
			throw (GradeToExecuteTooLowException());
	}
	else
	{
		throw (AttemptedToExecuteUnsignedForm());
	}
}

void		 PresidentialPardonForm::tellAboutZaphod() const {

	std::cout << _target << " has been pardoned by Zaphod Beeblebrox." << std::endl;
}
