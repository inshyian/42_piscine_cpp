/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 23:14:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/09 00:18:45 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OfficeBlock.hpp"

OfficeBlock::OfficeBlock() {

	_intern = NULL;
	_bureaucratSigner = NULL;
	_bureaucratExecutor = NULL;
};

OfficeBlock::OfficeBlock(Intern *intern, Bureaucrat *bureaucratSigner, Bureaucrat *bureaucratExecutor) : _intern(intern), _bureaucratSigner(bureaucratSigner), _bureaucratExecutor(bureaucratExecutor) {}

OfficeBlock::~OfficeBlock() {};

void		OfficeBlock::setIntern(Intern *intern) {

	_intern = intern;
}

void		OfficeBlock::setBureaucratSigner(Bureaucrat *bureaucratSigner) {

	_bureaucratSigner = bureaucratSigner;
}

void		OfficeBlock::setBureaucratExecutor(Bureaucrat *bureaucratExecutor) {

	_bureaucratExecutor = bureaucratExecutor;
}

void		OfficeBlock::doBureaucracy(std::string formName, std::string targetName) {

	Form	*form;

	if (_intern && _bureaucratSigner && _bureaucratExecutor)
	{
		form = _intern->makeForm(formName, targetName);
		_bureaucratSigner->signForm(*form);
		_bureaucratExecutor->executeForm(*form);
		delete form;
	}
	else
		throw TheOfficeIsNotStaffedException();
}

OfficeBlock::TheOfficeIsNotStaffedException::TheOfficeIsNotStaffedException() {};

OfficeBlock::TheOfficeIsNotStaffedException::TheOfficeIsNotStaffedException(TheOfficeIsNotStaffedException const &) {};

OfficeBlock::TheOfficeIsNotStaffedException::~TheOfficeIsNotStaffedException() throw() {};

OfficeBlock::TheOfficeIsNotStaffedException	&OfficeBlock::TheOfficeIsNotStaffedException::operator=(TheOfficeIsNotStaffedException const &) {

	return (*this);
}

const char* OfficeBlock::TheOfficeIsNotStaffedException::what() const throw() {

	return "TheOfficeIsNotStaffedException";
}
