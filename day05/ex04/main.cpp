/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 11:54:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/09 00:39:26 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "Intern.hpp"
#include "OfficeBlock.hpp"

void	ex04() {

	std::cout << "=+= ex04 tests ..." << std::endl;

	Intern      idiotOne;
    Bureaucrat  hermes = Bureaucrat("Hermes Conrad", 72);
    Bureaucrat  bob = Bureaucrat("Bobby Bobson", 45);
    OfficeBlock ob;

    ob.setIntern(&idiotOne);
    ob.setBureaucratSigner(&hermes);
    ob.setBureaucratExecutor(&bob);
	try
	{
		ob.doBureaucracy("robotomy request", "Pigley");
	}
	catch (OfficeBlock::TheOfficeIsNotStaffedException &e)
	{
        std::cout << e.what() << std::endl;
	}
	catch (Intern::UnknownFormRequestedException &e)
	{
		std::cout << e.what() << std::endl;
	}
	catch (Form::GradeToExecuteTooLowException &e)
	{
		std::cout << e.what() << std::endl;
	}
	catch (Form::GradeToSignTooLowException &e)
	{
		std::cout << e.what() << std::endl;
	}
	catch (Form::AttemptedToExecuteUnsignedForm &e)
	{
		std::cout << e.what() << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}
}

int		main() {

	srand(time(NULL));
	ex04();
	return (0);
}
