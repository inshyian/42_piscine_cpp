/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.hpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 18:10:29 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 23:03:41 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP

# include "Form.hpp"

class Bureaucrat;

class RobotomyRequestForm : public Form {

private:
	std::string		_target;
	RobotomyRequestForm();
	Form			&operator=(RobotomyRequestForm const &rhs);

public:
	RobotomyRequestForm(std::string target);
	RobotomyRequestForm(RobotomyRequestForm const &);
	virtual ~RobotomyRequestForm();

	void		execute(Bureaucrat const &executor) const;

	void		makeNoise() const;
};

#endif
