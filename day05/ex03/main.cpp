/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 11:54:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/09 00:39:32 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "Intern.hpp"

void	ex03() {

	std::cout << "=+= ex03 tests ..." << std::endl;
	Intern	a;

	try
	{
		delete a.makeForm("shrubber creation", "someone");
	}
	catch (Intern::UnknownFormRequestedException &e)
	{
		std::cout << e.what() << std::endl;
	}

	Bureaucrat godMode("godBureaucrat", 1);
	Form*   rrf;

	rrf = a.makeForm("presedential pardon", "someone");
	godMode.executeForm(*rrf);
	godMode.signForm(*rrf);
	godMode.executeForm(*rrf);
	delete rrf;
	rrf = a.makeForm("robotomy request", "sometwo");
	godMode.signForm(*rrf);
	godMode.executeForm(*rrf);
	delete rrf;
	rrf = a.makeForm("shrubbery creation", "somethree");
	godMode.signForm(*rrf);
	godMode.executeForm(*rrf);
	delete rrf;
}

int		main() {

	srand(time(NULL));
	ex03();
	return (0);
}
