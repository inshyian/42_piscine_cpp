/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 20:07:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 22:13:40 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INTERN_HPP
# define INTERN_HPP

# include <iostream>
# include "Form.hpp"

class Intern {

private:
	Intern		&operator=(Intern const &rhs);

public:
	Intern();
	~Intern();
	Intern(Intern const &);

	Form		*makeForm(std::string const formName, std::string const formTarget);

	Form		*makePresidentialPardon(std::string const formTarget);
	Form		*makeRobotomyRequest(std::string const formTarget);
	Form		*makeShrubberyCreation(std::string const formTarget);

	class UnknownFormRequestedException : public std::exception {

	public:
		UnknownFormRequestedException();
		UnknownFormRequestedException(UnknownFormRequestedException const &src);
		~UnknownFormRequestedException() throw();
		UnknownFormRequestedException	&operator=(UnknownFormRequestedException const &rhs);

		virtual const char		*what() const throw();
	};
};

#endif
