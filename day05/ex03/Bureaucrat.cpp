/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 18:26:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 19:50:47 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat() {};

Bureaucrat::Bureaucrat(std::string name, int grade) : _name(name), _grade(grade) {

}

Bureaucrat::Bureaucrat(Bureaucrat const &src) : _name(src._name) {

	*this = src;
}

Bureaucrat::~Bureaucrat() {};


Bureaucrat		&Bureaucrat::operator=(Bureaucrat const &rhs) {

	this->_grade = rhs._grade;
	return (*this);
}

std::string		Bureaucrat::getName() const {

	return (_name);
}

int				Bureaucrat::getGrade() const {

	return (_grade);
}

void			Bureaucrat::incrementGrade() {

	// try {
	//
	// 	if (_grade <= 1)
	// 		throw (GradeTooHighException());
	// 	else
	// 		_grade--;
	// }
	// catch (GradeTooHighException &e) {
	//
	// 	std::cout << e.what() << std::endl;
	// };
	if (_grade - 1 < 1)
		throw (GradeTooHighException());
	else
		_grade--;
}

void			Bureaucrat::decrementGrade() {

	// try {
	//
	// 	if (_grade >= 150)
	// 		throw (GradeTooLowException());
	// 	else
	// 		_grade++;
	// }
	// catch (GradeTooLowException &e) {
	//
	// 	std::cout << e.what() << std::endl;
	// };
	if (_grade + 1 > 150)
		throw (GradeTooHighException());
	else
		_grade++;
}

void			Bureaucrat::signForm(Form &form) {

	try {

		form.beSigned(*this);
		std::cout << this->getName() << " signs " << form.getName() << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << this->getName() << " can't sign " << form.getName() << " because " << e.what() << std::endl;
	}
}

void			Bureaucrat::executeForm(Form const &form) {

	try
	{
		form.execute(*this);
		std::cout << _name << " executes " << form.getName() << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << _name << " can't execute " << form.getName() << " because " << e.what() << std::endl;
	}
}

std::ostream			&operator<<(std::ostream &o, Bureaucrat const &rhs) {

	o << rhs.getName() << ", bureaucrat grade " << rhs.getGrade() << std::endl;
	return (o);
}

Bureaucrat::GradeTooLowException::GradeTooLowException() {};

Bureaucrat::GradeTooLowException::GradeTooLowException(GradeTooLowException const &) {

};

Bureaucrat::GradeTooLowException::~GradeTooLowException() throw() {};

Bureaucrat::GradeTooLowException	&Bureaucrat::GradeTooLowException::operator=(GradeTooLowException const &) {

	return (*this);
}

const char* Bureaucrat::GradeTooLowException::what() const throw() {

	return "GradeTooLowException";
}

Bureaucrat::GradeTooHighException::GradeTooHighException() {};

Bureaucrat::GradeTooHighException::GradeTooHighException(GradeTooHighException const &) {};

Bureaucrat::GradeTooHighException::~GradeTooHighException() throw() {};

Bureaucrat::GradeTooHighException	&Bureaucrat::GradeTooHighException::operator=(GradeTooHighException const &) {

	return (*this);
}

const char* Bureaucrat::GradeTooHighException::what() const throw() {

	return "GradeTooHighException";
}
