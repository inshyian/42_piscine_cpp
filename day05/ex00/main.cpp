/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 11:54:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/10 11:54:37 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

int		main() {

	std::cout << "=+= ex00 tests ..." << std::endl;
	try {

		Bureaucrat Test("Test me!", 2);

		std::cout << Test;
		Test.incrementGrade();
		std::cout << Test;
		Test.incrementGrade();
		std::cout << Test;
	}
	catch (Bureaucrat::GradeTooHighException &e) {

		std::cout << e.what() << std::endl;
	}
	try {

		Bureaucrat Test_2("Ordinary", 149);
		std::cout << Test_2;
		Test_2.decrementGrade();
		std::cout << Test_2;
		Test_2.decrementGrade();
		std::cout << Test_2;
	}
	catch (Bureaucrat::GradeTooLowException &e) {

		std::cout << e.what() << std::endl;
	}
	return (0);
}
