/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 19:27:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:35:07 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperMutant.hpp"

SuperMutant::SuperMutant() : Enemy(170, "Super Mutant") {

	std::cout << "Gaaah. Me want smash heads !" << std::endl;
	return ;
}

SuperMutant::SuperMutant(SuperMutant const &src) {

	std::cout << "Gaaah. Me want smash heads !" << std::endl;
	*this = src;
	return ;
}

SuperMutant::~SuperMutant() {

	std::cout << "Aaargh ..." << std::endl;
	return ;
}

SuperMutant			&SuperMutant::operator=(SuperMutant const &rhs) {

	if (this != &rhs)
	{
		_hp = rhs._hp;
		_type = rhs._type;
	}
	return (*this);
}

void				SuperMutant::takeDamage(int amount) {

	amount = amount > 3 ? amount - 3 : 0;
	_hp = _hp > amount ? _hp - amount : 0;
	return ;
}
