/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 19:25:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 19:31:36 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

class SuperMutant : public Enemy {

public:
	SuperMutant();
	SuperMutant(SuperMutant const &src);
	~SuperMutant();
	SuperMutant			&operator=(SuperMutant const &rhs);

	virtual void		takeDamage(int);
};
