/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 19:47:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:34:27 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character() {};

Character::Character(std::string const &name) : _name(name) {

	_ap = 40;
	_maxAp = 40;
	_recoverAp = 10;
	_wPtr = NULL;
	return ;
}

Character::Character(Character const &src) {

	*this = src;
	return ;
}

Character::~Character() {

	return ;
}

Character			&Character::operator=(Character const &rhs) {

	if (this != &rhs)
	{
		_name = rhs._name;
		_ap = rhs._ap;
		_maxAp = rhs._maxAp;
		_recoverAp = rhs._recoverAp;
	}
	return (*this);
}

void				Character::recoverAP() {

	_ap = (_maxAp - _ap) > _recoverAp ? _ap + _recoverAp : _ap + (_maxAp - _ap);
	return ;
}

void				Character::equip(AWeapon *weapon) {

	_wPtr = weapon;
	return ;
}

void				Character::attack(Enemy *enemy) {

	if (enemy && _ap >= _wPtr->getAPCost())
	{
		_ap = _ap - _wPtr->getAPCost();
		std::cout << _name << " attacks " << enemy->getType() << " with a " << _wPtr->getName() << std::endl;
		enemy->takeDamage(_wPtr->getDamage());
		_wPtr->attack();
		if (enemy->getHP() == 0)
			delete enemy;
	}
	return ;
}

std::string			Character::getName() const {

	return (_name);
}

AWeapon				*Character::viewWeapon() const {

	return (_wPtr);
}

int					Character::getAP() const {

	return (_ap);
}

std::ostream		&operator<<(std::ostream &o, Character const &rhs) {

	if (rhs.viewWeapon())
		o << rhs.getName() << " has " << rhs.getAP() << " AP and wields a " << rhs.viewWeapon()->getName() << std::endl;
	else
		o << rhs.getName() << " has " << rhs.getAP() << " AP and is unarmed" << std::endl;
	return (o);
}
