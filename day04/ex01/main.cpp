/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 18:59:22 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 20:49:20 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"
#include "PlasmaRifle.hpp"
#include "PowerFist.hpp"
#include "Enemy.hpp"
#include "RadScorpion.hpp"
#include "SuperMutant.hpp"
#include "Character.hpp"

void		checkEnemies() {

	std::cout << "=+= checkEnemies..." << std::endl;
	Enemy* rs = new RadScorpion();
	Enemy* sm = new SuperMutant();

	std::cout << rs->getType() << std::endl;
	std::cout << rs->getHP() << std::endl;
	rs->takeDamage(81);
	std::cout << rs->getHP() << std::endl;
	std::cout << sm->getType() << std::endl;
	std::cout << sm->getHP() << std::endl;
	sm->takeDamage(171);
	std::cout << sm->getHP() << std::endl;
}

void		checkWeapons() {

	std::cout << "=+= checkWeapons..." << std::endl;
	AWeapon* pr = new PlasmaRifle();
	pr->attack();

	AWeapon* pf = new PowerFist();

	pf->attack();
}

void		defaultChecks() {

	std::cout << "=+= defaultChecks..." << std::endl;
	Character* zaz = new Character("zaz");
	std::cout << *zaz;
	Enemy* b = new RadScorpion();
	AWeapon* pr = new PlasmaRifle();
	AWeapon* pf = new PowerFist();
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->equip(pf);
	zaz->attack(b);
	std::cout << *zaz;
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;
}

int			main() {

	defaultChecks();
	checkWeapons();
	checkEnemies();

	return (0);
}
