/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 18:27:34 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:34:19 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

AWeapon::AWeapon() {};

AWeapon::AWeapon(std::string const &name, int apcost, int damage) {

	_name = name;
	_apcost = apcost;
	_damage = damage;
	return ;
};

AWeapon::AWeapon(AWeapon const &src) {

	*this = src;
	return ;
};

AWeapon::~AWeapon() {

	return ;
};

AWeapon			&AWeapon::operator=(AWeapon const &rhs) {

	if (this != &rhs)
	{
		_name = rhs._name;
		_damage = rhs._damage;
		_apcost = rhs._apcost;
		_attackoutput = rhs._attackoutput;
	}
	return (*this);
};

std::string		AWeapon::getName() const {

	return (_name);
}

int				AWeapon::getAPCost() const {

	return (_apcost);
}

int				AWeapon::getDamage() const {

	return (_damage);
}
