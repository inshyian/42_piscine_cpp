/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 19:04:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 20:01:04 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMY_HPP
# define ENEMY_HPP

#include <iostream>

class Enemy {

protected:
	Enemy();
    int				_hp;
	std::string		_type;

public:
    Enemy(int hp, std::string const &type);
	Enemy(Enemy const &src);
    virtual ~Enemy();
	Enemy			&operator=(Enemy const &rhs);

    std::string		getType() const;
    int				getHP() const;

    virtual void	takeDamage(int);
};

#endif
