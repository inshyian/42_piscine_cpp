/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 18:58:13 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:34:52 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PowerFist.hpp"

PowerFist::PowerFist() : AWeapon("Power Fist", 8, 50) {

	_attackoutput = "* pschhh... SBAM! *";
	return ;
};

PowerFist::PowerFist(PowerFist const &src) {

	*this = src;
	return ;
};

PowerFist::~PowerFist() {

	return ;
};

PowerFist			&PowerFist::operator=(PowerFist const &rhs) {

	if (this != &rhs)
	{
		_name = rhs._name;
		_damage = rhs._damage;
		_apcost = rhs._apcost;
		_attackoutput = rhs._attackoutput;
	}
	return (*this);
};

void				PowerFist::attack() const {

	std::cout << _attackoutput << std::endl;
	return ;
}
