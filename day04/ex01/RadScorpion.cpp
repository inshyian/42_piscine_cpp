/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 19:34:06 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:34:59 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RadScorpion.hpp"

RadScorpion::RadScorpion() : Enemy(80, "RadScorpion") {

	std::cout << "* click click click *" << std::endl;
	return ;
}

RadScorpion::RadScorpion(RadScorpion const &src) {

	std::cout << "* click click click *" << std::endl;
	*this = src;
	return ;
}

RadScorpion::~RadScorpion() {

	std::cout << "* SPROTCH *" << std::endl;
	return ;
}

RadScorpion			&RadScorpion::operator=(RadScorpion const &rhs) {

	if (this != &rhs)
	{
		_hp = rhs._hp;
		_type = rhs._type;
	}
	return (*this);
}

void				RadScorpion::takeDamage(int amount) {

	_hp = _hp > amount ? _hp - amount : 0;
	return ;
}
