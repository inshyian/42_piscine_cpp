/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 19:12:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:34:35 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

Enemy::Enemy() {};

Enemy::Enemy(int hp, std::string const &type) : _hp(hp), _type(type) {

	return ;
}

Enemy::Enemy(Enemy const &src) {

	*this = src;
	return ;
}

Enemy::~Enemy() {

	return ;
}

Enemy			&Enemy::operator=(Enemy const &rhs) {

	if (this != &rhs)
	{
		_hp = rhs._hp;
		_type = rhs._type;
	}
	return (*this);
}

std::string		Enemy::getType() const {

	return (_type);
}

int				Enemy::getHP() const {

	return (_hp);
}

void			Enemy::takeDamage(int) {

}
