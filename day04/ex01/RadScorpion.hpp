/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 19:33:24 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 19:33:38 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

class RadScorpion : public Enemy {

public:
	RadScorpion();
	RadScorpion(RadScorpion const &src);
	~RadScorpion();
	RadScorpion			&operator=(RadScorpion const &rhs);

	virtual void		takeDamage(int);
};
