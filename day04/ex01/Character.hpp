/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 19:47:05 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 20:26:08 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"
#include "Enemy.hpp"

class Character {

private:
	Character();
	std::string		_name;
    int				_ap;
	int				_maxAp;
	int				_recoverAp;
	AWeapon			*_wPtr;

public:

    Character(std::string const & name);
	Character(Character const &src);
	~Character();
	Character				&operator=(Character const &rhs);

	void					recoverAP();
	void					equip(AWeapon *);
	void					attack(Enemy *);
	std::string virtual 	getName() const;
	AWeapon					*viewWeapon() const;
	int						getAP() const;
};

std::ostream		&operator<<(std::ostream &o, Character const &rhs);
