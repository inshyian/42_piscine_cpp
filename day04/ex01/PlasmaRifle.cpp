/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 18:40:36 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:34:44 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "PlasmaRifle.hpp"

PlasmaRifle::PlasmaRifle() : AWeapon("Plasma Rifle", 5, 21) {

	_attackoutput = "* piouuu piouuu piouuu *";
	return ;
};

PlasmaRifle::PlasmaRifle(PlasmaRifle const &src) {

	*this = src;
	return ;
};

PlasmaRifle::~PlasmaRifle() {

	return ;
};

PlasmaRifle			&PlasmaRifle::operator=(PlasmaRifle const &rhs) {

	if (this != &rhs)
	{
		_name = rhs._name;
		_damage = rhs._damage;
		_apcost = rhs._apcost;
		_attackoutput = rhs._attackoutput;
	}
	return (*this);
};

void				PlasmaRifle::attack() const {

	std::cout << _attackoutput << std::endl;
	return ;
}
