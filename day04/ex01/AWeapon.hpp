/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 18:13:23 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 19:09:12 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AWEAPON_HPP
# define AWEAPON_HPP

# include <iostream>

class AWeapon {

protected:
	AWeapon();
    std::string			_name;
	int					_apcost;
	int					_damage;
	std::string			_attackoutput;

public:
    AWeapon(std::string const &name, int apcost, int damage);
	AWeapon(AWeapon const &src);
    ~AWeapon();
	AWeapon				&operator=(AWeapon const &rhs);

    std::string			getName() const;

    int					getAPCost() const;
    int					getDamage() const;

    virtual void		attack() const = 0;
};

#endif
