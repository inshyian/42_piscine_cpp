/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 14:54:17 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 18:27:02 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
# define PEON_HPP
# include "Victim.hpp"

class Peon : public Victim {

private:
	Peon();

public:
	Peon(std::string name);
	Peon(Peon const &);
	~Peon();
	Peon			&operator=(Peon const &rhs);

	virtual void	getPolymorphed() const;
};

std::ostream		&operator<<(std::ostream &o, Peon const &rhs);

#endif
