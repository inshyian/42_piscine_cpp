/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 14:54:21 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 17:32:11 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_H
# define SORCERER_H
# include <iostream>
# include "Victim.hpp"

class Sorcerer {

private:
	Sorcerer();
	std::string		_name;
	std::string		_title;
	std::string		_introdution;


public:
	Sorcerer(std::string name, std::string title);
	Sorcerer(Sorcerer const &);
	~Sorcerer();
	Sorcerer		&operator=(Sorcerer const &rhs);

	std::string		getName() const;
	std::string		getIntroduce() const;

	void			polymorph(Victim const &) const;
};

std::ostream		&operator<<(std::ostream &o, Sorcerer const &rhs);

#endif
