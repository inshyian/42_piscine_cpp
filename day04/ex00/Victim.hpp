/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 14:54:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 18:23:52 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_H
# define VICTIM_H
# include <iostream>

class Victim {

protected:
	Victim();
	std::string		_name;
	std::string		_introdution;


public:
	Victim(std::string name);
	Victim(Victim const &);
	~Victim();
	Victim			&operator=(Victim const &rhs);

	std::string		getName() const;
	std::string		getIntroduce() const;

	virtual void	getPolymorphed() const;
};

std::ostream		&operator<<(std::ostream &o, Victim const &rhs);

#endif
