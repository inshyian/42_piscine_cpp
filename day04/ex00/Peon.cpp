/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 14:54:16 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:33:46 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.hpp"

Peon::Peon() {};

Peon::Peon(std::string name) : Victim(name) {

	std::cout << "Zog zog." << std::endl;
	return ;
};

Peon::Peon(Peon const &src) {

	*this = src;
	std::cout << "Zog zog." << std::endl;
	return ;
};

Peon::~Peon() {

	std::cout << "Bleuark..." << std::endl;
	return ;
};

Peon			&Peon::operator=(Peon const &rhs) {

	if (this != &rhs)
	{
		_name = rhs._name;
		_introdution = rhs._introdution;
	}
	return (*this);
};

void			Peon::getPolymorphed() const {

	std::cout << _name << " has been turned into a pink pony !" << std::endl;
	return ;
};

std::ostream	&operator<<(std::ostream &o, Peon const &rhs) {

	o << rhs.getIntroduce() << std::endl;
	return (o);
}
