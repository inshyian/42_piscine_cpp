/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 14:54:19 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:34:02 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"

Sorcerer::Sorcerer() {};

Sorcerer::Sorcerer(std::string name, std::string title) {

	_name = name;
	_title = title;
	_introdution = "I am " + _name + ", " + _title + ", and I like ponies";
	std::cout << _name << ", " << _title << ", is born !" << std::endl;
	return ;
};

Sorcerer::Sorcerer(Sorcerer const &src) {

	*this = src;
	std::cout << _name << ", " << _title << ", is born !" << std::endl;
	return ;
};

Sorcerer::~Sorcerer() {

	std::cout << _name << ", " << _title << ", is dead. Consequences will never be the same !" << std::endl;
	return ;
};

Sorcerer		&Sorcerer::operator=(Sorcerer const &rhs) {

	if (this != &rhs)
	{
		_name = rhs._name;
		_title = rhs._title;
		_introdution = rhs._introdution;
	}
	return (*this);
};

std::string		Sorcerer::getName() const {

	return (this->_name);
};

std::string		Sorcerer::getIntroduce() const {

	return (this->_introdution);
};

std::ostream	&operator<<(std::ostream &o, Sorcerer const &rhs) {

	o << rhs.getIntroduce() << std::endl;
	return (o);
}

void			Sorcerer::polymorph(const Victim &victim) const {

	// std::cout << "Stop " << victim.getName() << ". I'm " << _name << ", and I will polymorph you" << std::endl;
	victim.getPolymorphed();
	return ;
}
