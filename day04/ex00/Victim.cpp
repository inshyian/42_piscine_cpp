/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 14:54:23 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:34:11 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

Victim::Victim() {};

Victim::Victim(std::string name) {

	_name = name;
	_introdution = "I'm " + _name + " and I like otters !";
	std::cout << "Some random victim called " << _name << " just popped !" << std::endl;
	return ;
};

Victim::Victim(Victim const &src) {

	*this = src;
	std::cout << "Some random victim called " << _name << " just popped !" << std::endl;
	return ;
};

Victim::~Victim() {

	std::cout << "Victim " << _name << " just died for no apparent reason !" << std::endl;
	return ;
};

Victim			&Victim::operator=(Victim const &rhs) {

	if (this != &rhs)
	{
		_name = rhs._name;
		_introdution = rhs._introdution;
	}
	return (*this);
};

std::string		Victim::getName() const {

	return (this->_name);
};

std::string		Victim::getIntroduce() const {

	return (this->_introdution);
};

std::ostream	&operator<<(std::ostream &o, Victim const &rhs) {

	o << rhs.getIntroduce() << std::endl;
	return (o);
}

void			Victim::getPolymorphed() const {

	std::cout << _name << " has been turned into a cute little sheep !" << std::endl;
	return ;
};
