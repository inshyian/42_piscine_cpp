/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:18 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 18:24:24 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.hpp"

Cure::Cure() : AMateria("cure") {

	return ;
}

Cure::Cure(Cure const &src) {

	*this = src;
	return ;
}

Cure::~Cure() {

	return ;
}

Cure					&Cure::operator=(Cure const &rhs) {

	if (this != &rhs)
	{
		_xp = rhs._xp;
	}
	return (*this);
}

void 					Cure::use(ICharacter &target) {

	std::cout << "* heals " << target.getName() << "’s wounds *" << std::endl;
	return ;
}

Cure 					*Cure::clone() const {

	return (new Cure());
}
