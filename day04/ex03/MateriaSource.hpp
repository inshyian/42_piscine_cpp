/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:23 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 18:02:15 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATERIASOURCE_HPP
# define MATERIASOURCE_HPP

# include <iostream>
# include "IMateriaSource.hpp"

class MateriaSource : public IMateriaSource {

private:
	AMateria						*_source[4];
	int								_maxSources;

public:
	MateriaSource();
	MateriaSource(MateriaSource const &);
	~MateriaSource();
	MateriaSource				&operator=(MateriaSource const &rhs);

	virtual void				learnMateria(AMateria *);
	virtual AMateria			*createMateria(std::string const &type);
};

#endif
