/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:17 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 18:00:07 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character() {};

Character::Character(std::string name) {

	_name = name;
	_eq[0] = NULL;
	_eq[1] = NULL;
	_eq[2] = NULL;
	_eq[3] = NULL;
	_maxEqs = 4;
	return ;
}

Character::Character(Character const &src) {

	*this = src;
	return ;
}

Character::~Character() {

	return ;
}

Character				&Character::operator=(Character const &rhs) {

	if (this != &rhs)
	{
		_name = rhs._name;
		for (int i = 0; i < _maxEqs; i++)
		{
			if (rhs._eq[i])
				_eq[i] = rhs._eq[i]->clone();
			else
				_eq[i] = rhs._eq[i];
		}
	}
	return (*this);
}

std::string const 		&Character::getName() const {

	return (_name);
}

void					Character::equip(AMateria *materia) {

	int		i;

	i = 0;
	while (i < _maxEqs)
	{
		if (!_eq[i])
		{
			_eq[i] = materia;
			break ;
		}
		i++;
	}
	return ;
}

void					Character::unequip(int idx) {

	_eq[idx] = NULL;
	return ;
}

void					Character::use(int idx, ICharacter &target) {

	if (_eq[idx])
	{
		_eq[idx]->use(target);
	}
}
