/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:20 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 18:24:04 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Ice.hpp"

Ice::Ice() : AMateria("ice") {

	return ;
}

Ice::Ice(Ice const &src) {

	*this = src;
	return ;
}

Ice::~Ice() {

	return ;
}

Ice						&Ice::operator=(Ice const &rhs) {

	if (this != &rhs)
	{
		_xp = rhs._xp;
	}
	return (*this);
}

void 					Ice::use(ICharacter &target) {

	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
	return ;
}

Ice 					*Ice::clone() const {

	return (new Ice());
}
