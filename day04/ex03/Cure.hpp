/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:19 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 16:21:36 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CURE_HPP
# define CURE_HPP

# include <iostream>
# include "AMateria.hpp"

class Cure : public AMateria {

private:
	std::string		_type;
    unsigned int	_xp;

public:
	Cure();
	Cure(Cure const &src);
	virtual ~Cure();
	Cure					&operator=(Cure const &rhs);

	virtual Cure			*clone() const;
	virtual void 			use(ICharacter &target);
};

#endif
