/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:23 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 18:23:05 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <iostream>
# include "MateriaSource.hpp"

MateriaSource::MateriaSource() {

	_source[0] = NULL;
	_source[1] = NULL;
	_source[2] = NULL;
	_source[3] = NULL;
	_maxSources = 4;
	return ;
};

MateriaSource::MateriaSource(MateriaSource const &src) {

	*this = src;
}

MateriaSource::~MateriaSource() {

	for (int i = 0; i < _maxSources; i++)
	{
		if (_source[i])
			delete _source[i];
	}
};

MateriaSource		&MateriaSource::operator=(MateriaSource const &rhs) {

	if (this != &rhs)
	{
		for (int i = 0; i < _maxSources; i++)
		{
			if (rhs._source[i])
				_source[i] = rhs._source[i]->clone();
			else
				_source[i] = rhs._source[i];
		}
	}
	return (*this);
}

void				MateriaSource::learnMateria(AMateria *materia) {

	int		i;

	i = 0;
	while (i < _maxSources)
	{
		if (!_source[i])
		{
			_source[i] = materia;
			break ;
		}
		i++;
	}
	return ;
}

AMateria			*MateriaSource::createMateria(std::string const &type) {

	for (int i = 0; i < _maxSources; i++)
	{
		if (_source[i] && _source[i]->getType() == type)
			return (_source[i]->clone());
	}
	return (0);
}
