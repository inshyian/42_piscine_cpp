/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:12 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 17:57:56 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"

AMateria::AMateria() {};

AMateria::AMateria(std::string const &type) {

	_type = type;
	return ;
}

AMateria::AMateria(AMateria const &src) {

	*this = src;
	return ;
}

AMateria::~AMateria() {

	return ;
}

AMateria				&AMateria::operator=(AMateria const &rhs) {

	if (this != &rhs)
	{
		_xp = rhs._xp;
		_type = rhs._type;
	}
	return (*this);
}

std::string const 		&AMateria::getType() const {

	return (_type);
}

unsigned int 			AMateria::getXP() const {

	return (_xp);
}
