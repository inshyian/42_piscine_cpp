/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 14:23:45 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 17:54:02 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

# include "ICharacter.hpp"

class Character : public ICharacter {

private:
	std::string						_name;
	AMateria						*_eq[4];
	int								_maxEqs;

public:
	Character();
	Character(std::string name);
	Character(Character const &src);
	~Character();
	Character						&operator=(Character const &rhs);

	virtual std::string const 		&getName() const;
	virtual void					equip(AMateria *m);
	virtual void					unequip(int idx);
	virtual void					use(int idx, ICharacter &target);
};

#endif
