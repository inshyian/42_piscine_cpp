/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 17:13:46 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMATERIA_HPP
# define AMATERIA_HPP

# include <iostream>
# include "ICharacter.hpp"

class ICharacter;

class AMateria {

private:
	std::string		_type;
    unsigned int	_xp;

public:
	AMateria();
	AMateria(std::string const &type);
	AMateria(AMateria const &src);
	virtual ~AMateria();
	AMateria				&operator=(AMateria const &rhs);

	std::string const 		&getType() const; 		// Returns the materia type
	unsigned int 			getXP() const; 			// Returns the Materia's XP
	virtual AMateria		*clone() const = 0;
	virtual void 			use(ICharacter &target) = 0;
};

#endif
