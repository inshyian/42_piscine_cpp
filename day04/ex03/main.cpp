/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:22 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 18:28:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"
#include "Character.hpp"
#include "ICharacter.hpp"
#include "Ice.hpp"
#include "Cure.hpp"

void		additionalTests() {

	std::cout << "=+= Additional tests ..." << std::endl;

	std::cout << "=+= create empty source:" << std::endl;
	IMateriaSource* src = new MateriaSource();

	if (!src->createMateria("cure"))
		std::cout << "no cure materia" << std::endl;
	if (!src->createMateria("ice"))
		std::cout << "no ice materia" << std::endl;
	std::cout << "=+= add ice materia:" << std::endl;
	src->learnMateria(new Ice());
	src->learnMateria(new Ice());
	src->learnMateria(new Ice());
	src->learnMateria(new Ice());
	src->learnMateria(new Ice());
	if (!src->createMateria("cure"))
		std::cout << "no cure materia" << std::endl;
	if (src->createMateria("ice"))
		std::cout << "materia ice exists" << std::endl;

	std::cout << "=+= make copy of source:" << std::endl;
	IMateriaSource* src_(src);

	if (!src_->createMateria("cure"))
		std::cout << "no cure materia in dup" << std::endl;
	if (src_->createMateria("ice"))
		std::cout << "materia ice exists in dup" << std::endl;
}

int main() {

	IMateriaSource* src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());

	ICharacter* zaz = new Character("zaz");
	AMateria* tmp;
	tmp = src->createMateria("ice");
	zaz->equip(tmp);
	tmp = src->createMateria("cure");
	zaz->equip(tmp);

	ICharacter* bob = new Character("bob");
	zaz->use(0, *bob);
	zaz->use(1, *bob);

	delete bob;
	delete zaz;
	delete src;

	additionalTests();

	return 0;
}
