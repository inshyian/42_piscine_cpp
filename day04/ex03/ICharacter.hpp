/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ICharacter.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:21 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 17:10:54 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ICHARACTER_HPP
# define ICHARACTER_HPP

# include <iostream>
# include "AMateria.hpp"

class AMateria;

class ICharacter {

public:
	virtual ~ICharacter() {}
	virtual std::string const 		&getName() const = 0;
	virtual void					equip(AMateria *m) = 0;
	virtual void					unequip(int idx) = 0;
	virtual void					use(int idx, ICharacter &target) = 0;
};

#endif
