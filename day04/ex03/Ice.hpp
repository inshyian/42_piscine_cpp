/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/05 12:52:21 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 16:21:39 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ICE_HPP
# define ICE_HPP

# include <iostream>
# include "AMateria.hpp"

class Ice : public AMateria {

private:
	std::string		_type;
    unsigned int	_xp;

public:
	Ice();
	Ice(Ice const &src);
	virtual ~Ice();
	Ice						&operator=(Ice const &rhs);

	virtual Ice				*clone() const;
	virtual void 			use(ICharacter &target);
};

#endif
