/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 20:57:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 23:09:58 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
# define SQUAD_HPP

# include "ISquad.hpp"

class Squad : public ISquad {

protected:
	typedef struct	s_container	{

		ISpaceMarine			*unit;
		struct s_container		*next;
	}				t_container;
	t_container		*_units;
	int				_unitsCount;
	void			destroyUnits();
	void			copyUnits(Squad const &squad);
	bool			unitAlreadyPushed(t_container *, ISpaceMarine*);

public:
	Squad();
	Squad(Squad const &src);
	~Squad();
	Squad					&operator=(Squad const &rhs);

	virtual int				getCount() const;
	virtual ISpaceMarine*	getUnit(int) const;
	virtual int				push(ISpaceMarine*);

};

#endif
