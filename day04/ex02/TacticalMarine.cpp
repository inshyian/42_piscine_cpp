/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 20:57:27 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 23:35:25 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TacticalMarine.hpp"

TacticalMarine::TacticalMarine() {

	std::cout << "Tactical Marine ready for battle" << std::endl;
	return ;
};

TacticalMarine::TacticalMarine(TacticalMarine const &) {

	std::cout << "Tactical Marine ready for battle" << std::endl;
	return ;
};

TacticalMarine::~TacticalMarine() {

	std::cout << "Aaargh ..." << std::endl;
	return ;
};

TacticalMarine		&TacticalMarine::operator=(TacticalMarine const &) {

	return (*this);
};

ISpaceMarine		*TacticalMarine::clone() const {

	return (new TacticalMarine);
}

void				TacticalMarine::battleCry() const {

	std::cout << "For the holy PLOT !" << std::endl;
	return ;
}

void				TacticalMarine::rangedAttack() const {

	std::cout << "* attacks with bolter *" << std::endl;
	return ;
}

void				TacticalMarine::meleeAttack() const {

	std::cout << "* attacks with chainsword *" << std::endl;
	return ;
}
