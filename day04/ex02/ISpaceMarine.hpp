/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ISpaceMarine.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 20:57:20 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 23:34:48 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ISPACEMARINE_HPP
# define ISPACEMARINE_HPP

# include <iostream>

class ISpaceMarine {

public:
   virtual ~ISpaceMarine() {}
   virtual ISpaceMarine* clone() const = 0;
   virtual void		battleCry() const = 0;
   virtual void		rangedAttack() const = 0;
   virtual void		meleeAttack() const = 0;
};

#endif
