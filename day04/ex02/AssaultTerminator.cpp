/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 20:57:14 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 23:37:36 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AssaultTerminator.hpp"

AssaultTerminator::AssaultTerminator() {

	std::cout << "* teleports from space *" << std::endl;
	return ;
};

AssaultTerminator::AssaultTerminator(AssaultTerminator const &) {

	std::cout << "* teleports from space *" << std::endl;
	return ;
};

AssaultTerminator::~AssaultTerminator() {

	std::cout << "I’ll be back ..." << std::endl;
	return ;
};

AssaultTerminator		&AssaultTerminator::operator=(AssaultTerminator const &) {

	return (*this);
};

ISpaceMarine			*AssaultTerminator::clone() const {

	return (new AssaultTerminator);
}

void					AssaultTerminator::battleCry() const {

	std::cout << "This code is unclean. PURIFY IT !" << std::endl;
	return ;
}

void					AssaultTerminator::rangedAttack() const {

	std::cout << "* does nothing *" << std::endl;
	return ;
}

void					AssaultTerminator::meleeAttack() const {

	std::cout << "* attacks with chainfists *" << std::endl;
	return ;
}
