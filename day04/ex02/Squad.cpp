/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/04 20:57:24 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/05 15:36:29 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"

Squad::Squad() {

	_units = NULL;
	_unitsCount = 0;
};

Squad::Squad(Squad const &src) {

	*this = src;
}

Squad::~Squad() {

	destroyUnits();
};

Squad					&Squad::operator=(Squad const &rhs) {

	if (this != &rhs)
	{
		destroyUnits();
		copyUnits(rhs);
	}
	return (*this);
}

int						Squad::getCount() const {

	return (_unitsCount);
}

ISpaceMarine			*Squad::getUnit(int num) const {

	t_container		*tmp;

	tmp = _units;
	if (!(num < 0 || num >= _unitsCount))
	{
		while (num--)
		{
			tmp = tmp->next;
		}
		return (tmp->unit);
	}
	return (NULL);
}

int						Squad::push(ISpaceMarine *sm) {

	t_container		*tmp;

	tmp = _units;
	if (sm && !unitAlreadyPushed(_units, sm))
	{
		if (tmp)
		{
			while (tmp->next)
			{
				tmp = tmp->next;
			}
			tmp->next = new t_container;
			tmp = tmp->next;
			tmp->unit = sm;
			tmp->next = NULL;
		}
		else
		{
			_units = new t_container;
			_units->unit = sm;
			_units->next = NULL;
		}
		_unitsCount++;
	}
	return (_unitsCount);
}

void					Squad::copyUnits(Squad const &squad) {

	int		i;

	i = 0;
	while (i < squad.getCount())
	{
		push(squad.getUnit(i));
		i++;
	}
	return ;
}

void					Squad::destroyUnits() {

	t_container		*tmp;

	while (_units)
	{
		tmp = _units;
		_units = _units->next;
		delete tmp->unit;
		delete tmp;
	}
	return ;
}

bool					Squad::unitAlreadyPushed(t_container *cnt, ISpaceMarine *sm) {

	if (cnt == NULL)
		return (false);
	else if (cnt->unit == sm)
		return (true);
	else
		return (unitAlreadyPushed(cnt->next, sm));
}
