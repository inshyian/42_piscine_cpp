/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 22:52:19 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/10 23:27:42 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define ARRAY_SIZE 10

#include <iostream>

template<typename T>
static void		function(T &item) {

	item = item + 1;
}

template<typename T>
static void		iter(T *array, size_t size, void (*f)(T &)) {

	if (array)
	{
		while (size)
		{
			size--;
			f(array[size]);
		}
	}
}

int				main() {

	std::cout << "tests ex01..." << std::endl;

	int		int_array[ARRAY_SIZE] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
	char	char_array[ARRAY_SIZE] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};

	std::cout << "before iteration:" << std::endl;
	for (int i = 0; i < ARRAY_SIZE; i++) {

		std::cout << int_array[i] << " " << char_array[i] << ", ";
	}
	std::cout << std::endl;

	iter(int_array, ARRAY_SIZE, function);
	iter(char_array, ARRAY_SIZE, function);

	std::cout << "after iteration:" << std::endl;
	for (int i = 0; i < ARRAY_SIZE; i++) {

		std::cout << int_array[i] << " " << char_array[i] << ", ";
	}
	std::cout << std::endl;
	return (0);
}
