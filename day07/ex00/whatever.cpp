/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whatever.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 22:36:23 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 01:17:14 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template<typename T>
T		max(T &a, T &b) {

	return (a > b ? a : b);
}

template<typename T>
T		min(T &a, T &b) {

	return (a < b ? a : b);
}

template<typename T>
void	swap(T &a, T &b) {

	T	tmp;

	tmp = a;
	a = b;
	b = tmp;
}

void	myTests() {

	std::cout << "my tests..." << std::endl;

	int		a_int = 1;
	int		b_int = 2;

	std::cout << "before swap: a_int == " << a_int << ", b_int == " << b_int << std::endl;
	swap(a_int, b_int);
	std::cout << "after swap: a_int == " << a_int << ", b_int == " << b_int << std::endl;

	char	a_char = 'a';
	char	b_char = 'b';

	std::cout << "before swap: a_char == " << a_char << ", b_char == " << b_char << std::endl;
	swap(b_char, a_char);
	std::cout << "after swap: a_char == " << a_char << ", b_char == " << b_char << std::endl;

	std::cout << "biggest int: " << max(a_int, b_int) << std::endl;
	std::cout << "biggest char: " << max(b_char, a_char) << std::endl;

	std::cout << "smallest int: " << min(a_int, b_int) << std::endl;
	std::cout << "smallest char: " << min(b_char, a_char) << std::endl;
}

int		main() {

	std::cout << "tests ex00..." << std::endl;
	std::cout << "tests from exercise..." << std::endl;

 	int a = 2;
	int b = 3;
	::swap( a, b );
	std::cout << "a = " << a << ", b = " << b << std::endl;
	std::cout << "min( a, b ) = " << ::min( a, b ) << std::endl;
	std::cout << "max( a, b ) = " << ::max( a, b ) << std::endl;
	std::string c = "chaine1";
	std::string d = "chaine2";
	::swap(c, d);
	std::cout << "c = " << c << ", d = " << d << std::endl;
	std::cout << "min( c, d ) = " << ::min( c, d ) << std::endl;
	std::cout << "max( c, d ) = " << ::max( c, d ) << std::endl;

	std::cout << std::endl;
	myTests();

	return (0);
}
