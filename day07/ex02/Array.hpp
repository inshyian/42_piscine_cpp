/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 23:29:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 01:12:25 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARRAY_HPP
# define ARRAY_HPP

# include <iostream>

template<typename T>
class Array {

public:
	Array<T>() : _data(nullptr), _size(0) {};

	Array<T>(unsigned int n) : _data(new T[n]), _size(n) {};

	Array<T>(Array<T> const &rhs) {

		*this = rhs;
	};

	~Array<T>() {

		if (_data)
			delete[] _data;
	};

	Array<T>	&operator=(Array<T> const &src) {

		if (_data)
			delete[] _data;
		_size = src._size;
		if (_size)
		{
			_data = new T[src._size];
			copyElements(src._data);
		}
		else
			_data = nullptr;
		return (*this);
	};

	T			&operator[](unsigned int const num) {

		if (num > (_size - 1))
			throw (InvalidItemIndexException());
		return (_data[num]);
	};

	class InvalidItemIndexException : public std::exception {

	public:
		InvalidItemIndexException() {};
		InvalidItemIndexException(InvalidItemIndexException const &src);
		~InvalidItemIndexException() throw() {};
		InvalidItemIndexException	&operator=(InvalidItemIndexException const &rhs);

		virtual const char		*what() const throw() {

			return "InvalidItemIndexException";
		};
	};

private:
	void				copyElements(T *data) {

		unsigned int	i = 0;
		while (i < _size)
		{
			_data[i] = data[i];
			i++;
		}
	};

	T					*_data;
	unsigned int		_size;

	const unsigned int	&size() const {

		return (_size);
	}
};

#endif
