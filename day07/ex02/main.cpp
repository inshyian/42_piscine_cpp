/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 23:29:06 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 01:19:25 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Array.hpp"

int		main() {

	std::cout << "tests ex02..." << std::endl;
	Array	<int>array(1);

	array[0] = 1;
	std::cout << array[0] << std::endl;
	try
	{
		array[1] = 1;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}

	Array	<int>array_2;

	array_2 = array;

	std::cout << array_2[0] << std::endl;

	array_2[0] = 15;
	std::cout << array_2[0] << std::endl;
	std::cout << array[0] << std::endl;

	Array	<std::string>string_array(10);

	string_array[0] = "String zero";
	string_array[9] = "String nine";

	try
	{
		string_array[10] = "String ten";
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << string_array[0] << std::endl;
	std::cout << string_array[9] << std::endl;
	return (0);
}
