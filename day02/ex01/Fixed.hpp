/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/03 21:14:46 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/02 10:26:39 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP
# include <iostream>
# include <cmath>

class Fixed {

private:
	int					raw;
	static const int	fractionBitsNum;

public:
	Fixed(void); 										// Cannonical Constructor
	Fixed(int n);
	Fixed(float f);
	Fixed(Fixed const &rhs); 							// Cannonical Copy Constructor
	~Fixed(void); 										// Cannonical Destructor

	Fixed				&operator=(Fixed const &rhs); 	// Cannonical Operator Overload

	int					getRawBits(void) const;
	void				setRawBits(int const raw);
	float				toFloat(void) const;
	int					toInt(void) const;
};

std::ostream			&operator<<(std::ostream &o, Fixed const &rhs);

#endif
