/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/03 21:14:46 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/02 13:17:27 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP
# include <iostream>
# include <cmath>

class Fixed {

private:
	int					raw;
	static const int	fractionBitsNum;

public:
	Fixed(void); 										// Cannonical Constructor
	Fixed(int n);
	Fixed(float f);
	Fixed(Fixed const &rhs); 							// Cannonical Copy Constructor
	~Fixed(void); 										// Cannonical Destructor

	int					getRawBits(void) const;
	void				setRawBits(int const raw);
	float				toFloat(void) const;
	int					toInt(void) const;
	Fixed				&operator=(Fixed const &rhs); 	// Cannonical Operator Overload
	Fixed				operator+(Fixed const &rhs) const;
	Fixed				operator-(Fixed const &rhs) const;
	Fixed				operator*(Fixed const &rhs) const;
	Fixed				operator/(Fixed const &rhs) const;
	bool				operator>(Fixed const &rhs) const;
	bool				operator<(Fixed const &rhs) const;
	bool				operator>=(Fixed const &rhs) const;
	bool				operator<=(Fixed const &rhs) const;
	bool				operator==(Fixed const &rhs) const;
	bool				operator!=(Fixed const &rhs) const;
	Fixed				&operator++(void);
	Fixed				&operator--(void);
	Fixed				operator++(int);
	Fixed				operator--(int);
	static Fixed		&min(Fixed &a, Fixed &b);
	static Fixed		&max(Fixed &a, Fixed &b);
	static const Fixed	&min(Fixed const &a, const Fixed &b);
	static const Fixed	&max(Fixed const &a, const Fixed &b);

};

std::ostream			&operator<<(std::ostream &o, Fixed const &rhs);

#endif
