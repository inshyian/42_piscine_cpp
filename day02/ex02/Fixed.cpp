/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/03 21:14:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/03 09:39:51 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"

/*
**  Class static vars next...
*/

const int Fixed::fractionBitsNum = 8;

/*
**  Public functions next...
*/

Fixed::Fixed(void) {

	std::cout << "Default constructor called" << std::endl;
	raw = 0;
	return ;
}

Fixed::~Fixed(void) {

	std::cout << "Default destructor called" << std::endl;
	return ;
}

Fixed::Fixed(int n) {

	std::cout << "Int constructor called" << std::endl;
	this->raw = (n << fractionBitsNum);
	return ;
}

Fixed::Fixed(float f) {

	std::cout << "Float constructor called" << std::endl;
	this->raw = (int)roundf(f * (1 << fractionBitsNum));
	return ;
}

Fixed::Fixed(Fixed const &rhs) {

	std::cout << "Copy constructor called" << std::endl;
	this->raw = rhs.getRawBits();
	return ;
}

int				Fixed::getRawBits(void) const {

	std::cout << __func__ << " member function called" << std::endl;
	return (this->raw);
}

void			Fixed::setRawBits(int const raw) {

	std::cout << __func__ << " member function called" << std::endl;
	this->raw = raw;
	return ;
}

float			Fixed::toFloat(void) const {

	return ((float)this->raw / (1 << fractionBitsNum));
}

int				Fixed::toInt(void) const {

	return (this->raw >> fractionBitsNum);
}

Fixed			&Fixed::operator=(Fixed const &rhs) {

	std::cout << "Assignation operator called" << std::endl;
	if (this != &rhs)
	{
		this->raw = rhs.getRawBits();
	}
	return (*this);
}

Fixed			Fixed::operator+(Fixed const &rhs) const {

	Fixed		fixed;

	fixed.raw = this->raw + rhs.getRawBits();
	return (fixed);
}

Fixed			Fixed::operator-(Fixed const &rhs) const {

	Fixed		fixed;

	fixed.raw = this->raw - rhs.getRawBits();
	return (fixed);
}

Fixed			Fixed::operator*(Fixed const &rhs) const {

	int64_t		a;
	int64_t		b;
	Fixed		fixed;

	a = this->raw;
	b = rhs.getRawBits();
	fixed.raw = a * b >> fractionBitsNum;
	return (fixed);
}

Fixed			Fixed::operator/(Fixed const &rhs) const {

	int64_t		a;
	int64_t		b;
	Fixed		fixed;

	a = this->raw;
	b = rhs.getRawBits();
	fixed.raw = (a << fractionBitsNum) / b;
	return (fixed);
}

bool			Fixed::operator>(Fixed const &rhs) const {

	return (this->raw > rhs.getRawBits());
}

bool			Fixed::operator<(Fixed const &rhs) const {

	return (this->raw < rhs.getRawBits());
}

bool			Fixed::operator>=(Fixed const &rhs) const {

	return (this->raw >= rhs.getRawBits());
}

bool			Fixed::operator<=(Fixed const &rhs) const {

	return (this->raw <= rhs.getRawBits());
}

bool			Fixed::operator==(Fixed const &rhs) const {

	return (this->raw == rhs.getRawBits());
}

bool			Fixed::operator!=(Fixed const &rhs) const {

	return (this->raw != rhs.getRawBits());
}

Fixed			&Fixed::operator++(void) {

	this->raw++;
	return (*this);
}

Fixed			&Fixed::operator--(void) {

	this->raw--;
	return (*this);
}

Fixed			Fixed::operator++(int) {

	Fixed		tmp(*this);

	this->raw++;
	return (tmp);
}

Fixed			Fixed::operator--(int) {

	Fixed		tmp(*this);

	this->raw--;
	return (tmp);
}

Fixed			&Fixed::min(Fixed &a, Fixed &b) {

	return (a < b ? a : b);
}

Fixed			&Fixed::max(Fixed &a, Fixed &b) {

	return (a > b ? a : b);
}

const Fixed		&Fixed::min(Fixed const &a, Fixed const &b) {

	return (a < b ? a : b);
}

const Fixed		&Fixed::max(Fixed const &a, const Fixed &b) {

	return (a > b ? a : b);
}

std::ostream	&operator<<(std::ostream &o, Fixed const &rhs) {

	o << (rhs.toFloat());
	return (o);
}
