/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TokensList.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 10:25:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/02 16:43:48 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOKENSLIST_HPP
# define TOKENSLIST_HPP
# include <iostream>
# include <fstream>
# include <sstream>
# include <sys/stat.h>

class TokensList {

private:
	struct	s_node {
		std::string				lexem;
		s_node					*next;
		s_node					*prev;
	}		t_node;
	typedef struct node*	nodePtr;
	nodePtr					top;
	nodePtr					back;
	nodePtr					curr;
	bool					isSeparator();
	bool					isOperator();
	bool					isBracket();
	bool					isNum();

public:
	TokensList(void);
	TokensList(TokensList &rhs);
	~TokensList(void);
	TokensList				&operator=(TokensList const &rhs);
	void					parseString(std::string &s);
	void					pushBack(std::string string);
	char					*getHeadValue(void);
	void					freeList(void);
	void					printList(void);
	bool					isListValid(void);

};

#endif
