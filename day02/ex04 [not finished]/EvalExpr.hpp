/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   EvalExpr.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/02 10:23:41 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/02 16:20:34 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVALEXPR_HPP
# define EVALEXPR_HPP
# include "TokensList.hpp"
# include "Fixed.hpp"

class EvalExpr {

private:
	bool					isOperator();
	bool					isSum();
	bool					isSub();
	bool					isDiv();
	bool					isMult();
	// bool					isNum();
	bool					isOpenBrack();
	bool					isCloseBrack();

public:
	void		solveTokens(TokensList &tokensList);

};

#endif
