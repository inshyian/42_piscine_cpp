/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/03 21:14:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/02 16:19:53 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "TokensList.hpp"
#include "EvalExpr.hpp"

static void		wrongString(void) {

	std::cout << "\tWrong string format" << std::endl;
	std::cout << "\tValid format example: \"3 * ( 1 + -3 ) - 1\"" << std::endl;
}

static void		displayUsage(void) {

	std::cout << "Usage:\t./eval_expr \"arithmetic expression\"" << std::endl;
	std::cout << "\tProgram will display result" << std::endl;
}

int				main(int argc, char **argv) {

	TokensList			tokensList;
	EvalExpr			evalExpr;
	std::string			string;

	if (argc == 2)
	{
		string = argv[1];
		tokensList.parseString(string);
		if (tokensList.isListValid())
			evalExpr.solveTokens(tokensList);
		else
			wrongString();
	}
	else
		displayUsage();
	return (0);
}
