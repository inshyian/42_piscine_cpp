/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/03 21:14:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/02 10:58:47 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"

/*
**  Class static vars next...
*/

const int Fixed::fractionBitsNum = 8;

/*
**  Public functions next...
*/

Fixed::Fixed(void) {

	std::cout << "Default constructor called" << std::endl;
	raw = 0;
	return ;
}

Fixed::~Fixed(void) {

	std::cout << "Default destructor called" << std::endl;
	return ;
}

Fixed::Fixed(Fixed const &rhs) {

	std::cout << "Copy constructor called" << std::endl;
	this->raw = rhs.getRawBits();
	return ;
}

Fixed			&Fixed::operator=(Fixed const &rhs) {

	std::cout << "Assignation operator called" << std::endl;
	if (this != &rhs)
	{
		this->raw = rhs.getRawBits();
	}
	return *this;
}

int				Fixed::getRawBits(void) const {

	std::cout << __func__ << " member function called" << std::endl;
	return this->raw;
}

void			Fixed::setRawBits(int const raw) {

	std::cout << __func__ << " member function called" << std::endl;
	this->raw = raw;
	return ;
}

std::ostream	&operator<<(std::ostream &o, Fixed const &rhs) {

	o << ((rhs.getRawBits() >> 8) << 8) << "." << ((rhs.getRawBits() << 24) >> 24) << std::endl;
	return o;
}
