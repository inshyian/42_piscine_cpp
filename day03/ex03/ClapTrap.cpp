/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 19:15:42 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:48:37 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"

ClapTrap::ClapTrap(void) {};

ClapTrap::ClapTrap(std::string const name) {

	_name = name;
	std::cout << "CL4P-TP " << _name << " born" << std::endl;
	return ;
}

ClapTrap::ClapTrap(ClapTrap const &rhs) {

	*this = rhs;
	std::cout << "CL4P-TP " << _name << " born" << std::endl;
	return ;
}

ClapTrap::~ClapTrap(void) {

	std::cout << "CL4P-TP " << _name << " died" << std::endl;
	return ;
}

ClapTrap		&ClapTrap::operator=(ClapTrap const &rhs) {

	if (this != &rhs)
	{
		_type = rhs._type;
		_name = rhs._name;
		_hitPoints = rhs._hitPoints;
		_maxHitPoints = rhs._maxHitPoints;
		_enegryPoints = rhs._enegryPoints;
		_maxEnergyPoints = rhs._maxEnergyPoints;
		_level = rhs._level;
		_meeleAttackDamage = rhs._meeleAttackDamage;
		_rangedAttackDamage = rhs._rangedAttackDamage;
		_armorDamageRedution = rhs._armorDamageRedution;
	}
	return (*this);
}

void			ClapTrap::rangedAttack(const std::string &target) {

	std::cout << _type << " " << _name << " attacks " << target << " at range, causing " << _rangedAttackDamage << " points of damage" << std::endl;
	return ;
}

void			ClapTrap::meleeAttack(const std::string &target) {

	std::cout << _type << " " << _name << " kicks " << target << ", causing " << _meeleAttackDamage << " points of damage" << std::endl;
	return ;
}

unsigned int	ClapTrap::_reduceDamage(unsigned int amount) {

	if ((unsigned int)_armorDamageRedution >= amount)
		amount = 0;
	else
		amount = amount - (unsigned int)_armorDamageRedution;
	return (amount);
}

void			ClapTrap::takeDamage(unsigned int amount) {

	int				realAmount;
	std::string		quotes[] = {
		"Oh my God, I'm leaking! I think I'm leaking! Ahhhh, I'm leaking! There's oil everywhere!",
		"My servos... are seizing...",
		"I can see... the code"
	};

	amount = _reduceDamage(amount);
	if ((unsigned int)_hitPoints >= amount)
		realAmount = amount;
	else
		realAmount = _hitPoints;
	_hitPoints = _hitPoints - realAmount;
	std::cout << _type << " " << _name << " was damaged for " << realAmount << " hit points" << std::endl;
	srand(time(NULL) + _hitPoints);
	std::cout << _name << ": " << quotes[rand() % 3] << std::endl;
	return ;
}

void			ClapTrap::beRepaired(unsigned int amount) {

	int				realAmount;

	if ((unsigned int)(_maxHitPoints - _hitPoints) >= amount)
		realAmount = amount;
	else
		realAmount = _maxHitPoints - _hitPoints;
	_hitPoints = _hitPoints + realAmount;
	std::cout << _type << " " << _name << " was repaired for " << realAmount << " hit points" << std::endl;
	return ;
}

std::string		ClapTrap::getName() {

	return (_name);
}
