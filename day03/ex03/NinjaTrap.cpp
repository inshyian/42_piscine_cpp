/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 21:41:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:48:32 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap(void) {};

NinjaTrap::NinjaTrap(std::string const name) : ClapTrap(name) {

	_type = "SC4V-TP";
	_hitPoints = 60;
	_maxHitPoints = 60;
	_enegryPoints = 120;
	_maxEnergyPoints = 120;
	_level = 1;
	_meeleAttackDamage = 60;
	_rangedAttackDamage = 5;
	_armorDamageRedution = 0;
	std::cout << "NinjaTrap " << _name << " born" << std::endl;
	return ;
}

NinjaTrap::NinjaTrap(NinjaTrap const &rhs) : ClapTrap(rhs){

	*this = rhs;
	std::cout << "NinjaTrap " << _name << " born" << std::endl;
	return ;
}

NinjaTrap::~NinjaTrap(void) {

	std::cout << "NinjaTrap " << _name << " died" << std::endl;
	return ;
}

NinjaTrap		&NinjaTrap::operator=(NinjaTrap const &rhs) {

	if (this != &rhs)
	{
		_type = rhs._type;
		_name = rhs._name;
		_hitPoints = rhs._hitPoints;
		_maxHitPoints = rhs._maxHitPoints;
		_enegryPoints = rhs._enegryPoints;
		_maxEnergyPoints = rhs._maxEnergyPoints;
		_level = rhs._level;
		_meeleAttackDamage = rhs._meeleAttackDamage;
		_rangedAttackDamage = rhs._rangedAttackDamage;
		_armorDamageRedution = rhs._armorDamageRedution;
	}
	return (*this);
}

void			NinjaTrap::ninjaShoebox(FragTrap &ft) {

	this->meleeAttack(ft.getName());
	ft.takeDamage(this->_meeleAttackDamage);
	return ;
}
void			NinjaTrap::ninjaShoebox(ScavTrap &st) {

	this->rangedAttack(st.getName());
	st.takeDamage(this->_rangedAttackDamage);
	return ;
}
void			NinjaTrap::ninjaShoebox(NinjaTrap &nj) {

	if (this == &nj)
	{
		std::cout << _type << " " << _name << " self-medicates!!!" << std::endl;
		this->beRepaired(rand() % 12 + 3);
		this->takeDamage(rand() % 6);
	}
	else
	{
		this->meleeAttack(nj.getName());
		nj.takeDamage(this->_meeleAttackDamage);
	}
	return ;
}
