/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 21:40:57 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:48:28 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP
# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "ScavTrap.hpp"

class NinjaTrap : public ClapTrap {

public:
	NinjaTrap(void);
	NinjaTrap(const std::string name);
	NinjaTrap(NinjaTrap const &rhs);
	~NinjaTrap(void);
	NinjaTrap		&operator=(NinjaTrap const &rhs); 	// Cannonical Operator Overload

	void	ninjaShoebox(FragTrap &);
	void	ninjaShoebox(ScavTrap &);
	void	ninjaShoebox(NinjaTrap &);
};

#endif
