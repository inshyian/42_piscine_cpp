/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 12:09:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:45:13 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP
# include <iostream>
# include <sys/time.h>

class FragTrap {

private:
	int				_hitPoints;
	int				_maxHitPoints;
	int				_enegryPoints;
	int				_maxEnergyPoints;
	int				_level;
	std::string		_name;
	int				_meeleAttackDamage;
	int				_rangedAttackDamage;
	int				_napalmAttackDamage;
	int				_plasmAttackDamage;
	int				_rocketAttackDamage;
	int				_armorDamageRedution;
	unsigned int	_reduceDamage(unsigned int amount);

public:
	FragTrap(void);
	FragTrap(std::string const name);					// Cannonical
	FragTrap(FragTrap const &rhs);						// Cannonical
	~FragTrap(void);									// Cannonical
	FragTrap		&operator=(FragTrap const &rhs); 	// Cannonical

	void			rangedAttack(std::string const &target);
	void			meleeAttack(std::string const &target);
	void			napalmAttack(std::string const &target);
	void			plasmAttack(std::string const &target);
	void			rocketAttack(std::string const &target);

	void			takeDamage(unsigned int amount);
	void			beRepaired(unsigned int amount);

	void			vaulthunter_dot_exe(std::string const & target);

};

#endif
