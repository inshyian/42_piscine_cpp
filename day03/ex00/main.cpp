/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 12:09:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 12:53:34 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

static void		checkFunc(FragTrap &ft)
{
	/**/
	std::cout << "=+= Demostration of functions takeDamage, beRepaired, operator=..." << std::endl;
	/**/
	FragTrap	dup("Cluck-Trap");

	ft.beRepaired(13);
	ft.takeDamage(15);
	dup = ft;
	ft.beRepaired(10);
	ft.beRepaired(6);
	dup.beRepaired(10);
	dup.beRepaired(6);

	ft.takeDamage(80);
	ft.takeDamage(7);
	ft.takeDamage(5);
	ft.takeDamage(13);
	ft.takeDamage(13);
	/**/
	 std::cout << "=+= Demostration of attack functions..." << std::endl;
	/**/
	ft.rangedAttack("Some enemy");
	ft.meleeAttack("Some bruise");
	ft.napalmAttack("Some unhappy");
	ft.plasmAttack("Some enemy");
	ft.rocketAttack("Some enemy");
	/**/
	std::cout << "=+= Demostration of attack randomizer..." << std::endl;
	/**/
	ft.vaulthunter_dot_exe("punching bag");
	ft.vaulthunter_dot_exe("punching bag");
	ft.vaulthunter_dot_exe("punching bag");
	ft.vaulthunter_dot_exe("punching bag");
	ft.vaulthunter_dot_exe("punching bag");
}

int		main(void) {

	FragTrap	ft("First");

	checkFunc(ft);
	return (0);
}
