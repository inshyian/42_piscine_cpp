/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 12:09:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 12:56:37 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

static void		checkClapTrap(void) {

	/**/
	std::cout << "=+= Demostration of construction/destruction chaining..." << std::endl;
	/**/
	ScavTrap	st("ClapTrapChecker");
}

static void		checkFuncScavTrap(void) {

	/**/
	std::cout << "=+= Demostration of challenges randomizer..." << std::endl;
	/**/
	ScavTrap	st("Invader");

	st.challengeNewcomer("Pony");
	st.challengeNewcomer("Pony");
	st.challengeNewcomer("Pony");
}

static void		checkFuncFragTrap(void) {

	/**/
	std::cout << "=+= Demostration of functions takeDamage, beRepaired, operator=..." << std::endl;
	/**/
	FragTrap	ft("Little");
	FragTrap	dup("Little Copy");
	FragTrap	dup_2(ft);

	ft.beRepaired(13);
	ft.takeDamage(15);
	dup = ft;
	ft.beRepaired(10);
	ft.beRepaired(6);
	dup.beRepaired(10);
	dup.beRepaired(6);

	ft.takeDamage(80);
	ft.takeDamage(7);
	ft.takeDamage(5);
	ft.takeDamage(13);
	ft.takeDamage(13);
	/**/
	 std::cout << "=+= Demostration of attack functions..." << std::endl;
	/**/
	ft.rangedAttack("Some enemy");
	ft.meleeAttack("Some bruise");
	ft.napalmAttack("Some unhappy");
	ft.plasmAttack("Some enemy");
	ft.rocketAttack("Some enemy");
	/**/
	std::cout << "=+= Demostration of attack randomizer..." << std::endl;
	/**/
	ft.vaulthunter_dot_exe("Punching bag");
	ft.vaulthunter_dot_exe("Punching bag");
	ft.vaulthunter_dot_exe("Punching bag");
	ft.vaulthunter_dot_exe("Punching bag");
	ft.vaulthunter_dot_exe("Punching bag");
}

int		main(void) {
	checkFuncFragTrap();
	checkFuncScavTrap();
	checkClapTrap();

	return (0);
}
