/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 18:32:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:47:06 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP
# include <iostream>
# include <sys/time.h>
# include "ClapTrap.hpp"

class ScavTrap : public ClapTrap {

private:
	static std::string	_challenges[6];

public:
	ScavTrap(void);
	ScavTrap(std::string const name);
	ScavTrap(ScavTrap const &rhs);
	~ScavTrap(void);
	ScavTrap			&operator=(ScavTrap const &rhs); 	// Cannonical Operator Overload

	void				challengeNewcomer(std::string const & target);
};

#endif
