/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 12:09:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:47:30 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP
# include <iostream>
# include <sys/time.h>
# include "ClapTrap.hpp"

class FragTrap : public ClapTrap {

private:
	int				_napalmAttackDamage;
	int				_plasmAttackDamage;
	int				_rocketAttackDamage;

public:
	FragTrap(void);
	FragTrap(std::string const name);
	FragTrap(FragTrap const &rhs);
	~FragTrap(void);
	FragTrap		&operator=(FragTrap const &rhs); 	// Cannonical Operator Overload

	void			napalmAttack(std::string const &target);
	void			plasmAttack(std::string const &target);
	void			rocketAttack(std::string const &target);

	void			vaulthunter_dot_exe(std::string const & target);
};

#endif
