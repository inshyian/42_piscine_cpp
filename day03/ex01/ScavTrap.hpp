/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 18:32:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:47:13 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP
# include <iostream>
# include <sys/time.h>

class ScavTrap {

private:
	int					_hitPoints;
	int					_maxHitPoints;
	int					_enegryPoints;
	int					_maxEnergyPoints;
	int					_level;
	std::string			_name;
	std::string			_type;
	int					_meeleAttackDamage;
	int					_rangedAttackDamage;
	int					_armorDamageRedution;
	unsigned int		_reduceDamage(unsigned int amount);
	static std::string	_challenges[6];

public:
	ScavTrap(void);
	ScavTrap(std::string const name);
	ScavTrap(ScavTrap const &rhs);
	~ScavTrap(void);
	ScavTrap			&operator=(ScavTrap const &rhs); 	// Cannonical Operator Overload

	void				rangedAttack(std::string const &target);
	void				meleeAttack(std::string const &target);

	void				takeDamage(unsigned int amount);
	void				beRepaired(unsigned int amount);

	void				challengeNewcomer(std::string const & target);
};

#endif
