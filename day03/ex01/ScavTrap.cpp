/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 18:32:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:48:55 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

ScavTrap::ScavTrap(void) {};

ScavTrap::ScavTrap(std::string const name) {

	_type = "SC4V-TP";
	_name = name;
	_hitPoints = 100;
	_maxHitPoints = 100;
	_enegryPoints = 50;
	_maxEnergyPoints = 50;
	_level = 1;
	_meeleAttackDamage = 20;
	_rangedAttackDamage = 15;
	_armorDamageRedution = 3;
	std::cout << _type << " " << _name << " born" << std::endl;
	return ;
}

ScavTrap::ScavTrap(ScavTrap const &rhs) {

	*this = rhs;
	std::cout << _type << " " << _name << " born" << std::endl;
	return ;
}

ScavTrap::~ScavTrap(void) {

	std::cout << _type << " " << _name << " died" << std::endl;
	return ;
}

ScavTrap		&ScavTrap::operator=(ScavTrap const &rhs) {

	if (this != &rhs)
	{
		_type = rhs._type;
		_name = rhs._name;
		_hitPoints = rhs._hitPoints;
		_maxHitPoints = rhs._maxHitPoints;
		_enegryPoints = rhs._enegryPoints;
		_maxEnergyPoints = rhs._maxEnergyPoints;
		_level = rhs._level;
		_meeleAttackDamage = rhs._meeleAttackDamage;
		_rangedAttackDamage = rhs._rangedAttackDamage;
		_armorDamageRedution = rhs._armorDamageRedution;
	}
	return (*this);
}

void			ScavTrap::rangedAttack(const std::string &target) {

	std::cout << _type << " " << _name << " attacks " << target << " at range, causing " << _rangedAttackDamage << " points of damage" << std::endl;
	return ;
}

void			ScavTrap::meleeAttack(const std::string &target) {

	std::cout << _type << " " << _name << " kicks " << target << ", causing " << _meeleAttackDamage << " points of damage" << std::endl;
	return ;
}

unsigned int	ScavTrap::_reduceDamage(unsigned int amount) {

	if ((unsigned int)_armorDamageRedution >= amount)
		amount = 0;
	else
		amount = amount - (unsigned int)_armorDamageRedution;
	return (amount);
}

void		ScavTrap::takeDamage(unsigned int amount) {

	int				realAmount;
	std::string		quotes[] = {
		"Oh my God, I'm leaking! I think I'm leaking! Ahhhh, I'm leaking! There's oil everywhere!",
		"My servos... are seizing...",
		"I can see... the code"
	};

	amount = _reduceDamage(amount);
	if ((unsigned int)_hitPoints >= amount)
		realAmount = amount;
	else
		realAmount = _hitPoints;
	_hitPoints = _hitPoints - realAmount;
	std::cout << _type << " " << _name << " was damaged for " << realAmount << " hit points" << std::endl;
	srand(time(NULL) + _hitPoints);
	std::cout << _name << ": " << quotes[rand() % 3] << std::endl;
	return ;
}

void			ScavTrap::beRepaired(unsigned int amount) {

	int				realAmount;

	if ((unsigned int)(_maxHitPoints - _hitPoints) >= amount)
		realAmount = amount;
	else
		realAmount = _maxHitPoints - _hitPoints;
	_hitPoints = _hitPoints + realAmount;
	std::cout << _type << " " << _name << " was repaired for " << realAmount << " hit points" << std::endl;
	return ;
}

std::string		ScavTrap::_challenges[6] = {

	"The Vault Map is gone! Forever! It will never be found. Never, ever, ever! is what I'll say to everyone I know while I look for it.",
	"And I thought bandits were bad BEFORE they had nightmare plants growing out of them!",
	"As a robot, I'm completely immune to Hector's gas atttacks. But that hasn't stopped me from incessantly cowering!",
	"Don't you worry, minion! Give me one good shot at that Hector dude and I'll take him right out! I... just got some stuff to do first.",
	"We've really come a long way, haven't we, minion? And you're still just as loyal as ever! Who's a good minion? You are! Yes you are!",
	"Sanctuary's gone? But the bank! All my stuff! All my crucial information! \nYES! I'M OFF THE GRID, BABY! NO MORE CREDITORS! \nSeriously, I owe a lot of people a lot of money."
};

void			ScavTrap::challengeNewcomer(std::string const & target) {

	std::cout << _type << " " << _name << " makes challenge for Newcomer " << target << std::endl;
	std::cout << this->_challenges[rand() % 6] << std::endl;
	return ;
}
