/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 13:29:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 14:48:54 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(void) {};

FragTrap::FragTrap(std::string const name) {

	_type = "FR4G-TP";
	_name = name;
	_hitPoints = 100;
	_maxHitPoints = 100;
	_enegryPoints = 100;
	_maxEnergyPoints = 100;
	_level = 1;
	_meeleAttackDamage = 30;
	_rangedAttackDamage = 20;
	_armorDamageRedution = 5;
	_napalmAttackDamage = 35;
	_plasmAttackDamage = 15;
	_rocketAttackDamage = 25;
	std::cout << _type << " " << _name << " born" << std::endl;
	return ;
}

FragTrap::FragTrap(FragTrap const &rhs) {

	*this = rhs;
	std::cout << _type << " " << _name << " born" << std::endl;
	return ;
}

FragTrap::~FragTrap(void) {

	std::cout << _type << " " << _name << " died" << std::endl;
	return ;
}

FragTrap		&FragTrap::operator=(FragTrap const &rhs) {

	if (this != &rhs)
	{
		_type = rhs._type;
		_name = rhs._name;
		_hitPoints = rhs._hitPoints;
		_maxHitPoints = rhs._maxHitPoints;
		_enegryPoints = rhs._enegryPoints;
		_maxEnergyPoints = rhs._maxEnergyPoints;
		_level = rhs._level;
		_meeleAttackDamage = rhs._meeleAttackDamage;
		_rangedAttackDamage = rhs._rangedAttackDamage;
		_armorDamageRedution = rhs._armorDamageRedution;
		_napalmAttackDamage = rhs._napalmAttackDamage;
		_plasmAttackDamage = rhs._plasmAttackDamage;
		_rocketAttackDamage = rhs._rocketAttackDamage;
	}
	return (*this);
}

void			FragTrap::rangedAttack(const std::string &target) {

	std::cout << _type << " " << _name << " attacks " << target << " at range, causing " << _rangedAttackDamage << " points of damage" << std::endl;
	return ;
}

void			FragTrap::meleeAttack(const std::string &target) {

	std::cout << _type << " " << _name << " kicks " << target << ", causing " << _meeleAttackDamage << " points of damage" << std::endl;
	return ;
}

void			FragTrap::napalmAttack(const std::string &target) {

	std::cout << _type << " " << _name << " pours napalm " << target << ", causing " << _napalmAttackDamage << " points of damage" << std::endl;
	return ;
}

void			FragTrap::plasmAttack(const std::string &target) {

	std::cout << _type << " " << _name << " envelops " << target << " with plasma, causing " << _plasmAttackDamage << " points of damage" << std::endl;
	return ;
}

void			FragTrap::rocketAttack(const std::string &target) {

	std::cout << _type << " " << _name << " launches rocket in " << target << ", causing " << _rocketAttackDamage << " points of damage" << std::endl;
	return ;
}

unsigned int	FragTrap::_reduceDamage(unsigned int amount) {

	if ((unsigned int)_armorDamageRedution >= amount)
		amount = 0;
	else
		amount = amount - (unsigned int)_armorDamageRedution;
	return (amount);
}

void		FragTrap::takeDamage(unsigned int amount) {

	int				realAmount;
	std::string		quotes[] = {
		"Oh my God, I'm leaking! I think I'm leaking! Ahhhh, I'm leaking! There's oil everywhere!",
		"My servos... are seizing...",
		"I can see... the code"
	};

	amount = _reduceDamage(amount);
	if ((unsigned int)_hitPoints >= amount)
		realAmount = amount;
	else
		realAmount = _hitPoints;
	_hitPoints = _hitPoints - realAmount;
	std::cout << _type << " " << _name << " was damaged for " << realAmount << " hit points" << std::endl;
	srand(time(NULL) + _hitPoints);
	std::cout << _name << ": " << quotes[rand() % 3] << std::endl;
	return ;
}

void			FragTrap::beRepaired(unsigned int amount) {

	int				realAmount;

	if ((unsigned int)(_maxHitPoints - _hitPoints) >= amount)
		realAmount = amount;
	else
		realAmount = _maxHitPoints - _hitPoints;
	_hitPoints = _hitPoints + realAmount;
	std::cout << _type << " " << _name << " was repaired for " << realAmount << " hit points" << std::endl;
	return ;
}

void			FragTrap::vaulthunter_dot_exe(std::string const & target) {

	if (_enegryPoints >= 25)
	{
		typedef void	(FragTrap::*Attacks)(const std::string &);

		Attacks attacks[] = {
			&FragTrap::rangedAttack,
			&FragTrap::meleeAttack,
			&FragTrap::napalmAttack,
			&FragTrap::plasmAttack,
			&FragTrap::rocketAttack
		};
		_enegryPoints = _enegryPoints - 25;
		(this->*(attacks[(rand()) % 5]))(target);
	}
	else
		std::cout << "Ohh, human.. charge my batteries" << std::endl;
	return ;
}
