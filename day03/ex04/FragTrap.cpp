/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 13:29:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/08 22:01:53 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(void) {};

FragTrap::FragTrap(std::string const name) : ClapTrap (name) {

	_type = "FR4G-TP";
	_hitPoints = 100;
	_maxHitPoints = 100;
	_enegryPoints = 100;
	_maxEnergyPoints = 100;
	_level = 1;
	_meeleAttackDamage = 30;
	_rangedAttackDamage = 20;
	_armorDamageRedution = 5;
	_napalmAttackDamage = 35;
	_plasmAttackDamage = 15;
	_rocketAttackDamage = 25;

	std::cout << "FR4G-TP " << _name << " born" << std::endl;
	return ;
}

FragTrap::FragTrap(FragTrap const &rhs) : ClapTrap(rhs){

	*this = rhs;
	std::cout << "FR4G-TP " << _name << " born" << std::endl;
	return ;
}

FragTrap::~FragTrap(void) {

	std::cout << "FR4G-TP " << _name << " died" << std::endl;
	return ;
}

FragTrap		&FragTrap::operator=(FragTrap const &rhs) {

	if (this != &rhs)
	{
		_type = rhs._type;
		_name = rhs._name;
		_hitPoints = rhs._hitPoints;
		_maxHitPoints = rhs._maxHitPoints;
		_enegryPoints = rhs._enegryPoints;
		_maxEnergyPoints = rhs._maxEnergyPoints;
		_level = rhs._level;
		_meeleAttackDamage = rhs._meeleAttackDamage;
		_rangedAttackDamage = rhs._rangedAttackDamage;
		_armorDamageRedution = rhs._armorDamageRedution;
		_napalmAttackDamage = rhs._napalmAttackDamage;
		_plasmAttackDamage = rhs._plasmAttackDamage;
		_rocketAttackDamage = rhs._rocketAttackDamage;
	}
	return (*this);
}

void			FragTrap::napalmAttack(const std::string &target) {

	std::cout << _type << " " << _name << " pours napalm " << target << ", causing " << _napalmAttackDamage << " points of damage" << std::endl;
	return ;
}

void			FragTrap::plasmAttack(const std::string &target) {

	std::cout << _type << " " << _name << " envelops " << target << " with plasma, causing " << _plasmAttackDamage << " points of damage" << std::endl;
	return ;
}

void			FragTrap::rocketAttack(const std::string &target) {

	std::cout << _type << " " << _name << " launches rocket in " << target << ", causing " << _rocketAttackDamage << " points of damage" << std::endl;
	return ;
}

void			FragTrap::recallRangedAttack(std::string const &target) {

	FragTrap::rangedAttack(target);
}

void			FragTrap::recallMeleeAttack(std::string const &target) {

	FragTrap::meleeAttack(target);
}

void			FragTrap::vaulthunter_dot_exe(std::string const & target) {

	if (_enegryPoints >= 25)
	{
		typedef void	(FragTrap::*Attacks)(std::string const &);

		Attacks attacks[] = {
			&FragTrap::recallRangedAttack,
			&FragTrap::recallMeleeAttack,
			&FragTrap::napalmAttack,
			&FragTrap::plasmAttack,
			&FragTrap::rocketAttack
		};
		_enegryPoints = _enegryPoints - 25;
		(this->*(attacks[(rand()) % 5]))(target);
	}
	else
		std::cout << "Ohh, human.. charge my batteries" << std::endl;
	return ;
}
