/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 19:15:39 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 01:02:47 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP
# include <iostream>
# include <sys/time.h>

class ClapTrap {

protected:
	int				_hitPoints;
	int				_maxHitPoints;
	int				_enegryPoints;
	int				_maxEnergyPoints;
	int				_level;
	std::string		_name;
	std::string		_type;
	int				_meeleAttackDamage;
	int				_rangedAttackDamage;
	int				_napalmAttackDamage;
	int				_plasmAttackDamage;
	int				_rocketAttackDamage;
	int				_armorDamageRedution;
	unsigned int	_reduceDamage(unsigned int amount);

public:
	ClapTrap(void);
	ClapTrap(std::string const name);
	ClapTrap(ClapTrap const &rhs);
	~ClapTrap(void);
	ClapTrap		&operator=(ClapTrap const &rhs);

	void			rangedAttack(std::string const &target);
	void			meleeAttack(std::string const &target);

	void			takeDamage(unsigned int amount);
	void			beRepaired(unsigned int amount);

	std::string		getName(void);
	void			setName(std::string name);
};

#endif
