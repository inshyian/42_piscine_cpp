/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 22:40:00 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 01:16:57 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERTRAP_HPP
# define SUPERTRAP_HPP
# include "FragTrap.hpp"
# include "NinjaTrap.hpp"

class SuperTrap : public NinjaTrap, public FragTrap {

public:
	SuperTrap(void);
	SuperTrap(const std::string name);
	SuperTrap(SuperTrap const &rhs);
	~SuperTrap(void);
	SuperTrap		&operator=(SuperTrap const &rhs);

	using::FragTrap::rangedAttack;
	using::FragTrap::vaulthunter_dot_exe;
	using::NinjaTrap::meleeAttack;
	using::NinjaTrap::ninjaShoebox;

};

#endif
