/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 12:09:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 12:56:09 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

void	checkSuperTrap(void) {

	/**/
	std::cout << "=+= Demostration of SuperTrap (inherit of two classes)..." << std::endl;
	/**/
	SuperTrap	st("Powerfull");
	NinjaTrap	nt("newborn");

	st.meleeAttack("meleeTarget");
	st.rangedAttack("rangedTarget");
	st.napalmAttack("napalmTarget");
	st.vaulthunter_dot_exe("vaulthunterTarget");
	st.ninjaShoebox(nt);
}

static void		checkNinjaTrap(void) {

	/**/
	std::cout << "=+= Demostration of NinjaTrap (ClapTrap creates only once)..." << std::endl;
	/**/
	{
		NinjaTrap	tester("NinjaTester!");
	}
	/**/
	std::cout << "=+= Demostration of NinjaTrap..." << std::endl;
	/**/
	{
		NinjaTrap	nt("Ninja!");
		NinjaTrap	nt_("Ninja2");
		ScavTrap	st("Target One");
		FragTrap	ft("Target Two");

		nt.ninjaShoebox(nt);
		nt.ninjaShoebox(nt_);
		nt.ninjaShoebox(st);
		nt.ninjaShoebox(ft);
	}
}

void	checkClapTrap(void) {

	/**/
	std::cout << "=+= Demostration of construction/destruction chaining..." << std::endl;
	/**/
	ScavTrap	st("ClapTrapChecker");
}

void	checkFuncScavTrap(void) {

	/**/
	std::cout << "=+= Demostration of challenges randomizer..." << std::endl;
	/**/
	ScavTrap	st("Invader");

	st.challengeNewcomer("Pony");
	st.challengeNewcomer("Pony");
	st.challengeNewcomer("Pony");
}

void	checkFuncFragTrap(void) {

	/**/
	std::cout << "=+= Demostration of functions takeDamage, beRepaired, operator=..." << std::endl;
	/**/
	FragTrap	ft("Little");
	FragTrap	dup("Cluck-Trap");

	ft.beRepaired(13);
	ft.takeDamage(15);
	dup = ft;
	ft.beRepaired(10);
	ft.beRepaired(6);
	dup.beRepaired(10);
	dup.beRepaired(6);

	ft.takeDamage(80);
	ft.takeDamage(7);
	ft.takeDamage(5);
	ft.takeDamage(13);
	ft.takeDamage(13);
	/**/
	 std::cout << "=+= Demostration of attack functions..." << std::endl;
	/**/
	ft.rangedAttack("Some enemy");
	ft.meleeAttack("Some bruise");
	ft.napalmAttack("Some unhappy");
	ft.plasmAttack("Some enemy");
	ft.rocketAttack("Some enemy");
	/**/
	std::cout << "=+= Demostration of attack randomizer..." << std::endl;
	/**/
	ft.vaulthunter_dot_exe("Punching bag");
	ft.vaulthunter_dot_exe("Punching bag");
	ft.vaulthunter_dot_exe("Punching bag");
	ft.vaulthunter_dot_exe("Punching bag");
	ft.vaulthunter_dot_exe("Punching bag");
}

int		main(void) {

	checkFuncFragTrap();
	checkFuncScavTrap();
	checkClapTrap();
	checkNinjaTrap();
	checkSuperTrap();
	return (0);
}
