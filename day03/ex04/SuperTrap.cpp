/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 22:40:02 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 01:26:05 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.hpp"

SuperTrap::SuperTrap(void) {};

SuperTrap::SuperTrap(const std::string name) : NinjaTrap(name), FragTrap(name) {

	_type = "SuperTrap";
	_hitPoints = FragTrap::_hitPoints;
	_maxHitPoints = FragTrap::_maxHitPoints;
	_enegryPoints = NinjaTrap::_enegryPoints;
	_maxEnergyPoints = NinjaTrap::_maxEnergyPoints;
	_level = 1;
	_name = name;
	_meeleAttackDamage = NinjaTrap::_meeleAttackDamage;
	_rangedAttackDamage = FragTrap::_rangedAttackDamage;
	_armorDamageRedution = FragTrap::_armorDamageRedution;
	std::cout << "SuperTrap " << FragTrap::_name << " born" << std::endl;
	return ;
}

SuperTrap::SuperTrap(SuperTrap const &rhs) : ClapTrap(rhs){

	*this = rhs;
	std::cout << "SuperTrap " << _name << " born" << std::endl;
	return ;
}

SuperTrap::~SuperTrap(void) {

	std::cout << "SuperTrap " << _name << " died" << std::endl;
	return ;
}

SuperTrap		&SuperTrap::operator=(SuperTrap const &rhs) {

	if (this != &rhs)
	{
		_type = rhs._type;
		_name = rhs._name;
		_hitPoints = rhs._hitPoints;
		_maxHitPoints = rhs._maxHitPoints;
		_enegryPoints = rhs._enegryPoints;
		_maxEnergyPoints = rhs._maxEnergyPoints;
		_level = rhs._level;
		_meeleAttackDamage = rhs._meeleAttackDamage;
		_rangedAttackDamage = rhs._rangedAttackDamage;
		_armorDamageRedution = rhs._armorDamageRedution;
	}
	return (*this);
}
