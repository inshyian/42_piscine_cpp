/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/03 12:09:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/04 01:16:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP
# include <iostream>
# include <sys/time.h>
# include "ClapTrap.hpp"

class FragTrap : virtual public ClapTrap {

private:
	int				_napalmAttackDamage;
	int				_plasmAttackDamage;
	int				_rocketAttackDamage;

public:
	FragTrap(void);
	FragTrap(std::string const name);
	FragTrap(FragTrap const &rhs);
	~FragTrap(void);
	FragTrap		&operator=(FragTrap const &rhs); 	// Cannonical Operator Overload

	void			recallRangedAttack(std::string const &target);
	void			recallMeleeAttack(std::string const &target);

	void			napalmAttack(std::string const &target);
	void			plasmAttack(std::string const &target);
	void			rocketAttack(std::string const &target);

	void			vaulthunter_dot_exe(std::string const & target);
};

#endif
