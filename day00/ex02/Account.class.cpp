/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Account.class.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/25 22:54:45 by ishyian           #+#    #+#             */
/*   Updated: 2019/06/26 20:55:07 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ctime>
#include <iostream>
#include "Account.class.hpp"

/*
**  Class static vars next...
*/

int		Account::_nbAccounts = 0;
int		Account::_totalAmount = 0;
int		Account::_totalNbDeposits = 0;
int		Account::_totalNbWithdrawals = 0;

/*
**  Public functions next...
*/

Account::Account(int initial_deposit) {

		this->_accountIndex = this->_nbAccounts;
		this->_amount = initial_deposit;
		this->_nbDeposits = 0;
		this->_nbWithdrawals = 0;
		this->_nbAccounts = this->_nbAccounts + 1;
		this->_totalAmount = this->_totalAmount + initial_deposit;
		Account::_displayTimestamp();
		std::cout << "index:" << this->_accountIndex
		<< ";amount:" << this->_amount
		<< ";created" << std::endl;
		return;
}

Account::~Account(void) {

		Account::_displayTimestamp();
		std::cout << "index:" << this->_accountIndex
		<< ";amount:" << this->_amount
		<< ";closed" << std::endl;
		return;
}

int		Account::getNbAccounts(void) {

		return (Account::_nbAccounts);
}

int		Account::getTotalAmount(void) {

		return (Account::_totalAmount);
}

int		Account::getNbDeposits(void) {

		return (Account::_totalNbDeposits);
}

int		Account::getNbWithdrawals(void) {

		return (Account::_totalNbWithdrawals);
}

void	Account::makeDeposit(int deposit) {

		this->_amount = this->_amount + deposit;
		this->_nbDeposits = this->_nbDeposits + 1;
		this->_totalAmount = this->_totalAmount + deposit;
		this->_totalNbDeposits = this->_totalNbDeposits + 1;
		Account::_displayTimestamp();
		std::cout << "index:" << this->_accountIndex
		<< ";p_amount:" << this->_amount - deposit
		<< ";deposit:" << deposit
		<< ";amount:" << this->_amount
		<< ";nb_deposits:" << this->_nbDeposits << std::endl;
		return;
}

bool	Account::makeWithdrawal(int withdrawal) {

		if (withdrawal > this->_amount)
		{
			Account::_displayTimestamp();
			std::cout << "index:" << this->_accountIndex
			<< ";p_amount:" << this->_amount
			<< ";withdrawal:refused" << std::endl;
			return (false);
		}
		this->_amount = this->_amount - withdrawal;
		this->_nbWithdrawals = this->_nbWithdrawals + 1;
		this->_totalAmount = this->_totalAmount - withdrawal;
		this->_totalNbWithdrawals = this->_totalNbWithdrawals + 1;
		Account::_displayTimestamp();
		std::cout << "index:" << this->_accountIndex
		<< ";p_amount:" << this->_amount + withdrawal
		<< ";withdrawal:" << withdrawal
		<< ";amount:" << this->_amount
		<< ";nb_withdrawals:" << this->_nbWithdrawals << std::endl;
		return (true);
}

int		Account::checkAmount(void) const {

		return (this->_amount);
		std::cout << __func__ << std::endl;
		return (0);
}

void	Account::displayStatus(void) const {

		Account::_displayTimestamp();
		std::cout << "index:" << this->_accountIndex
		<< ";amount:" << this->_amount
		<< ";deposits:" << this->_nbDeposits
		<< ";withdrawals:" << this->_nbWithdrawals << std::endl;
		return;
}

void	Account::displayAccountsInfos(void) {

		Account::_displayTimestamp();
		std::cout << "accounts:" << Account::getNbAccounts()
		<< ";total:" << Account::getTotalAmount()
		<< ";deposits:" << Account::getNbDeposits()
		<< ";withdrawals:" << Account::getNbWithdrawals() << std::endl;
		return;
}

/*
**  Private functions next...
*/

void	Account::_displayTimestamp (void) {

		time_t			rawtime;
		struct tm		*timeinfo;
		char			buffer[80];

		time (&rawtime);
		timeinfo = localtime(&rawtime);
		strftime(buffer,80,"[%Y%m%d_%H%M%S] ",timeinfo);
	  	std::cout << buffer;
		return;
}
