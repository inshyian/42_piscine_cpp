/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/24 17:57:20 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/30 12:53:15 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <locale>

int		main(int ac, char **av)
{
	int				i;
	std::string		str;
	std::locale		loc;

	if (ac == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
	else
	{
		i = 1;
		while (i < ac)
		{
			str = av[i];
			for (std::string::size_type ii = 0; ii < str.length(); ii++)
				std::cout << std::toupper(str[ii], loc);
			i++;
		}
	}
	std::cout << std::endl;
	return (0);
}
