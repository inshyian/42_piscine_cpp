/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBookClass.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/24 19:11:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/03 11:41:04 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOKCLASS_H
# define PHONEBOOKCLASS_H
# define CONTACT_LIST_SIZE 8
# include <iostream>

class Contact {
private:
		int				index;
		std::string		firstName;
		std::string		lastName;
		std::string		nickname;
		std::string		login;
		std::string		postalAddress;
		std::string		emailAddress;
		std::string		phoneNumber;
		std::string		birthdayDate;
		std::string		favouriteMeal;
		std::string		underwearColor;
		std::string		darkestSecret;
		void			truncField(std::string);

public:
		Contact(void);
		~Contact(void);
		void			fillContact(int index);
		void			printContactFullData(void) const;
		void			printContactTruncatedData(void) const;

};

class Phonebook {
private:
		Contact			contacts[CONTACT_LIST_SIZE];
		int				memory;
		void			printContact(void) const;
		int				isAvailableFreeMemory(void) const;
		int				isPhoneBookEmpty(void) const;

public:
		Phonebook(void);
		~Phonebook(void);
		void			searchContact(void) const;
		void			addContact(void);

};

#endif
