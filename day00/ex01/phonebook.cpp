/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/24 19:23:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/01 12:16:59 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "PhoneBookClass.hpp"

int		main(void) {

	Phonebook		book;
	std::string		command;

	while (!std::cin.eof())
	{
		std::cout << "MAIN MENU" << std::endl;
		std::cin >> command;
		if (command == "ADD")
			book.addContact();
		else if (command == "SEARCH")
			book.searchContact();
		else if (command == "EXIT")
			break;
		else
			std::cout << "Reread instructions, please." << std::endl;
	}
	return (0);
}
