/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PhoneBookClass.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/24 19:15:24 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/03 11:23:51 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "PhoneBookClass.hpp"

Contact::Contact(void) {

	return;
}

Contact::~Contact(void) {

	return;
}

void	Contact::fillContact(int index_) {

	this->index = index_;
	std::cout << "Enter First Name: ";
	std::cin >> firstName;
	std::cout << "Enter Last Name: ";
	std::cin >> lastName;
	std::cout << "Enter nickname: ";
	std::cin >> nickname;
	std::cout << "Enter login: ";
	std::cin >> login;
	std::cout << "Enter Postal address: ";
	std::cin >> postalAddress;
	std::cout << "Enter Phone Number: ";
	std::cin >> phoneNumber;
	std::cout << "Enter Birthday: ";
	std::cin >> birthdayDate;
	std::cout << "Enter Favourite Meal: ";
	std::cin >> favouriteMeal;
	std::cout << "Enter Underwear Color: ";
	std::cin >> underwearColor;
	std::cout << "Enter The Darkest Secret: ";
	std::cin >> darkestSecret;
	std::cout << "Contact has been saved to the phonebook's memory." << std::endl;
}

void	Contact::printContactTruncatedData() const {

	std::string		str;

	std::cout << std::setw(10) << index << "|";
	if (firstName.length() < 10)
		std::cout << std::setw(10) << firstName << "|";
	else
		std::cout << std::setw(9) << firstName.substr(0, 9) << ".|";
	if (lastName.length() < 10)
		std::cout << std::setw(10) << lastName << "|";
	else
		std::cout << std::setw(9) << lastName.substr(0, 9) << ".|";
	if (nickname.length() < 10)
		std::cout << std::setw(10) << nickname;
	else
		std::cout << std::setw(9) << nickname.substr(0, 9) << ".";
	std::cout << std::endl;
	return;
}

void	Contact::printContactFullData() const {

	std::cout << std::setw(25) << "First Name: " << firstName << std::endl;
	std::cout << std::setw(25) << "Last Name: " << lastName << std::endl;
	std::cout << std::setw(25) << "Nickname: " << nickname << std::endl;
	std::cout << std::setw(25) << "Login: " << login << std::endl;
	std::cout << std::setw(25) << "Postal address: " << postalAddress << std::endl;
	std::cout << std::setw(25) << "Phone Number: " << phoneNumber << std::endl;
	std::cout << std::setw(25) << "Birthday: " << birthdayDate << std::endl;
	std::cout << std::setw(25) << "Favourite Meal: " << favouriteMeal << std::endl;
	std::cout << std::setw(25) << "Underwear Color ;): " << underwearColor << std::endl;
	std::cout << std::setw(25) << "The Darkest Secret 0_o: " << darkestSecret << std::endl;
	return;
}

Phonebook::Phonebook(void) : memory(CONTACT_LIST_SIZE) {

	std::cout << "Welcome to the awesome phonebook!" << std::endl;
	std::cout << "Here you can add AS MANY AS EIGHT contacts" << std::endl;
	std::cout << "Print ADD to create a new contact" << std::endl;
	std::cout << "Print SEARCH to search a contact" << std::endl;
	std::cout << "Print EXIT to close phonebook" << std::endl;
	std::cout << "Be carefull! No GDPR at all!" << std::endl;
	return;
}

Phonebook::~Phonebook(void) {

	std::cout << "Miss you.." << std::endl;
	return;
}

int		Phonebook::isAvailableFreeMemory(void) const {

	if (memory)
		return (1);
	return (0);
}

int		Phonebook::isPhoneBookEmpty(void) const {

	if (memory == CONTACT_LIST_SIZE)
		return (1);
	return (0);
}

void	Phonebook::searchContact(void) const {

	std::string		request;

	if (Phonebook::isPhoneBookEmpty())
	{
		std::cout << "Add someone first." << std::endl;
		return;
	}
	else
	{
		std::cout << std::setw (10) << "Index" << "|" << std::setw (10) << "First Name" << "|" << std::setw (10) << "Last Name" << "|" << std::setw (10) << "Nickname" << std::endl;
		for (int i = 0; i < (CONTACT_LIST_SIZE - memory); i++)
		{
			contacts[i].printContactTruncatedData();
		}
		std::cout << "Print your request (index):" << std::endl;
		std::cin >> request;
		if (atoi(request.c_str()) < 1 || atoi(request.c_str()) > (CONTACT_LIST_SIZE - memory))
			std::cout << "Index you have been entered is wrong :-[. Try more." << std::endl;
		else
			contacts[atoi(request.c_str()) - 1].printContactFullData();
		return;
	}
}

void	Phonebook::addContact(void) {

	if (Phonebook::isAvailableFreeMemory())
	{
		contacts[CONTACT_LIST_SIZE - memory].fillContact(CONTACT_LIST_SIZE - memory + 1);
		memory--;
	}
	else
	{
		std::cout << "PHONEBOOK IS FULL!!!" << std::endl;
		std::cout << "The only way to free it - destroy it and rerun." << std::endl;
		std::cout << "To run destroyer print EXIT" << std::endl;
	}
	return;
}
