/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionWhile.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:45:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:11:59 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONWHILE_HPP
# define INSTRUCTIONWHILE_HPP

# include "AInstruction.hpp"

class InstructionWhile : public AInstruction {

public:
	InstructionWhile();
	virtual ~InstructionWhile();
	void		execute(Data &);
	char		getCharacter();

private:
	InstructionWhile(InstructionWhile const &);
	InstructionWhile		&operator=(InstructionWhile const &);
};

#endif
