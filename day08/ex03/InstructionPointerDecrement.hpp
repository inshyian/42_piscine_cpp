/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionPointerDecrement.hpp                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:44:16 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:26:59 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONPOINTERDECREMENT_HPP
# define INSTRUCTIONPOINTERDECREMENT_HPP

# include "AInstruction.hpp"

class InstructionPointerDecrement : public AInstruction {

public:
	InstructionPointerDecrement();
	virtual ~InstructionPointerDecrement();
	void		execute(Data &);
	char		getCharacter();

private:
	InstructionPointerDecrement(InstructionPointerDecrement const &);
	InstructionPointerDecrement		&operator=(InstructionPointerDecrement const &);
};

#endif
