/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IInstruction.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 19:57:43 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 19:48:28 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IINSTRUCTION_HPP
# define IINSTRUCTION_HPP

# include <iostream>
# include "parameters.h"
# include "Data.hpp"

class IInstruction {

public:
	virtual void	execute(Data &) = 0;
	virtual char	getCharacter() = 0;
};

#endif
