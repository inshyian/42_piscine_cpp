/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionByteDecrement.hpp                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:42:19 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:28:36 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONBYTEDECREMENT_HPP
# define INSTRUCTIONBYTEDECREMENT_HPP

# include "AInstruction.hpp"

class InstructionByteDecrement : public AInstruction {

public:
	InstructionByteDecrement();
	virtual ~InstructionByteDecrement();
	void		execute(Data &);
	char		getCharacter();

private:
	InstructionByteDecrement(InstructionByteDecrement const &);
	InstructionByteDecrement		&operator=(InstructionByteDecrement const &);
};

#endif
