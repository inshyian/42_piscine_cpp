/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Data.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:24:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 19:46:44 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_HPP
# define DATA_HPP

# include <list>
# include "parameters.h"

class IInstruction;

typedef	std::list<IInstruction *>::iterator	iIterator;

class Data {

public:
	Data();
	~Data();
	char						*getMemory();
	int							&getMemoryIterator();
	std::list<IInstruction *>	&getInstructions();
	iIterator					&getInstructionsIterator();
	void						initializeInstructionsIterator();

private:
	Data(Data const &);
	Data		&operator=(Data const &);
	std::list<IInstruction *>	_instructions;
	iIterator					_instructionsIterator;
	static char					_memory[MEMORY_SIZE];
	int							_memoryIterator;
};

#endif
