/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionGetByte.hpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:43:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:27:55 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONGETBYTE_HPP
# define INSTRUCTIONGETBYTE_HPP

# include "AInstruction.hpp"

class InstructionGetByte : public AInstruction {

public:
	InstructionGetByte();
	virtual ~InstructionGetByte();
	void		execute(Data &);
	char		getCharacter();

private:
	InstructionGetByte(InstructionGetByte const &);
	InstructionGetByte		&operator=(InstructionGetByte const &);
};

#endif
