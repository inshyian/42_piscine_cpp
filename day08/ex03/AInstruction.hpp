/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AInstruction.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 16:54:35 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:28:52 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AINSTRUCTION_HPP
# define AINSTRUCTION_HPP

# include "IInstruction.hpp"

class AInstruction : public IInstruction {

public:
	AInstruction();
	virtual ~AInstruction();
	virtual void	execute(Data &) = 0;
	virtual char	getCharacter() = 0;

	class OutOfMemoryRangeException : public std::exception {
	public:
		OutOfMemoryRangeException();
		OutOfMemoryRangeException(OutOfMemoryRangeException const &src);
		~OutOfMemoryRangeException() throw();
		OutOfMemoryRangeException	&operator=(OutOfMemoryRangeException const &rhs);
		virtual const char	*what() const throw();};

	class BadCycleException : public std::exception {
	public:
		BadCycleException();
		BadCycleException(BadCycleException const &src);
		~BadCycleException() throw();
		BadCycleException	&operator=(BadCycleException const &rhs);
		virtual const char	*what() const throw();};
};

#endif
