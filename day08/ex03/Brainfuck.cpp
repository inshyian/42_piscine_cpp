/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brainfuck.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 21:15:16 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 22:05:55 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brainfuck.hpp"

Brainfuck::Brainfuck() {};
Brainfuck::~Brainfuck() {};

void			Brainfuck::openFile(std::string const filename) {

	struct stat sb;
	if (stat(filename.c_str(), &sb) == 0 && S_ISREG(sb.st_mode))
	{
		_ifs.open(filename);
		if (!_ifs.good())
		{
			throw BadFileException();
		}
	}
	else
		throw BadFileException();
};

void			Brainfuck::closeFile() {

	_ifs.close();
};

Data			Brainfuck::_data;

void			Brainfuck::readCode() {

	if (!_ifs.is_open())
		throw NoFileProvidedException();
	std::stringstream	s_str;
	s_str << _ifs.rdbuf();
	std::string			str;
	str	= s_str.str();
	std::for_each(str.begin(), str.end(), Brainfuck::addInstruction);
};

void			Brainfuck::executeInstructions() {

	_data.initializeInstructionsIterator();
	while (_data.getInstructionsIterator() != _data.getInstructions().end())
	{
		if (DEBUG_MODE == 3)
			std::cout << "Now executes: " << (*_data.getInstructionsIterator())->getCharacter() << std::endl;
		(*_data.getInstructionsIterator())->execute(_data);
	}
};

void			Brainfuck::addInstruction(char const &char_) {

	IInstruction	*instruction;

	instruction = _iSource.getInstruction(char_);
	if (instruction)
	{
		_data.getInstructions().push_back(instruction);
	}
	if (DEBUG_MODE == 1)
	{
		std::cout << "src char: " << char_ << ", identified as ";
		if (_iSource.getInstruction(char_))
			std::cout << _iSource.getInstruction(char_)->getCharacter() << std::endl;
		else
			std::cout << "anyhow" << std::endl;
	}
};

Brainfuck::BadFileException::BadFileException() {};
Brainfuck::BadFileException::BadFileException(BadFileException const &) {};
Brainfuck::BadFileException::~BadFileException() throw() {};
Brainfuck::BadFileException &Brainfuck::BadFileException::operator=(BadFileException const &) {return (*this);}
const char* Brainfuck::BadFileException::what() const throw() {return "BadFileException";}

Brainfuck::NoFileProvidedException::NoFileProvidedException() {};
Brainfuck::NoFileProvidedException::NoFileProvidedException(NoFileProvidedException const &) {};
Brainfuck::NoFileProvidedException::~NoFileProvidedException() throw() {};
Brainfuck::NoFileProvidedException &Brainfuck::NoFileProvidedException::operator=(NoFileProvidedException const &) {return (*this);}
const char* Brainfuck::NoFileProvidedException::what() const throw() {return "NoFileProvidedException";}
