/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionsSource.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 22:19:26 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 22:01:48 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionsSource.hpp"
#include "InstructionByteDecrement.hpp"
#include "InstructionByteIncrement.hpp"
#include "InstructionDone.hpp"
#include "InstructionGetByte.hpp"
#include "InstructionPointerDecrement.hpp"
#include "InstructionPointerIncrement.hpp"
#include "InstructionPrintByte.hpp"
#include "InstructionWhile.hpp"

InstructionsSource::InstructionsSource() {};

InstructionsSource::~InstructionsSource() {};

InstructionCreators 	InstructionsSource::_instructionCreators[] = {
	[static_cast<int>(INSTRUCTION_BYTE_DECREMENT)] = 		InstructionsSource::getByteDecrement,
	[static_cast<int>(INSTRUCTION_BYTE_INCREMENT)] = 		InstructionsSource::getByteIncrement,
	[static_cast<int>(INSTRUCTION_DONE)] = 					InstructionsSource::getDone,
	[static_cast<int>(INSTRUCTION_GET_BYTE)] = 				InstructionsSource::getGetByte,
	[static_cast<int>(INSTRUCTION_POINTER_DECREMENT)] = 	InstructionsSource::getPointerDecrement,
	[static_cast<int>(INSTRUCTION_POINTER_INCREMENT)] = 	InstructionsSource::getPointerIncrement,
	[static_cast<int>(INSTRUCTION_PRINT_BYTE)] = 			InstructionsSource::getPrintByte,
	[static_cast<int>(INSTRUCTION_WHILE)] = 				InstructionsSource::getWhile
};

char	InstructionsSource::_validCharacters[] = {

	[0] = INSTRUCTION_BYTE_DECREMENT,
	[1] = INSTRUCTION_BYTE_INCREMENT,
	[2] = INSTRUCTION_DONE,
	[3] = INSTRUCTION_GET_BYTE,
	[4] = INSTRUCTION_POINTER_DECREMENT,
	[5] = INSTRUCTION_POINTER_INCREMENT,
	[6] = INSTRUCTION_PRINT_BYTE,
	[7] = INSTRUCTION_WHILE
};

std::string	InstructionsSource::_stdValidCharacters = _validCharacters;

IInstruction			*InstructionsSource::getInstruction(char const &char_) {

	if (std::isspace(char_))
		return nullptr;
	else if (_stdValidCharacters.find(char_) == std::string::npos)
	{
		if (DEBUG_MODE == 2)
			std::cout << "Incorrect character: " << char_ << std::endl;
	}
	else
		return (_instructionCreators[static_cast<int>(char_)]());
	return nullptr;
}

IInstruction			*InstructionsSource::getByteIncrement() {

	return (new InstructionByteIncrement);
};
;
IInstruction			*InstructionsSource::getByteDecrement() {

	return (new InstructionByteDecrement);
};

IInstruction			*InstructionsSource::getPointerIncrement() {

	return (new InstructionPointerIncrement);
};

IInstruction			*InstructionsSource::getPointerDecrement() {

	return (new InstructionPointerDecrement);
};

IInstruction			*InstructionsSource::getDone() {

	return (new InstructionDone);
};
;
IInstruction			*InstructionsSource::getWhile() {

	return (new InstructionWhile);
};

IInstruction			*InstructionsSource::getGetByte() {

	return (new InstructionGetByte);
};

IInstruction	*InstructionsSource::getPrintByte() {

	return (new InstructionPrintByte);
};
