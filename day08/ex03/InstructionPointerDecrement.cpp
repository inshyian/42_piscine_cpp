/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionPointerDecrement.cpp                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 11:00:14 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 19:06:15 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionPointerDecrement.hpp"

InstructionPointerDecrement::InstructionPointerDecrement() {};
InstructionPointerDecrement::InstructionPointerDecrement(InstructionPointerDecrement const &) {};
InstructionPointerDecrement::~InstructionPointerDecrement() {};
InstructionPointerDecrement		&InstructionPointerDecrement::operator=(InstructionPointerDecrement const &) {return (*this);};

void		InstructionPointerDecrement::execute(Data &data) {

	data.getMemoryIterator()--;
	data.getInstructionsIterator()++;
	if (data.getMemoryIterator() < 0)
		throw OutOfMemoryRangeException();
};

char		InstructionPointerDecrement::getCharacter() {

	return INSTRUCTION_POINTER_DECREMENT;
};
