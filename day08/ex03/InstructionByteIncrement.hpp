/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionByteIncrement.hpp                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:43:03 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:28:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONBYTEINCREMENT_HPP
# define INSTRUCTIONBYTEINCREMENT_HPP

# include "AInstruction.hpp"

class InstructionByteIncrement : public AInstruction {

public:
	InstructionByteIncrement();
	virtual ~InstructionByteIncrement();
	void		execute(Data &);
	char		getCharacter();

private:
	InstructionByteIncrement(InstructionByteIncrement const &);
	InstructionByteIncrement		&operator=(InstructionByteIncrement const &);
};

#endif
