/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionGetByte.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 11:00:14 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 21:49:00 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionGetByte.hpp"

InstructionGetByte::InstructionGetByte() {};
InstructionGetByte::InstructionGetByte(InstructionGetByte const &) {};
InstructionGetByte::~InstructionGetByte() {};
InstructionGetByte		&InstructionGetByte::operator=(InstructionGetByte const &) {return (*this);};

void		InstructionGetByte::execute(Data &data) {

	if (DEBUG_MODE)
	{
		std::cout << "Enter a character" << std::endl;
	}
	data.getMemory()[data.getMemoryIterator()] = getc(stdin);
	data.getInstructionsIterator()++;
};

char		InstructionGetByte::getCharacter() {

	return INSTRUCTION_GET_BYTE;
};
