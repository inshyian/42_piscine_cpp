/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 19:56:15 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 20:26:35 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** https://fatiherikli.github.io/brainfuck-visualizer/#
*/

#include "Brainfuck.hpp"

int		main(int argc, char **argv) {

	Brainfuck  fuck;

	if (argc == 2)
	{
		try
		{
			fuck.openFile(argv[1]);
			fuck.readCode();
			fuck.closeFile();
			fuck.executeInstructions();
		}
		catch (std::exception &e)
		{
			std::cout << e.what() << std::endl;
		}
	}
	else
		std::cout << "usage:\t./brainfuck <sourceFile>" << std::endl;
	return 0;
}
