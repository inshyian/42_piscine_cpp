/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parameters.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 12:29:54 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 22:07:02 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARAMETERS_H
# define PARAMETERS_H

# define MEMORY_SIZE 						30000 	// work memory size
# define DEBUG_MODE 						0 		// 0 - no debugging messages
													// 1 - identified chars
													// 2 - incorrect chars
													// 3 - instructions executing sequence

// Customized instructions
# define INSTRUCTION_BYTE_DECREMENT 		'{' // '-'
# define INSTRUCTION_BYTE_INCREMENT 		'}' // '+'
# define INSTRUCTION_DONE 					')' // ']'
# define INSTRUCTION_GET_BYTE 				'$' // ','
# define INSTRUCTION_POINTER_DECREMENT 		'|' // '<'
# define INSTRUCTION_POINTER_INCREMENT 		'/' // '>'
# define INSTRUCTION_PRINT_BYTE 			'#' // '.'
# define INSTRUCTION_WHILE 					'(' // '['

// Default Brainfuck instructons
// # define INSTRUCTION_BYTE_DECREMENT 		'-'
// # define INSTRUCTION_BYTE_INCREMENT 		'+'
// # define INSTRUCTION_DONE 					']'
// # define INSTRUCTION_GET_BYTE 				','
// # define INSTRUCTION_POINTER_DECREMENT 		'<'
// # define INSTRUCTION_POINTER_INCREMENT 		'>'
// # define INSTRUCTION_PRINT_BYTE 			'.'
// # define INSTRUCTION_WHILE 					'['

#endif
