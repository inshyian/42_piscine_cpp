/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionDone.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 11:00:13 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 19:22:12 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionDone.hpp"

InstructionDone::InstructionDone() {};
InstructionDone::InstructionDone(InstructionDone const &) {};
InstructionDone::~InstructionDone() {};
InstructionDone		&InstructionDone::operator=(InstructionDone const &) {return (*this);};

void		InstructionDone::execute(Data &data) {

	if (data.getMemory()[data.getMemoryIterator()] != 0)
	{
		int		bracketsSkipper = 0;
		while ((*data.getInstructionsIterator())->getCharacter() != INSTRUCTION_WHILE || bracketsSkipper != 0)
		{
			if (data.getInstructionsIterator() == data.getInstructions().begin())
			{
				throw BadCycleException();
			}
			data.getInstructionsIterator()--;
			if ((*data.getInstructionsIterator())->getCharacter() == INSTRUCTION_DONE)
			{
				bracketsSkipper++;
			}
			else if ((*data.getInstructionsIterator())->getCharacter() == INSTRUCTION_WHILE && bracketsSkipper)
			{
				bracketsSkipper--;
				data.getInstructionsIterator()--;
				if (data.getInstructionsIterator() == data.getInstructions().begin())
				{
					throw BadCycleException();
				}
			}
		}
		data.getInstructionsIterator()++;
	}
	else
		data.getInstructionsIterator()++;
};

char		InstructionDone::getCharacter() {

	return INSTRUCTION_DONE;
};
