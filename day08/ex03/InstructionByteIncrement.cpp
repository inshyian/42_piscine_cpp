/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionByteIncrement.cpp                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 11:00:12 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:31:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionByteIncrement.hpp"

InstructionByteIncrement::InstructionByteIncrement() {};
InstructionByteIncrement::InstructionByteIncrement(InstructionByteIncrement const &) {};
InstructionByteIncrement::~InstructionByteIncrement() {};
InstructionByteIncrement		&InstructionByteIncrement::operator=(InstructionByteIncrement const &) {return (*this);};

void		InstructionByteIncrement::execute(Data &data) {

	data.getMemory()[data.getMemoryIterator()]++;
	data.getInstructionsIterator()++;
};

char		InstructionByteIncrement::getCharacter() {

	return INSTRUCTION_BYTE_INCREMENT;
};
