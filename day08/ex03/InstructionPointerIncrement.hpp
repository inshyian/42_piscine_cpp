/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionPointerIncrement.hpp                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:44:54 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:26:44 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONPOINTERINCREMENT_HPP
# define INSTRUCTIONPOINTERINCREMENT_HPP

# include "AInstruction.hpp"

class InstructionPointerIncrement : public AInstruction {

public:
	InstructionPointerIncrement();
	virtual ~InstructionPointerIncrement();
	void		execute(Data &);
	char		getCharacter();

private:
	InstructionPointerIncrement(InstructionPointerIncrement const &);
	InstructionPointerIncrement		&operator=(InstructionPointerIncrement const &);
};

#endif
