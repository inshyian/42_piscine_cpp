/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionPrintByte.cpp                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 11:00:16 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:32:15 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionPrintByte.hpp"

InstructionPrintByte::InstructionPrintByte() {};
InstructionPrintByte::InstructionPrintByte(InstructionPrintByte const &) {};
InstructionPrintByte::~InstructionPrintByte() {};
InstructionPrintByte		&InstructionPrintByte::operator=(InstructionPrintByte const &) {return (*this);};

void		InstructionPrintByte::execute(Data &data) {

	std::cout << data.getMemory()[data.getMemoryIterator()];
	data.getInstructionsIterator()++;
};

char		InstructionPrintByte::getCharacter() {

	return INSTRUCTION_PRINT_BYTE;
};
