/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionPointerIncrement.cpp                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 11:00:15 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 21:45:22 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionPointerIncrement.hpp"

InstructionPointerIncrement::InstructionPointerIncrement() {};
InstructionPointerIncrement::InstructionPointerIncrement(InstructionPointerIncrement const &) {};
InstructionPointerIncrement::~InstructionPointerIncrement() {};
InstructionPointerIncrement		&InstructionPointerIncrement::operator=(InstructionPointerIncrement const &) {return (*this);};

void		InstructionPointerIncrement::execute(Data &data) {

	data.getMemoryIterator()++;
	data.getInstructionsIterator()++;
	if (data.getMemoryIterator() > MEMORY_SIZE - 1)
		throw OutOfMemoryRangeException();
};

char		InstructionPointerIncrement::getCharacter() {

	return INSTRUCTION_POINTER_INCREMENT;
};
