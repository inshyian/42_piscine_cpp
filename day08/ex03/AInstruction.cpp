/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AInstruction.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 16:54:37 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:07:54 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AInstruction.hpp"

AInstruction::AInstruction() {};

AInstruction::~AInstruction() {};

AInstruction::OutOfMemoryRangeException::OutOfMemoryRangeException() {};
AInstruction::OutOfMemoryRangeException::OutOfMemoryRangeException(OutOfMemoryRangeException const &) {};
AInstruction::OutOfMemoryRangeException::~OutOfMemoryRangeException() throw() {};
AInstruction::OutOfMemoryRangeException &AInstruction::OutOfMemoryRangeException::operator=(OutOfMemoryRangeException const &) {return (*this);}
const char* AInstruction::OutOfMemoryRangeException::what() const throw() {return "OutOfMemoryRangeException";}

AInstruction::BadCycleException::BadCycleException() {};
AInstruction::BadCycleException::BadCycleException(BadCycleException const &) {};
AInstruction::BadCycleException::~BadCycleException() throw() {};
AInstruction::BadCycleException &AInstruction::BadCycleException::operator=(BadCycleException const &) {return (*this);}
const char* AInstruction::BadCycleException::what() const throw() {return "BadCycleException";}
