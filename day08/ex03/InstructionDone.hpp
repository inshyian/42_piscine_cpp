/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionDone.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:43:21 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:14:35 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONDONE_HPP
# define INSTRUCTIONDONE_HPP

# include "AInstruction.hpp"

class InstructionDone : public AInstruction {

public:
	InstructionDone();
	virtual ~InstructionDone();
	void		execute(Data &);
	char		getCharacter();

private:
	InstructionDone(InstructionDone const &);
	InstructionDone		&operator=(InstructionDone const &);
};

#endif
