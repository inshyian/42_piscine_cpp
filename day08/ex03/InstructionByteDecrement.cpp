/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionByteDecrement.cpp                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 11:00:11 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:31:24 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionByteDecrement.hpp"

InstructionByteDecrement::InstructionByteDecrement() {};
InstructionByteDecrement::InstructionByteDecrement(InstructionByteDecrement const &) {};
InstructionByteDecrement::~InstructionByteDecrement() {};
InstructionByteDecrement		&InstructionByteDecrement::operator=(InstructionByteDecrement const &) {return (*this);};

void		InstructionByteDecrement::execute(Data &data) {

	data.getMemory()[data.getMemoryIterator()]--;
	data.getInstructionsIterator()++;
};

char		InstructionByteDecrement::getCharacter() {

	return INSTRUCTION_BYTE_DECREMENT;
};
