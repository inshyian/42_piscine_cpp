/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionPrintByte.hpp                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:45:12 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 17:28:02 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONPRINTBYTE_HPP
# define INSTRUCTIONPRINTBYTE_HPP

# include "AInstruction.hpp"

class InstructionPrintByte : public AInstruction {

public:
	InstructionPrintByte();
	virtual ~InstructionPrintByte();
	void		execute(Data &);
	char		getCharacter();

private:
	InstructionPrintByte(InstructionPrintByte const &);
	InstructionPrintByte		&operator=(InstructionPrintByte const &);
};


#endif
