/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Data.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 10:24:23 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 16:28:12 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Data.hpp"

Data::Data() : _memoryIterator(0) {};

Data::~Data() {};

void						Data::initializeInstructionsIterator() {

	_instructionsIterator = _instructions.begin();
}

char						Data::_memory[];

char						*Data::getMemory() {

	return (_memory);
};

int							&Data::getMemoryIterator() {

	return (_memoryIterator);
};

std::list<IInstruction *>	&Data::getInstructions() {

	return (_instructions);
};

iIterator					&Data::getInstructionsIterator() {

	return (_instructionsIterator);
};
