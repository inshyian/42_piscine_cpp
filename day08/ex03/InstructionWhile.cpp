/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionWhile.cpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 11:00:17 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 19:21:51 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "InstructionWhile.hpp"

InstructionWhile::InstructionWhile() {};
InstructionWhile::InstructionWhile(InstructionWhile const &) {};
InstructionWhile::~InstructionWhile() {};
InstructionWhile		&InstructionWhile::operator=(InstructionWhile const &) {return (*this);};

void		InstructionWhile::execute(Data &data) {

	if (data.getMemory()[data.getMemoryIterator()] == 0)
	{
		int		bracketsSkipper = 0;
		while ((*data.getInstructionsIterator())->getCharacter() != INSTRUCTION_DONE || bracketsSkipper != 0)
		{
			data.getInstructionsIterator()++;
			if (data.getInstructionsIterator() == data.getInstructions().end())
			{
				throw BadCycleException();
			}
			if ((*data.getInstructionsIterator())->getCharacter() == INSTRUCTION_WHILE)
			{
				bracketsSkipper++;
			}
			else if ((*data.getInstructionsIterator())->getCharacter() == INSTRUCTION_DONE && bracketsSkipper)
			{
				data.getInstructionsIterator()++;
				if (data.getInstructionsIterator() == data.getInstructions().end())
				{
					throw BadCycleException();
				}
				bracketsSkipper--;
			}
		}
		data.getInstructionsIterator()++;
	}
	else
		data.getInstructionsIterator()++;
};

char		InstructionWhile::getCharacter() {

	return INSTRUCTION_WHILE;
};
