/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   InstructionsSource.hpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 22:19:23 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 19:48:11 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INSTRUCTIONSSOURCE_HPP
# define INSTRUCTIONSSOURCE_HPP

# include "parameters.h"
# include "IInstruction.hpp"

typedef	IInstruction		*(*InstructionCreators)();

class InstructionsSource {

public:
	InstructionsSource();
	~InstructionsSource();
	static IInstruction 		*getInstruction(char const &);
	void						initializeInstructionCreators();
	void						initializeValidCharacters();

private:
	InstructionsSource(InstructionsSource const &);
	InstructionsSource			&operator=(InstructionsSource const &);
	static IInstruction			*getByteIncrement();
	static IInstruction			*getByteDecrement();
	static IInstruction			*getPointerIncrement();
	static IInstruction			*getPointerDecrement();
	static IInstruction			*getDone();
	static IInstruction			*getWhile();
	static IInstruction			*getGetByte();
	static IInstruction			*getPrintByte();

	static InstructionCreators 	_instructionCreators[127];
	static char				 	_validCharacters[9];
	static std::string		 	_stdValidCharacters;
};

#endif
