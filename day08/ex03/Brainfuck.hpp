/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brainfuck.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 21:15:13 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 20:17:22 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BRAINFUCK_HPP
# define BRAINFUCK_HPP

# include <sys/stat.h>
# include <sstream>
# include <algorithm>
# include <fstream>
# include <list>
# include "IInstruction.hpp"
# include "Data.hpp"
# include "InstructionsSource.hpp"
# include "parameters.h"

class Brainfuck {

public:
	Brainfuck();
	~Brainfuck();

	typedef	std::list<IInstruction>::iterator iterator;

	void						openFile(std::string const filename);
	void						readCode();
	void						closeFile();
	void						executeInstructions();

	class BadFileException : public std::exception {
	public:
		BadFileException();
		BadFileException(BadFileException const &src);
		~BadFileException() throw();
		BadFileException	&operator=(BadFileException const &rhs);
		virtual const char	*what() const throw();};

	class NoFileProvidedException : public std::exception {
	public:
		NoFileProvidedException();
		NoFileProvidedException(NoFileProvidedException const &src);
		~NoFileProvidedException() throw();
		NoFileProvidedException	&operator=(NoFileProvidedException const &rhs);
		virtual const char	*what() const throw();};

private:
	Brainfuck(Brainfuck const &);
	Brainfuck					&operator=(Brainfuck const &);
	std::ifstream				_ifs;
	static void					addInstruction(char const &);
	static Data					_data;
	static InstructionsSource	_iSource;
};

#endif
