/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 11:41:20 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 14:24:36 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_HPP
# define EASYFIND_HPP

# include <map>
# include <iostream>
# include <list>
# include <vector>

class NonexistentItemWasRequestedException : public std::exception {

public:
	NonexistentItemWasRequestedException() {};
	NonexistentItemWasRequestedException(NonexistentItemWasRequestedException const &) {};
	~NonexistentItemWasRequestedException() throw() {};
	NonexistentItemWasRequestedException	&operator=(NonexistentItemWasRequestedException const &) {
		return *this;
	};

	virtual const char		*what() const throw() {

		return "NonexistentItemWasRequestedException";
	};
};

template<typename T>
void		easyfind(T &container, int int_) {

	typename T::iterator		element;

	element = find(container.begin(), container.end(), int_);
	if (element != container.end())
	{
		std::cout << "Element found in container" << std::endl;
		return ;
	}
	throw NonexistentItemWasRequestedException();
}

#endif
