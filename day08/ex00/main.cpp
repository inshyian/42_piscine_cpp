/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 11:41:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 14:25:32 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "easyfind.hpp"

int		main() {

	std::list<int>		container_l;
	std::vector<int>	container_v;

	container_l.push_back(1);
	container_l.push_back(10);
	container_l.push_back(20);

	container_v.push_back(1);
	container_v.push_back(10);
	container_v.push_back(20);

	easyfind(container_l, 1);
	easyfind(container_v, 20);

	try
	{
		easyfind(container_v, 21);
	}
	catch (NonexistentItemWasRequestedException &e)
	{
		std::cout << e.what() << std::endl;
	}
	return 0;
}
