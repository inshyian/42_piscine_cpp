/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 14:27:45 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 16:43:20 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"

int		main () {

	Span sp = Span(5);

	sp.addNumber(5);
	sp.addNumber(3);
	sp.addNumber(17);
	sp.addNumber(9);
	sp.addNumber(11);
	try
	{
		sp.addNumber(11);
	}
	catch (Span::ContainerOverflowException &e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << sp.shortestSpan() << std::endl;
	std::cout << sp.longestSpan() << std::endl;

	Span sp_ = Span(1);

	sp_.addNumber(5);

	try
	{
		std::cout << sp_.shortestSpan() << std::endl;
	}
	catch (Span::LessThenTwoElementsException &e)
	{
		std::cout << e.what() << std::endl;
	}

	return (0);
}
