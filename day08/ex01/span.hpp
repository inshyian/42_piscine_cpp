/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 14:27:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 17:47:54 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
# define SPAN_HPP

# include <iostream>
# include <list>
# include <vector>

class Span {

public:
	Span();
	Span(unsigned int);
	Span(Span const &);
	~Span();

	Span	&operator=(Span const &);

	void	addNumber(int);
	int		shortestSpan();
	int		longestSpan();

	class ContainerOverflowException : public std::exception {

	public:
		ContainerOverflowException();
		ContainerOverflowException(ContainerOverflowException const &src);
		~ContainerOverflowException() throw();
		ContainerOverflowException	&operator=(ContainerOverflowException const &rhs);

		virtual const char		*what() const throw();
	};

	class LessThenTwoElementsException : public std::exception {

	public:
		LessThenTwoElementsException();
		LessThenTwoElementsException(LessThenTwoElementsException const &src);
		~LessThenTwoElementsException() throw();
		LessThenTwoElementsException	&operator=(LessThenTwoElementsException const &rhs);

		virtual const char		*what() const throw();
	};

private:
	unsigned int		_n;
	std::list<int>		_container;
};

#endif
