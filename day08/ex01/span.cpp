/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 14:27:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 16:41:30 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"

Span::Span() {};

Span::Span(unsigned int num) : _n(num) {}

Span::Span(Span const &src) {

	*this = src;
}

Span::~Span() {};

Span	&Span::operator=(Span const &rhs) {

	if (this != &rhs)
	{
		_container = rhs._container;
		_n = rhs._n;
	}
	return (*this);
};

void	Span::addNumber(int num) {

	if (_container.size() == _n)
		throw ContainerOverflowException();
	_container.push_back(num);

};

int		Span::shortestSpan() {

	if (_container.size() <= 1)
		throw LessThenTwoElementsException();

	std::list<int>::iterator	i = _container.begin();
	std::list<int>::iterator	i_curr = _container.begin();
	std::list<int>::iterator	i_last = _container.end();

	i_last--;

	int		diff = 0;
	int		diff_curr = 0;

	diff = abs(*_container.begin() - *i_last);

	diff_curr = diff;
	while (i_curr != _container.end())
	{
		while (i != _container.end())
		{
			diff_curr = abs(*i - *i_curr);
			if (diff_curr < diff && i != i_curr)
				diff = diff_curr;
			i++;
		}
		i_curr++;
		i = _container.begin();
	}
	return (diff);
};

int		Span::longestSpan() {

	if (_container.size() <= 1)
		throw LessThenTwoElementsException();

	std::list<int>::iterator	i = _container.begin();
	std::list<int>::iterator	i_curr = _container.begin();
	std::list<int>::iterator	i_last = _container.end();

	i_last--;

	int		diff = 0;
	int		diff_curr = 0;

	diff = abs(*_container.begin() - *i_last);

	diff_curr = diff;
	while (i_curr != _container.end())
	{
		while (i != _container.end())
		{
			diff_curr = abs(*i - *i_curr);
			if (diff_curr > diff && i != i_curr)
				diff = diff_curr;
			i++;
		}
		i_curr++;
		i = _container.begin();
	}
	return (diff);
};

Span::ContainerOverflowException::ContainerOverflowException() {};

Span::ContainerOverflowException::ContainerOverflowException(ContainerOverflowException const &) {};

Span::ContainerOverflowException::~ContainerOverflowException() throw() {};

Span::ContainerOverflowException	&Span::ContainerOverflowException::operator=(ContainerOverflowException const &) {

	return (*this);
}

const char* Span::ContainerOverflowException::what() const throw() {

	return "ContainerOverflowException";
}

Span::LessThenTwoElementsException::LessThenTwoElementsException() {};

Span::LessThenTwoElementsException::LessThenTwoElementsException(LessThenTwoElementsException const &) {};

Span::LessThenTwoElementsException::~LessThenTwoElementsException() throw() {};

Span::LessThenTwoElementsException	&Span::LessThenTwoElementsException::operator=(LessThenTwoElementsException const &) {

	return (*this);
}

const char* Span::LessThenTwoElementsException::what() const throw() {

	return "LessThenTwoElementsException";
}
