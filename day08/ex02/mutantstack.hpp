/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutantstack.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 16:45:21 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/11 19:48:28 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MUTANTSTACK_HPP
# define MUTANTSTACK_HPP

#include <iostream>
#include <stack>

template<typename T>
class MutantStack : public std::stack<T> {

public:
	MutantStack() : std::stack<T>() {};
	MutantStack(MutantStack const &src) : std::stack<T>(src) {};
	MutantStack(std::stack<T> const &src) : std::stack<T>(src) {};
	virtual ~MutantStack() {};

	using std::stack<T>::operator=;

	typedef	typename std::deque<T>::iterator	iterator;

	iterator	begin() {

		return (std::stack<T>::c.begin());
	};

	iterator	end() {

		return (std::stack<T>::c.end());
	};
};

#endif
