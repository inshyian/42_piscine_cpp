/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/11 18:12:55 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/12 12:58:01 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mutantstack.hpp"

int		main() {

	std::cout << "Default tests ..." << std::endl;

	MutantStack<int> mstack;

	mstack.push(5);
	mstack.push(17);
	std::cout << mstack.top() << std::endl;
	mstack.pop();
	std::cout << mstack.size() << std::endl;

	mstack.push(3);
	mstack.push(5);
	mstack.push(737);

	MutantStack<int>::iterator it = mstack.begin();
	MutantStack<int>::iterator ite = mstack.end();

	++it;
	--it;

	while (it != ite)
	{
		std::cout << *it << std::endl;
		++it;
	}

	std::stack<int> s(mstack);
/* ************************************************************************** */
	std::cout << "Print healthy stack copied from MutantStack ..." << std::endl;

	while (!s.empty())
	{
		std::cout << s.top() << std::endl;
		s.pop();
	}

/* ************************************************************************** */
	MutantStack<int> mstack_;
	mstack_ = mstack;

	std::cout << "Print copy stack ..." << std::endl;

	it = mstack_.begin();
	ite = mstack_.end();

	while (it != ite)
	{
		std::cout << *it << std::endl;
		++it;
	}

	return 0;
}
