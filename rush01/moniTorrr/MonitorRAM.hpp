/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorRAM.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 10:00:10 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 19:14:13 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MONITORRAM_HPP
# define MONITORRAM_HPP

# include "AMonitorModule.hpp"

class MonitorRAM : public AMonitorModule {

public:
	MonitorRAM();
	virtual ~MonitorRAM();
	virtual void						initModule();
	virtual void						updateInfo();

private:
	MonitorRAM(MonitorRAM const &);
	MonitorRAM		&operator=(MonitorRAM const &);
	std::string		getRAMGeneralSize();
	std::string		getRAMCurrentUse();
	std::string		getRAMCurrentFree();
};

#endif
