/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorRAM.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 10:00:08 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 20:57:25 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sstream>
#include "MonitorRAM.hpp"
#include <sys/time.h>
#include <sys/resource.h>

MonitorRAM::MonitorRAM() : AMonitorModule("RAM info") {};

MonitorRAM::~MonitorRAM() {};

void						MonitorRAM::initModule() {

	t_data					data;

	data.name = "RAM total size";
	data.value = getRAMGeneralSize() + "M";
	_iformationStrings.push_back(data);

	data.name = "Used";
	data.value = getRAMCurrentUse();
	_iformationStrings.push_back(data);

	data.name = "Available";
	data.value = getRAMCurrentFree();
	_iformationStrings.push_back(data);
};

void						MonitorRAM::updateInfo() {

	std::list<t_data>::iterator		iterator;
	iterator = _iformationStrings.begin();
	++iterator;
	(*iterator).value = getRAMCurrentUse();
	++iterator;
	(*iterator).value = getRAMCurrentFree();
};

std::string					MonitorRAM::getRAMGeneralSize() {

	std::string 		ret = getReturnFromCommand("sysctl hw.memsize | awk '{print $2}'");
	std::stringstream	ss;

	ss << ret;
	unsigned long		ramSize;
	ss >> ramSize;
	ramSize = ramSize / 1024000;
	std::stringstream	ss_;
	ss_ << ramSize;
	ret = ss_.str();
	return (ret);
}

std::string					MonitorRAM::getRAMCurrentUse() {

	return getReturnFromCommand("top -l 1 | grep PhysMem | awk '{print $2}' | tr -d '\n'");
}

std::string					MonitorRAM::getRAMCurrentFree() {

	return getReturnFromCommand("top -l 1 | grep PhysMem | awk '{print $6}' | tr -d '\n'");;
}
