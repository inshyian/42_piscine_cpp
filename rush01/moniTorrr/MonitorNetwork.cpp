/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorNetwork.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 09:59:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 21:30:27 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MonitorNetwork.hpp"

MonitorNetwork::MonitorNetwork() : AMonitorModule("Network information") {};

MonitorNetwork::~MonitorNetwork() {};

void						MonitorNetwork::initModule() {

	t_data					data;

	data.name = "IP Address";
	data.value = getIPAddress();
	_iformationStrings.push_back(data);

	data.name = "MAC Address";
	data.value = getMACAddress();
	_iformationStrings.push_back(data);

	data.name = "Gateway";
	data.value = getGateway();
	_iformationStrings.push_back(data);
};

void						MonitorNetwork::updateInfo() {}

std::string					MonitorNetwork::getIPAddress() {

	return getReturnFromCommand("ipconfig getifaddr en0 | tr -d '\n'");
}

std::string					MonitorNetwork::getMACAddress() {

	return getReturnFromCommand("ifconfig -a | grep ether | head -1 | awk '{print $2}' | tr -d '\n'");
}

std::string					MonitorNetwork::getGateway() {

	return getReturnFromCommand("route -n get default | grep \"gateway: \" | awk '{print $2}' | tr -d '\n'");
}
