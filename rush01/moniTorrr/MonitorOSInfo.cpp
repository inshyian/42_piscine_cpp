/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorOSInfo.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 10:00:03 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 21:22:58 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MonitorOSInfo.hpp"

MonitorOSInfo::MonitorOSInfo() : AMonitorModule("OS Info") {};

MonitorOSInfo::~MonitorOSInfo() {};

void						MonitorOSInfo::initModule() {

	t_data			data;
	static char		buf[BUFF_SIZE + 1];
	FILE			*file;
	std::string		string;
	size_t			pos;
	size_t			posENDL;

	file = popen("sw_vers", "r");
	if ((fgets(buf, sizeof(buf) - 1, file)) != NULL)
	{
		data.name = "ProductName";
		string = buf;
		pos = string.find('\t');
		posENDL = string.find('\n', pos + 1);
		data.value = string.substr(pos + 1, posENDL - pos - 1);
		_iformationStrings.push_back(data);
	}
	if ((fgets(buf, sizeof(buf) - 1, file)) != NULL)
	{
		data.name = "ProductVersion";
		string = buf;
		pos = string.find('\t');
		posENDL = string.find('\n', pos + 1);
		data.value = string.substr(pos + 1, posENDL - pos - 1);
		_iformationStrings.push_back(data);
	}
	if ((fgets(buf, sizeof(buf) - 1, file)) != NULL)
	{
		data.name = "BuildVersion";
		string = buf;
		pos = string.find('\t');
		posENDL = string.find('\n', pos + 1);
		data.value = string.substr(pos + 1, posENDL - pos - 1);
		_iformationStrings.push_back(data);
	}
};

void						MonitorOSInfo::updateInfo() {}
