/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Core.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 09:54:34 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 17:10:38 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CORE_HPP
# define CORE_HPP

# include <vector>
# include "IMonitorModule.hpp"
# include "IMonitorDisplay.hpp"
# include "MonitorCPU.hpp"
# include "MonitorDateTime.hpp"
# include "MonitorHostnameUsername.hpp"
# include "MonitorNetwork.hpp"
# include "MonitorOSInfo.hpp"
# include "MonitorRAM.hpp"

class Core	{

public:
	Core();
	explicit Core(t_parameters parameters);
	~Core();
	void							run();
	t_parameters					&getParameters();
	void							initParameters(t_parameters parameters);

private:

	void							createMonitors();
	void							initMonitors();
	void							refreshMonitors();
	void							displayMonitors();
	void							freeMonitors();

	void							prepareDisplays();
	void							initDisplays();
	void							destroyDisplays();
	void							freeDisplays();

	std::list<IMonitorModule *>		_IMonitorModules;
	std::vector<IMonitorDisplay *>	_IMonitorDisplays;
	t_parameters					_parameters;

	Core(Core const &);
	Core	&operator=(Core const &);
};

#endif
