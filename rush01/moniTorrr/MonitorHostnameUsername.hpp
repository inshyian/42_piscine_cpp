/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorHostnameUsername.hpp                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 22:58:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 15:57:02 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MONITORHOSTNAMEUSERNAME_HPP
# define MONITORHOSTNAMEUSERNAME_HPP

# include <unistd.h>
# include "AMonitorModule.hpp"

class MonitorHostnameUsername : public AMonitorModule {

public:
	MonitorHostnameUsername();
	virtual ~MonitorHostnameUsername();
	virtual void						initModule();
	virtual void						updateInfo();

private:
	MonitorHostnameUsername(MonitorHostnameUsername const &);
	MonitorHostnameUsername		&operator=(MonitorHostnameUsername const &);

};

#endif
