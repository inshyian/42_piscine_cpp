/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   monitor.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 22:47:41 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 22:03:52 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MONITOR_HPP
# define MONITOR_HPP

# define MONITOR_DATE_TIME			1
# define MONITOR_HOSTNAME_USERNAME	1
# define MONITOR_OS_INFO			1
# define MONITOR_CPU				1
# define MONITOR_RAM				1
# define MONITOR_NETWORK			1

# define NCURSES					1
# define GRAPHIC					1

# include <iostream>

namespace endProcess {

	extern bool	ncurses;
	extern bool	graphic;
}

typedef struct s_data {

	std::string		name;
	std::string		value;

}				t_data;

typedef struct s_parameters {

	bool		MonitorDateTime;
	bool		MonitorHostnameUsername;
	bool		MonitorOSInfo;
	bool		MonitorCPU;
	bool		MonitorRAM;
	bool		MonitorNetwork;

	bool		ncurses;
	bool		graphic;

}				t_parameters;

t_parameters	readArgs(int argc, char **argv);
void			printUsage();

#endif
