/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMonitorModule.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 23:07:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 19:54:17 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMonitorModule.hpp"

AMonitorModule::AMonitorModule() {};
AMonitorModule::AMonitorModule(std::string moduleName) : _moduleName(moduleName) {};
AMonitorModule::~AMonitorModule() {};

std::string const			&AMonitorModule::getModuleName() {

	return (_moduleName);
};

std::list<t_data> const		&AMonitorModule::getIformationStrings() {

	return (_iformationStrings);
};

std::string 				AMonitorModule::getReturnFromCommand(const char *command) {

	std::string				ret;
	char					buf[BUFF_SIZE + 1];

	try
	{
		FILE *input	= popen(command, "r");
		while (!feof(input))
		{
			if (fgets(buf, BUFF_SIZE, input))
				ret += buf;
		}
		pclose(input);
	}
	catch (std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}
	return (ret);
};
