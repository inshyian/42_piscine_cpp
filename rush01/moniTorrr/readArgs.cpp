#include "monitor.hpp"

namespace
{
	typedef struct	s_parametersMarked
	{
		char 		name;
		bool		&value;
	}				t_parametersMarked;

	t_parameters	getZeroingParameters()
	{
		t_parameters parameters;

		parameters.ncurses = false;
		parameters.graphic = false;
		parameters.MonitorDateTime = false;
		parameters.MonitorHostnameUsername = false;
		parameters.MonitorOSInfo = false;
		parameters.MonitorCPU = false;
		parameters.MonitorRAM = false;
		return parameters;
	}
}

t_parameters readArgs(int argc, char **argv)
{
	t_parameters parameters = getZeroingParameters();
	std::string parameterMark;
	t_parametersMarked parametersMarked[] = {
			{'t', parameters.ncurses},
			{'v', parameters.graphic},
			{'h', parameters.MonitorHostnameUsername},
			{'O', parameters.MonitorOSInfo},
			{'d', parameters.MonitorDateTime},
			{'C', parameters.MonitorCPU},
			{'R', parameters.MonitorRAM},
			{'n', parameters.MonitorNetwork}
	};
	bool	displayType = false;
	bool	module = false;

	for (int i = 1; i < argc; i++)
	{
		parameterMark = argv[i];
		if (i == 1 && parameterMark != "-v" && parameterMark != "-t")
		{
			printUsage();
			std::exit(0);
		}
		for (size_t j = 0; j < parameterMark.length(); j++)
		{
			for (size_t k = 0; k < sizeof(parametersMarked) / sizeof(t_parametersMarked); k++)
				if (parametersMarked[k].name == parameterMark[j])
					parametersMarked[k].value = true;
		}
	}
	for (size_t i = 0; i < sizeof(parametersMarked) / sizeof(t_parametersMarked); i++)
	{
		if (i < 2 && parametersMarked[i].value)
			displayType = true;
		else if (parametersMarked[i].value)
		{
			module = true;
			break;
		}
	}
	if (!displayType || !module)
	{
		printUsage();
		exit(0);
	}

	return (parameters);

}