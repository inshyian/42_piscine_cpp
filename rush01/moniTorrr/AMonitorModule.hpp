/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMonitorModule.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 22:51:11 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 22:04:20 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMONITORMODULE_HPP
# define AMONITORMODULE_HPP

# define BUFF_SIZE 255

# include <sys/sysctl.h>
# include <sys/types.h>
# include "IMonitorModule.hpp"

class AMonitorModule : public IMonitorModule {

public:
	AMonitorModule();
	AMonitorModule(std::string moduleName);
	virtual ~AMonitorModule();
	virtual std::string const			&getModuleName();
	virtual std::list<t_data> const 	&getIformationStrings();
	virtual void						initModule() = 0;
	virtual void						updateInfo() = 0;

protected:
	std::string			getReturnFromCommand(const char *command);
	std::string			_moduleName;
	std::list<t_data>	_iformationStrings;

private:
	AMonitorModule(AMonitorModule const &);
	AMonitorModule		&operator=(AMonitorModule const &);
};

#endif
