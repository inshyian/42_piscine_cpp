/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorCPU.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 09:59:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 20:56:04 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sstream>
#include "MonitorCPU.hpp"

MonitorCPU::MonitorCPU() : AMonitorModule("CPU info") {};

MonitorCPU::~MonitorCPU() {};

void						MonitorCPU::initModule() {

	t_data					data;

	data.name = "CPU";
	data.value = getCPUName();
	_iformationStrings.push_back(data);

	data.name = "Frequency";
	data.value = getCPUFrequency() + "GHz";
	_iformationStrings.push_back(data);

	data.name = "Cores";
	data.value = getCPUCores();
	_iformationStrings.push_back(data);

	data.name = "Current usage";
	data.value = getCPULoad() + "%";
	_iformationStrings.push_back(data);
};

void						MonitorCPU::updateInfo() {

	std::list<t_data>::iterator		iterator;
	iterator = _iformationStrings.begin();
	++iterator;
	(*iterator).value = getCPUFrequency() + "GHz";
	++iterator;
	++iterator;
	(*iterator).value = getCPULoad() + "%";
};

std::string					MonitorCPU::getCPUName() {

	char			buf[BUFF_SIZE + 1];
	size_t 			size_ = sizeof(buf);
	sysctlbyname("machdep.cpu.brand_string", &buf, &size_, NULL, 0);
	return (buf);
};

std::string					MonitorCPU::getCPUFrequency() {

	uint64_t 	freq = 0;
	size_t 		size = sizeof(freq);
	sysctlbyname("hw.cpufrequency", &freq, &size, NULL, 0);

	float	 	freq_f = static_cast<float>(freq) / 1000000000;
	std::ostringstream ss;
	ss << freq_f;
	return (ss.str());
};

std::string					MonitorCPU::getCPUCores() {

	int			cores;
	size_t 		size = sizeof(cores);
	sysctlbyname("hw.ncpu", &cores, &size, NULL, 0);
	std::ostringstream ss;
	ss << cores;
	return (ss.str());
};

std::string					MonitorCPU::getCPULoad() {

	return (getReturnFromCommand("ps -A -o %cpu | awk '{s+=$1} END {print s}' | tr -d '\n'"));
};
