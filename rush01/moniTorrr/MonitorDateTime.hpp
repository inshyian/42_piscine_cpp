/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorDateTime.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 09:59:38 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 18:26:41 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MONITORDATETIME_HPP
# define MONITORDATETIME_HPP

# include <unistd.h>
# include "AMonitorModule.hpp"

class MonitorDateTime : public AMonitorModule {

public:
	MonitorDateTime();
	virtual ~MonitorDateTime();
	virtual void						initModule();
	virtual void						updateInfo();

private:
	MonitorDateTime(MonitorDateTime const &);
	MonitorDateTime		&operator=(MonitorDateTime const &);
	std::string			getTime();
	std::string			getDate();
};

#endif
