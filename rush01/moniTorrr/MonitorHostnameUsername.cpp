/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorHostnameUsername.cpp                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 23:01:03 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 21:23:15 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MonitorHostnameUsername.hpp"

MonitorHostnameUsername::MonitorHostnameUsername() : AMonitorModule("Hostname and Username") {};

MonitorHostnameUsername::~MonitorHostnameUsername() {};

void						MonitorHostnameUsername::initModule() {

	t_data					data;
	static char				hostname[BUFF_SIZE + 1];

	gethostname(hostname, BUFF_SIZE);
	data.name = "Hostname";
	data.value = hostname;
	_iformationStrings.push_back(data);

	data.name = "Username";
	data.value = getenv("USER");
	_iformationStrings.push_back(data);
};

void						MonitorHostnameUsername::updateInfo() {}
