/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorOSInfo.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 10:00:06 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 15:20:56 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MONITOROSINFO_HPP
# define MONITOROSINFO_HPP

#include "AMonitorModule.hpp"

class MonitorOSInfo : public AMonitorModule {

public:
	MonitorOSInfo();
	virtual ~MonitorOSInfo();
	virtual void						initModule();
	virtual void						updateInfo();

private:
	MonitorOSInfo(MonitorOSInfo const &);
	MonitorOSInfo		&operator=(MonitorOSInfo const &);

};

#endif
