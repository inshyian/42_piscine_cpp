#pragma once

# define MLX_WIN_WIDTH 500
# define MLX_WIN_HEIGHT 1200
# define MLX_WIN_NAME "Display Map with MiniLIBX"

# include "mlx.h"
# include "IMonitorDisplay.hpp"

class DisplayGraphic : public IMonitorDisplay
{
public:
	DisplayGraphic();

	virtual ~DisplayGraphic();

	void 	initDisplay();

	void 	destroyDisplay();

	void 	placeMonitorsInWindow(std::list<IMonitorModule *> &list);

private:
	DisplayGraphic(DisplayGraphic const &);
	DisplayGraphic		&operator=(DisplayGraphic const &);
	static int			exit_x();
	int					_width;
	int					_height;
	void				*_mlx_ptr;
	void				*_win_ptr;
	int					_bpp;
	int					_size_line;
	int					_endian;
};
