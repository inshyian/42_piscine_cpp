/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IMonitorModule.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 21:30:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 14:20:48 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IMONITORMODULE_HPP
# define IMONITORMODULE_HPP

# include <iostream>
# include <list>
# include "monitor.hpp"

class IMonitorModule {

public:
	virtual	~IMonitorModule() {};
	virtual std::string const			&getModuleName() = 0;
	virtual std::list<t_data> const 	&getIformationStrings() = 0;
	virtual void						initModule() = 0;
	virtual void						updateInfo() = 0;
};

#endif
