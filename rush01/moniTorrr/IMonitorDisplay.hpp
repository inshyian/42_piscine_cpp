/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IMonitorDisplay.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 21:30:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 14:41:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IMONITORDISPLAY_HPP
# define IMONITORDISPLAY_HPP

# include "IMonitorModule.hpp"
# include "monitor.hpp"

class IMonitorDisplay {

public:
	virtual	~IMonitorDisplay() {};
	virtual void	initDisplay() = 0;
	virtual void	destroyDisplay() = 0;
	virtual void	placeMonitorsInWindow(std::list<IMonitorModule *> &) = 0;
};

#endif
