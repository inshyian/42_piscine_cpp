/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Core.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 09:54:32 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 23:10:49 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Core.hpp"
#include "DisplayNcurses.hpp"
#include "DisplayGraphic.hpp"

Core::Core() {}

Core::~Core() {

	freeMonitors();
	freeDisplays();
}

void		Core::run() {

	createMonitors();
	initMonitors();
	prepareDisplays();
	initDisplays();
	while (!endProcess::graphic && !endProcess::ncurses)
	{
		refreshMonitors();
		displayMonitors();
		sleep(1);
	}
	destroyDisplays();
}

void		Core::initParameters(t_parameters parameters) {

	_parameters = parameters;
}

void		Core::createMonitors() {

	if (_parameters.MonitorHostnameUsername)
		_IMonitorModules.push_back(new MonitorHostnameUsername);
	if (_parameters.MonitorOSInfo)
		_IMonitorModules.push_back(new MonitorOSInfo);
	if (_parameters.MonitorDateTime)
		_IMonitorModules.push_back(new MonitorDateTime);
	if (_parameters.MonitorCPU)
		_IMonitorModules.push_back(new MonitorCPU);
	if (_parameters.MonitorRAM)
		_IMonitorModules.push_back(new MonitorRAM);
	if (_parameters.MonitorNetwork)
		_IMonitorModules.push_back(new MonitorNetwork);
}

void		Core::initMonitors() {

	std::list<IMonitorModule *>::iterator	iterator;

	iterator = _IMonitorModules.begin();
	for (; iterator != _IMonitorModules.end(); ++iterator)
	{
		(*iterator)->initModule();
	}
}

void		Core::refreshMonitors() {

	std::list<IMonitorModule *>::iterator	iterator;

	iterator = _IMonitorModules.begin();
	for (; iterator != _IMonitorModules.end(); ++iterator)
	{
		(*iterator)->updateInfo();
	}
}

void		Core::displayMonitors() {

	std::vector<IMonitorDisplay *>::iterator	iteratorD;

	iteratorD = _IMonitorDisplays.begin();
	for (; iteratorD != _IMonitorDisplays.end(); ++iteratorD)
	{
		(*iteratorD)->placeMonitorsInWindow(_IMonitorModules);
	}
}

void		Core::freeMonitors() {

	std::list<IMonitorModule *>::iterator	iterator;

	iterator = _IMonitorModules.begin();
	for (; iterator != _IMonitorModules.end(); ++iterator)
	{
		delete (*iterator);
	}
}

void		Core::prepareDisplays() {

	if (_parameters.ncurses)
		_IMonitorDisplays.push_back(new DisplayNcurses);
	if (_parameters.graphic)
		_IMonitorDisplays.push_back(new DisplayGraphic);
}

void		Core::initDisplays() {

	std::vector<IMonitorDisplay *>::iterator	iterator;

	for (iterator = _IMonitorDisplays.begin();
			iterator != _IMonitorDisplays.end();
			++iterator
			)
	{
		(*iterator)->initDisplay();
	}
}

void		Core::destroyDisplays() {

	std::vector<IMonitorDisplay *>::iterator	iterator;

	for (iterator = _IMonitorDisplays.begin();
		 iterator != _IMonitorDisplays.end();
		 ++iterator
			)
	{
		(*iterator)->destroyDisplay();
	}
}

void		Core::freeDisplays() {

	std::vector<IMonitorDisplay *>::iterator	iterator;

	iterator = _IMonitorDisplays.begin();
	for (; iterator != _IMonitorDisplays.end(); ++iterator)
	{
		delete (*iterator);
	}
}

Core::Core(t_parameters parameters) :
_parameters(parameters)
{

}

t_parameters &Core::getParameters()
{
	return _parameters;
}
