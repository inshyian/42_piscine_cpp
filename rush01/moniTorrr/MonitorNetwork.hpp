/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorNetwork.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 10:00:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 21:12:34 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MONITORNETWORK_HPP
# define MONITORNETWORK_HPP

# include "AMonitorModule.hpp"

class MonitorNetwork : public AMonitorModule {

public:
	MonitorNetwork();
	virtual ~MonitorNetwork();
	virtual void						initModule();
	virtual void						updateInfo();

private:
	MonitorNetwork(MonitorNetwork const &);
	MonitorNetwork		&operator=(MonitorNetwork const &);
	std::string			getIPAddress();
	std::string			getMACAddress();
	std::string			getGateway();
};

#endif
