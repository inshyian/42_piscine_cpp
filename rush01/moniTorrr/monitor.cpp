/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   monitor.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 10:09:27 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 23:34:18 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Core.hpp"
#include <iostream>
#include "DisplayNcurses.hpp"
#include <mlx.h>

bool endProcess::graphic = false;
bool endProcess::ncurses = false;

void	printUsage()
{
	std::cout << "usage: ./moniTorrr [-t | -v] [-hOdCRn]" << std::endl;
}

int		main(int argc, char **argv)
{

	Core			moniTorrr;

	if (argc - 1 < 2)
	{
		printUsage();
		return (0);
	}
	moniTorrr.initParameters(readArgs(argc, argv));
	moniTorrr.run();
	return (0);
}
