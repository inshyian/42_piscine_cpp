#include "DisplayGraphic.hpp"

DisplayGraphic::DisplayGraphic() :
_width(MLX_WIN_WIDTH),
_height(MLX_WIN_HEIGHT)
{

}

DisplayGraphic::~DisplayGraphic()
{

}

void DisplayGraphic::initDisplay()
{
	_mlx_ptr = mlx_init();
	_win_ptr = mlx_new_window(_mlx_ptr, _width, _height, const_cast<char *>(std::string(MLX_WIN_NAME).c_str()));
}

void DisplayGraphic::destroyDisplay()
{
	mlx_destroy_window(_mlx_ptr, _win_ptr);
}

int	DisplayGraphic::exit_x(void)
{
	endProcess::graphic = true;
	return (0);
}

void DisplayGraphic::placeMonitorsInWindow(std::list<IMonitorModule *> &list)
{
	std::list<IMonitorModule *>::iterator iterator = list.begin();
	std::list<t_data>::const_iterator infoIterator;
	std::list<t_data> infoList;

	int		x = 10;
	int		y = 10;
	int	color = 0xFFFFFFFF;

	for(; iterator != list.end(); ++iterator)
	{
		color = 0xFFFFFFFF;
		mlx_string_put(_mlx_ptr, _win_ptr, x + 100, y, color, const_cast<char *>((*iterator)->getModuleName().c_str()));
		y += 30;
		infoList = (*iterator)->getIformationStrings();
		infoIterator = infoList.begin();
		color = 0xFF6347;
		for (; infoIterator != infoList.end(); ++infoIterator)
		{
			mlx_string_put(_mlx_ptr, _win_ptr, x, y, color, const_cast<char *>(((*infoIterator).name + ": " + (*infoIterator).value).c_str()));
			y += 20;
		}
		y += 30;
	}
	mlx_do_sync(_mlx_ptr);
	mlx_clear_window(_mlx_ptr, _win_ptr);
	mlx_hook(_win_ptr, 17, 1L << 17, DisplayGraphic::exit_x, 0);
}
