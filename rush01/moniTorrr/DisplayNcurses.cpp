#include <zconf.h>
#include "DisplayNcurses.hpp"

DisplayNcurses::DisplayNcurses()
{

}

DisplayNcurses::~DisplayNcurses()
{

}

void DisplayNcurses::initDisplay()
{
	initscr();			/* Start curses mode 		*/
	raw();				/* Line buffering disabled	*/
	nodelay(stdscr, true);
	keypad(stdscr, TRUE);		/* We get F1, F2 etc..		*/
	noecho();			/* Don't echo() while we do getch */
}

void DisplayNcurses::destroyDisplay()
{
	endwin();
}

void DisplayNcurses::placeMonitorsInWindow(std::list<IMonitorModule *> &list)
{
	std::list<IMonitorModule *>::iterator iterator = list.begin();
	std::list<t_data>::const_iterator infoIterator;
	std::list<t_data> infoList;
	int key = 0;

	clear();
	for(; iterator != list.end(); ++iterator)
	{
		printw("\t\t%s\n", (*iterator)->getModuleName().c_str());
		infoList = (*iterator)->getIformationStrings();
		infoIterator = infoList.begin();
		for (; infoIterator != infoList.end(); ++infoIterator)
		{
			printw("%s: %s\n", (*infoIterator).name.c_str(), (*infoIterator).value.c_str());
		}
		printw("\n");

	}
	refresh();
	key = getch();
	flushinp();
	if (key == 27)
		endProcess::ncurses = true;
}
