#pragma once

# include "IMonitorDisplay.hpp"
# include <ncurses.h>
# include <iterator>
# include <list>

class DisplayNcurses : public IMonitorDisplay
{
public:
	DisplayNcurses();

	virtual ~DisplayNcurses();

	void 	initDisplay();

	void 	destroyDisplay();

	void 	placeMonitorsInWindow(std::list<IMonitorModule *> &list);

private:
	DisplayNcurses(DisplayNcurses const &);
	DisplayNcurses		&operator=(DisplayNcurses const &);
};
