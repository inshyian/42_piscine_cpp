/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorCPU.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 10:14:44 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 20:04:38 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MonitorCPU_HPP
# define MonitorCPU_HPP

# include <unistd.h>
# include "AMonitorModule.hpp"

class MonitorCPU : public AMonitorModule {

public:
	MonitorCPU();
	virtual ~MonitorCPU();
	virtual void						initModule();
	virtual void						updateInfo();

private:
	MonitorCPU(MonitorCPU const &);
	MonitorCPU		&operator=(MonitorCPU const &);
	std::string		getCPUName();
	std::string		getCPUFrequency();
	std::string		getCPUCores();
	std::string		getCPULoad();
};

#endif
