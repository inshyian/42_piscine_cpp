/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MonitorDateTime.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/13 09:59:28 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/13 18:32:49 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MonitorDateTime.hpp"
#include <stdio.h>
#include <time.h>

MonitorDateTime::MonitorDateTime() : AMonitorModule("Date and Time") {};

MonitorDateTime::~MonitorDateTime() {};

void						MonitorDateTime::initModule() {

	t_data					data;

	data.name = "Date";
	data.value = getDate();
	_iformationStrings.push_back(data);

	data.name = "Time";
	data.value = getTime();
	_iformationStrings.push_back(data);
};

void						MonitorDateTime::updateInfo() {

	std::list<t_data>::iterator		iterator;
	iterator = _iformationStrings.begin();
	(*iterator).value = getDate();
	++iterator;
	(*iterator).value = getTime();
};

std::string					MonitorDateTime::getTime() {

	static char				buf[BUFF_SIZE + 1];
	time_t 					rawtime;
	struct tm 				*timeinfo;

	time (&rawtime);
	timeinfo = localtime (&rawtime);
	strftime(buf,BUFF_SIZE,"%e-%B-%G",timeinfo);
	return (buf);
};

std::string					MonitorDateTime::getDate() {

	static char				buf[BUFF_SIZE + 1];
	time_t 					rawtime;
	struct tm 				*timeinfo;

	time (&rawtime);
	timeinfo = localtime (&rawtime);
	strftime(buf,BUFF_SIZE,"%H:%M:%S",timeinfo);
	return (buf);
};
