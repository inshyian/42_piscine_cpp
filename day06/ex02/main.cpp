/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 22:17:45 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/09 22:56:13 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Base.hpp"
#include "A.hpp"
#include "B.hpp"
#include "C.hpp"

void	identify_from_reference(Base &p) {

	std::cout << "identify_from_reference:" << std::endl;
	try
	{
		A a = dynamic_cast<A &>(p);
		std::cout << "the reference is to A class," << std::endl;
	}
	catch (std::bad_cast &e)
	{
		std::cout << "the reference is not to A class, " << e.what() << std::endl;
	}
	try
	{
		B b = dynamic_cast<B &>(p);
		std::cout << "the reference is to B class," << std::endl;
	}
	catch (std::bad_cast &e)
	{
		std::cout << "the reference is not to B class, " << e.what() << std::endl;
	}
	try
	{
		C c = dynamic_cast<C &>(p);
		std::cout << "the reference is to C class," << std::endl;
	}
	catch (std::bad_cast &e)
	{
		std::cout << "the reference is not to C class, " << e.what() << std::endl;
	}
}

void	identify_from_pointer(Base *p) {

	std::cout << "identify_from_pointer: " << std::endl;

	A *a = dynamic_cast<A *>(p);
	if (a)
		std::cout << "the pointer is to A class," << std::endl;
	else
		std::cout << "the pointer is not to A class," << std::endl;
	B *b = dynamic_cast<B *>(p);
	if (b)
		std::cout << "the pointer is to B class," << std::endl;
	else
		std::cout << "the pointer is not to B class," << std::endl;
	C *c = dynamic_cast<C *>(p);
	if (c)
		std::cout << "the pointer is to C class," << std::endl;
	else
		std::cout << "the pointer is not to C class," << std::endl;
}

Base	*generate(void) {

	int	i;

	i = rand() % 3;
	if (i == 0)
	{
		std::cout << "A class created" << std::endl;
		return (new A);
	}
	else if (i == 1)
	{
		std::cout << "B class created" << std::endl;
		return (new B);
	}
	std::cout << "C class created" << std::endl;
	return (new C);
}

int		main() {

	Base	*ptr;

	srand(time(NULL));
	for (int i = 0; i < 5; i++)
	{
		ptr = generate();
		identify_from_pointer(ptr);
		identify_from_reference(*ptr);
		delete ptr;

	}
	return (0);
}
