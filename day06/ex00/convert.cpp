/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 18:14:19 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/10 21:54:09 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "convert.hpp"

static void			convert(std::string string) {

	typedef			bool (*detectors)(std::string);
	detectors		arrayDetectors[] = {

		&isChar,
		&isInt,
		&isFloat,
		&isDouble
	};

	typedef			void (*convertors)(std::string);
	convertors		arrayConvertors[] = {

		&convertChar,
		&convertInt,
		&convertFloat,
		&convertDouble
	};

	int i = 0;

	while (i < TYPES_COUNT)
	{
		if (arrayDetectors[i](string))
		{
			arrayConvertors[i](string);
			break ;
		}
		i++;
	}
	if (i == TYPES_COUNT)
		std::cout << "Wrong input!" << std::endl;
}

int					main(int argc, char **argv) {

	int				i;

    std::cout.setf(std::ios::fixed);
	i = 1;

	if (argc == 1)
		std::cout << "No input!" << std::endl;
	else if (argc == 2)
	{
		std::cout.precision(1);
		convert(argv[i]);
	}
	else
		while (i < argc)
		{
			std::cout << i << ": " << argv[i] << std::endl;
			std::cout.precision(1);
			convert(argv[i]);
			i++;
		}
	return (0);
}
