/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   detectors.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 18:56:51 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/10 23:36:54 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "convert.hpp"

bool			isChar(std::string string) {

	if (string.length() == 1)
		return true ;
	return false ;
}

bool			isInt(std::string string) {

	const char	*str = string.c_str();

	int i = 0;
	if (str[i] == '-')
		i++;
	while (str[i])
	{
		if (str[i] < '0' || str[i] > '9')
			return false ;
		i++;
	}
	 if (atof(string.c_str()) > static_cast<double>(INT_MAX) || atof(string.c_str()) < static_cast<double>(INT_MIN))
		return false ;
	return true ;
}

bool			isFloat(std::string string) {

	if (string == "-inff" || string == "+inff" || string == "inff" || string == "nanf")
	{
		return true ;
	}

	const char	*str = string.c_str();
	bool		point_found = false;

	int	precision = 0;
	int i = 0;

	if (str[i] == '-')
		i++;
	while (str[i])
	{
		if ((str[i] < '0' || str[i] > '9') && (str[i] != '.' && str[i] != 'f'))
			return false ;
		if (point_found)
			precision++;
		if (str[i] == '.')
		{
			if (point_found == false)
				point_found = true;
			else
				return false ;
		}
		i++;
	}
	if (str[i - 1] != 'f')
		return false ;
	if (precision)
		std::cout.precision(--precision);
	return true ;
}

bool			isDouble(std::string string) {

	if (string == "-inf" || string == "+inf" || string == "inf" || string == "nan")
		return true ;

	const char	*str = string.c_str();
	bool		point_found = false;

	int	precision = 0;
	int i = 0;

	if (str[i] == '-')
		i++;
	while (str[i])
	{
		if ((str[i] < '0' || str[i] > '9') && str[i] != '.')
			return false ;
		if (point_found)
			precision++;
		if (str[i] == '.')
		{
			if (point_found == false)
				point_found = true;
			else
				return false ;
		}
		i++;
	}
	if (precision)
		std::cout.precision(precision);
	return true ;
}
