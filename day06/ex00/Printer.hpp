/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Printer.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 19:44:05 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/10 23:30:38 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTER_HPP
# define PRINTER_HPP

# include <climits>
# include <cfloat>
# include <cstdlib>
# include <math.h>
# include <iostream>

class Printer {

private:
	Printer(Printer const &src);
	Printer		&operator=(Printer const &rhs);

public:
	Printer();
	~Printer();

	void		printChar(char);
	void		printChar(int);
	void		printChar(float);
	void		printChar(double);

	void		printInt(char);
	void		printInt(int);
	void		printInt(float);
	void		printInt(double);

	void		printFloat(char);
	void		printFloat(int);
	void		printFloat(float);
	void		printFloat(double);

	void		printDouble(char);
	void		printDouble(int);
	void		printDouble(float);
	void		printDouble(double);

	bool		isNan(std::string);
	bool		isInfPos(std::string);
	bool		isInfNeg(std::string);
};

#endif
