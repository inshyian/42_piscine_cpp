/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Printer.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 19:47:34 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/10 21:41:20 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Printer.hpp"

Printer::Printer() {};

Printer::~Printer() {};

void		Printer::printChar(char char_) {

	if (std::isprint(char_))
		std::cout << "char: '" << char_ << "'" << std::endl;
	else
		std::cout << "char: " << "Non displayable" << std::endl;
};

void		Printer::printChar(int int_) {

	char	char_;

	if (int_ >= static_cast<int>(CHAR_MIN) && int_ <= static_cast<int>(CHAR_MAX))
	{
		char_ = static_cast<char>(int_);
		if (std::isprint(char_))
			std::cout << "char: '" << char_ << "'" << std::endl;
		else
			std::cout << "char: " << "non displayable" << std::endl;
	}
	else
		std::cout << "char: " << "impossible" << std::endl;
};

void		Printer::printChar(float float_) {

	char	char_;

	if (float_ >= static_cast<float>(CHAR_MIN) && float_ <= static_cast<float>(CHAR_MAX))
	{
		char_ = static_cast<char>(float_);
		if (std::isprint(char_))
			std::cout << "char: '" << char_ << "'" << std::endl;
		else
			std::cout << "char: " << "non displayable" << std::endl;
	}
	else
		std::cout << "char: " << "impossible" << std::endl;
};

void		Printer::printChar(double double_) {

	char	char_;

	if (double_ >= static_cast<double>(CHAR_MIN) && double_ <= static_cast<double>(CHAR_MAX))
	{
		char_ = static_cast<char>(double_);
		if (std::isprint(char_))
			std::cout << "char: '" << char_ << "'" << std::endl;
		else
			std::cout << "char: " << "non displayable" << std::endl;
	}
	else
		std::cout << "char: " << "impossible" << std::endl;
};

void		Printer::printInt(char char_) {

	int		int_;

	int_ = static_cast<int>(char_);
	std::cout << "int: " << int_ << std::endl;
}

void		Printer::printInt(int int_) {

	std::cout << "int: " << int_ << std::endl;
}

void		Printer::printInt(float float_) {

	int		int_;

	if (float_ >= static_cast<float>(INT_MIN) && float_ <= static_cast<float>(INT_MAX))
	{
		int_ = static_cast<int>(float_);
		std::cout << "int: " << int_ << std::endl;
	}
	else
		std::cout << "int: " << "impossible" << std::endl;
}

void		Printer::printInt(double double_) {

	int		int_;

	if (double_ >= static_cast<double>(INT_MIN) && double_ <= static_cast<double>(INT_MAX))
	{
		int_ = static_cast<int>(double_);
		std::cout << "int: " << int_ << std::endl;
	}
	else
		std::cout << "int: " << "impossible" << std::endl;
}

void		Printer::printFloat(char char_) {

	float	float_;

	float_ = static_cast<float>(char_);
	std::cout << "float: " << float_ << "f" << std::endl;
}

void		Printer::printFloat(int int_) {

	float	float_;

	float_ = static_cast<float>(int_);
	std::cout << "float: " << float_ << "f" << std::endl;
}

void		Printer::printFloat(float float_) {

	std::cout << "float: " << float_ << "f" << std::endl;
}

void		Printer::printFloat(double double_) {

	float	float_;
	if ((double_ >= static_cast<double>(FLT_MAX * -1) && double_ <= static_cast<double>(FLT_MAX)) || isnan(double_) || isinf(double_))
	{
		float_ = static_cast<float>(double_);
		std::cout << "float: " << float_ << "f" << std::endl;
	}
	else
		std::cout << "float: " << "impossible" << std::endl;
}

void		Printer::printDouble(char char_) {

	double	double_;

	double_ = static_cast<double>(char_);
	std::cout << "double: " << double_ << std::endl;
}

void		Printer::printDouble(int int_) {

	double	double_;

	double_ = static_cast<double>(int_);
	std::cout << "double: " << double_ << std::endl;
}

void		Printer::printDouble(float float_) {

	double	double_;

	double_ = static_cast<double>(float_);
	std::cout << "double: " << double_ << std::endl;
}

void		Printer::printDouble(double double_) {

	std::cout << "double: " << double_ << std::endl;
}

bool		Printer::isNan(std::string string) {

	if (string == "nanf" || string == "nan")
		return true ;
	return false ;
}

bool		Printer::isInfPos(std::string string) {

	if (string == "+inff" || string == "+inf" || string == "inf")
		return true ;
	return false ;
}

bool		Printer::isInfNeg(std::string string) {

	if (string == "-inff" || string == "-inf")
		return true ;
	return false ;
}
