/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 18:57:14 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/10 21:54:04 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONVERT_HPP
# define CONVERT_HPP
# define TYPES_COUNT 4

# include <iostream>
# include "Printer.hpp"

bool			isChar(std::string string);
bool			isInt(std::string string);
bool			isFloat(std::string string);
bool			isDouble(std::string string);
void			convertChar(std::string string);
void			convertInt(std::string string);
void			convertFloat(std::string string);
void			convertDouble(std::string string);

#endif
