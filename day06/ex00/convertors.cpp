/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convertors.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 18:58:55 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/09 21:09:25 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "convert.hpp"

void		convertChar(std::string string) {

	Printer	printer;
	char	char_;

	char_ = string.c_str()[0];

	printer.printChar(char_);
	printer.printInt(char_);
	printer.printFloat(char_);
	printer.printDouble(char_);
}

void		convertInt(std::string string) {

	Printer	printer;
	int		int_;

	int_ = atoi(string.c_str());

	printer.printChar(int_);
	printer.printInt(int_);
	printer.printFloat(int_);
	printer.printDouble(int_);
}


void		convertFloat(std::string string) {

	Printer	printer;
	float	float_;

	float_ = atof(string.c_str());

	printer.printChar(float_);
	printer.printInt(float_);
	printer.printFloat(float_);
	printer.printDouble(float_);
}

void		convertDouble(std::string string) {

	Printer	printer;
	double	double_;

	double_ = atof(string.c_str());

	printer.printChar(double_);
	printer.printInt(double_);
	printer.printFloat(double_);
	printer.printDouble(double_);
}
