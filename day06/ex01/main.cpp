/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/09 21:37:13 by ishyian           #+#    #+#             */
/*   Updated: 2019/10/10 21:45:57 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "main.hpp"

Data	*deserialize(void *raw) {

	return (reinterpret_cast<Data *>(raw));
}

void	*serialize (void) {

	Data			*data = new Data;
	std::string		pool = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char			string[9];
	int				i;

	data->n = rand() % INT_MAX;
	string[8] = '\0';
	i = 0;
	while (i < 8)
		string[i++] = pool.c_str()[rand() % pool.length()];
	data->s1 = string;
	i = 0;
	while (i < 8)
		string[i++] = pool.c_str()[rand() % pool.length()];
	data->s2 = string;
	return (reinterpret_cast<void *>(data));

}

int		main() {

	srand(time(NULL));

	void	*raw = serialize();
	Data	*data = deserialize(raw);

	std::cout << data->s1 << std::endl;
	std::cout << data->n << std::endl;
	std::cout << data->s2 << std::endl;

	delete data;
	return (0);
}
